package com.skb.goodsapp.ui.screens.product_detail;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.skb.goodsapp.R;
import com.skb.goodsapp.di.DaggerService;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductDetailView extends LinearLayout {

    @BindView(R.id.more_info_pager)
    ViewPager mViewPager;

    @Inject
    ProductDetailScreen.ProductDetailPresenter mPresenter;


    public ProductDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<ProductDetailScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                mPresenter.setViewPagerPosition(position);
            }
        });
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    public void initView(String productId, int position) {
        mViewPager.setAdapter(new ProductDetailAdapter(getContext(), productId));
        mViewPager.setCurrentItem(position);
    }
}
