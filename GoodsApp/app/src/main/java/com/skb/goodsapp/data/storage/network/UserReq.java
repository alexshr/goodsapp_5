package com.skb.goodsapp.data.storage.network;

public class UserReq {
    private String mEmail = "";

    public UserReq(String email) {
        mEmail = email;
    }

    public UserReq() {
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }


}
