package com.skb.goodsapp.ui.screens.login;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.skb.goodsapp.R;
import com.skb.goodsapp.di.DaggerService;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class LoginView extends RelativeLayout {


    //region ================= butterknife =================

    @BindView(R.id.auth_panel)
    LinearLayout mLoginPanel;

    @BindView(R.id.auth_card)
    CardView mLoginCard;
    /*
        @BindView(R.id.progress)
        ProgressBar mProgress;
    */
    @BindView(R.id.fb_btn)
    ImageButton mFbBtn;
    @BindView(R.id.tw_btn)
    ImageButton mTwBtn;
    @BindView(R.id.vk_btn)
    ImageButton mVkBtn;

    @BindView(R.id.login_btn)
    Button mLoginBtn;

    @BindView(R.id.login_email_et)
    EditText mEmailEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;

    @BindView(R.id.login_email_til)
    TextInputLayout mEmailLayout;
    @BindView(R.id.login_password_til)
    TextInputLayout mPasswordLayout;

    @BindView(R.id.logo_img)
    ImageView mLogoImg;

    @OnClick({R.id.fb_btn, R.id.tw_btn, R.id.vk_btn, R.id.login_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                break;
            case R.id.fb_btn:
                mPresenter.clickOnFb();
                break;
            case R.id.tw_btn:
                mPresenter.clickOnTwitter();
                break;
            case R.id.vk_btn:
                mPresenter.clickOnVk();
                break;
        }
    }
    //endregion

    @Inject
    LoginScreen.LoginPresenter mPresenter;

    private Disposable mAnimLogoObs;

    private Unbinder mUnbinder;

    public LoginView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<LoginScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region ================= lifecycle =================


    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            //fullscreen
            //setStyle(DialogFragment.STYLE_NORMAL, R.style.fragment_fullscreen);
            mUnbinder = ButterKnife.bind(this);
            startLogoAnim();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
            mPasswordEt.addTextChangedListener(new AuthTextWatcher(mPasswordEt));
            mEmailEt.addTextChangedListener(new AuthTextWatcher(mEmailEt));
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mUnbinder.unbind();
            mPresenter.dropView(this);
            mAnimLogoObs.dispose();
        }
    }

    //endregion

    public void showMockLoginData() {
        mEmailEt.setText("test@test.ru");
        mPasswordEt.setText("hjkjhkjhkjhkjh");
    }


    public void startFbAnimation() {
        startShakeAnimation(mFbBtn);
    }


    public void startTwAnimation() {
        startShakeAnimation(mTwBtn);
    }


    public void startVkAnimation() {
        startShakeAnimation(mVkBtn);
    }


    public void setEmailError(String error) {
        if (error != null) {
            mEmailLayout.setError(error);
        } else {
            mEmailLayout.setErrorEnabled(false);
        }
    }

    public void setEmailError(int errorRes) {
        mEmailLayout.setError(getResources().getString(errorRes));
    }

    public void setPasswordError(String error) {
        if (error != null) {
            mPasswordLayout.setError(error);
        } else {
            mPasswordLayout.setErrorEnabled(false);
        }
    }

    public void setPasswordError(int errorRes) {
        mPasswordLayout.setError(getResources().getString(errorRes));
    }

    public String getUserEmail() {
        return mEmailEt.getText().toString().trim();
    }

    public String getUserPassword() {
        return mPasswordEt.getText().toString().trim();
    }


    private void startShakeAnimation(View btn) {
        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.shake);
        btn.startAnimation(animation);
    }

    public void onLoginPanelStateChanged(boolean isOpened) {

        LinearLayout.LayoutParams cardParams = (LinearLayout.LayoutParams) mLoginCard.getLayoutParams();
        if (isOpened) {
            cardParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            mLoginCard.setLayoutParams(cardParams);
            mLoginCard.getChildAt(0).setVisibility(VISIBLE);
            mLoginCard.setCardElevation(getResources().getDimensionPixelSize(R.dimen.card_elevation));
        } else {
            cardParams.height = 0;
            mLoginCard.setLayoutParams(cardParams);
            mLoginCard.getChildAt(0).setVisibility(INVISIBLE);
            mLoginCard.setCardElevation(0f);
        }
        showMockLoginData();
    }


    private void startLogoAnim() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Animator logoScaleAnim = AnimatorInflater.loadAnimator(getContext(), R.animator.logo_scale_animator);
            AnimatedVectorDrawable logoVectorAnim = (AnimatedVectorDrawable) mLogoImg.getDrawable();
            logoScaleAnim.setTarget(mLogoImg);
            mAnimLogoObs = Observable.interval(4000, TimeUnit.MILLISECONDS)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aLong -> {
                        logoScaleAnim.start();
                        logoVectorAnim.start();
                    });
        }
    }

    class AuthTextWatcher implements TextWatcher {

        private final EditText mEditText;


        public AuthTextWatcher(EditText editText) {
            mEditText = editText;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (mEditText.getId()) {
                case R.id.login_email_et:
                case R.id.login_password_et:
                    mLoginBtn.setEnabled(mPresenter.validateEmail() & mPresenter.validatePassword());
            }
        }
    }


}