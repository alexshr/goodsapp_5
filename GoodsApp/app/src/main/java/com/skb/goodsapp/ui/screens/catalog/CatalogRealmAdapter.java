package com.skb.goodsapp.ui.screens.catalog;

import android.content.Context;
import android.database.DataSetObserver;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.ProductRealm;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.mortar.ScreenScoper;
import com.skb.goodsapp.mvp.models.CatalogModel;
import com.skb.goodsapp.ui.screens.product.ProductScreen;
import com.skb.goodsapp.ui.screens.product.ProductView;

import javax.inject.Inject;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import mortar.MortarScope;

import static com.skb.goodsapp.ConstantManager.LOG_TAG;

/**
 * адаптер инициализируется с помощью RealmResults
 * при изменениях данных (например на realm server) - обновляет view автоматически!
 */
public class CatalogRealmAdapter extends PagerAdapter {

    @Inject
    CatalogModel mCatalogModel;

    private ProductView mSelectedProductView;

    private RealmResults<ProductRealm> mAdapterData;

    private RealmChangeListener<RealmResults<ProductRealm>> mResultChangeListener =
            result -> notifyDataSetChanged();

    public CatalogRealmAdapter(Context context) {
        ((CatalogScreen.Component) DaggerService.getDaggerComponent(context)).inject(this);

        mAdapterData = mCatalogModel.getProductList();
        mAdapterData.addChangeListener(mResultChangeListener);
        //mResultChangeListener.onChange(data);//первое отображение?
    }

    @Override
    public int getCount() {
        return mAdapterData == null ? 0 : mAdapterData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ProductRealm product = mAdapterData.get(position);

        MortarScope scope = ScreenScoper.provideScopeFromParentContext(new ProductScreen(product.getId()), container.getContext());
        Context context = scope.createContext(container.getContext());

        View newView = LayoutInflater.from(context).inflate(R.layout.screen_product, container, false);
        container.addView(newView);
        MortarScope screenScope = MortarScope.getScope(newView.getContext());
        Log.d(LOG_TAG, "instantiateItem: " + screenScope.getName());
        return newView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        MortarScope screenScope = MortarScope.getScope(((View) object).getContext());
        container.removeView((View) object);
        screenScope.destroy();
        Log.d(LOG_TAG, "destroyItem and mortarScope: " + screenScope.getName());
    }

    public ProductRealm getProductByPosition(int pos) {
        return mAdapterData.get(pos);
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        super.unregisterDataSetObserver(observer);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        mSelectedProductView = (ProductView) object;
    }

    public ProductView getSelectedProductView() {
        return mSelectedProductView;
    }
}
