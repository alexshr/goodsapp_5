package com.skb.goodsapp.data.rx;

import io.reactivex.observers.DisposableObserver;

public class SimpleDisposableObserver<T> extends DisposableObserver<T> {
    @Override
    public void onNext(T o) {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onComplete() {

    }
}
