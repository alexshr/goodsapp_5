package com.skb.goodsapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Point;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import java.security.MessageDigest;
import java.util.List;
import java.util.regex.Pattern;

public class Utils {

    public static boolean validate(String str, Pattern pattern) {
        return str != null && !str.trim().isEmpty() &&
                pattern.matcher(str.trim()).matches();
    }

    //http://stackoverflow.com/questions/1016896/get-screen-dimensions-in-pixels
    public static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);

        return size.x;
    }


/*
    public static String getPath(Uri uri, Context context) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) {
            return null;
        }
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(columnIndex);
        cursor.close();
        return path;
    }
*/

    /**
     * получаем задержку перед следующей покупкой (растет по экспоненте)
     *
     * @param runCount           - номер попытки
     * @param initialBackOffInMs - время перед первой попыткой
     * @return
     */
    public static Long getExponentialDelayInMs(int runCount, long initialBackOffInMs) {
        return Long.valueOf(initialBackOffInMs * (long) Math.pow(2.0D, (double) Math.max(0, runCount - 1)));
    }

    public static String listToString(List list) {
        StringBuilder sb = null;
        if (list != null) {
            sb = new StringBuilder("{");
            if (!list.isEmpty()) {
                for (Object obj : list) {
                    sb.append("," + obj);
                }
                sb.delete(1, 2);
            }
            sb.append("}");
            return sb.toString();
        } else {
            return null;
        }
    }

    public static boolean isAndroidTestMode() {
        return isClassAvailable("com.skb.goodsapp.AndroidTestIndicator");
    }

    public static boolean isTestMode() {
        return isClassAvailable("com.skb.goodsapp.TestIndicator");
    }

    public static boolean isClassAvailable(String className) {
        boolean result;
        try {
            Class.forName(className);

            result = true;
        } catch (final Exception e) {
            result = false;
        }
        return result;
    }

    /**
     * Call it in Activity.onCreate to see facebook debug key hash
     *
     * @param activity
     */
    public static void logFbKeyHash(Activity activity) {
        try {
            PackageInfo info = activity.getPackageManager().getPackageInfo(
                    activity.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            Log.e("logFbKeyHash", "", e);
        }
    }
}
