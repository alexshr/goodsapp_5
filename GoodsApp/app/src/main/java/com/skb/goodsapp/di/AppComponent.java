package com.skb.goodsapp.di;

import com.skb.goodsapp.data.managers.DataManager;
import com.skb.goodsapp.data.managers.RealmManager;
import com.skb.goodsapp.flow.AppDispatcher;
import com.skb.goodsapp.flow.ScreenMenu;
import com.skb.goodsapp.jobs.BaseJob;
import com.skb.goodsapp.mvp.models.RootModel;
import com.skb.goodsapp.mvp.presenters.RootPresenter;
import com.skb.goodsapp.ui.activities.SplashActivity;

import javax.inject.Singleton;

import retrofit2.Retrofit;

/**
 * Created by pc on 04.04.2017.
 */ //region ================= DI =================
@Singleton
@dagger.Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(DataManager dataManager);

    void inject(RealmManager realmManager);

    void inject(RootModel rootModel);

    void inject(RootPresenter rootPresenter);

    void inject(AppDispatcher appDispatcher);

    void inject(SplashActivity splashActivity);

    void inject(BaseJob baseJob);

    void inject(ScreenMenu menu);

    Retrofit getRetrofit();// for ErrorUtils
}
