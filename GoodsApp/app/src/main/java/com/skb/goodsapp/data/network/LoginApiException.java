package com.skb.goodsapp.data.network;


import com.skb.goodsapp.R;

public class LoginApiException extends Exception {
    public int getMessageRes() {
        return R.string.login_error;
    }

}
