package com.skb.goodsapp.data.storage.dto;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;

public class ActivityResultDto {
    private int requestCode;
    private int resultCode;
    @Nullable
    private Intent mIntent;

    public ActivityResultDto(int requestCode, int resultCode, Intent intent) {
        this.requestCode = requestCode;
        this.resultCode = resultCode;
        this.mIntent = intent;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public int getResultCode() {
        return resultCode;
    }

    @Nullable
    public Intent getIntent() {
        return mIntent;
    }

    @Nullable
    public Uri getData() {
        return mIntent == null ? null : mIntent.getData();
    }

}
