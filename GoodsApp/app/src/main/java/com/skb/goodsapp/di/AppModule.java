package com.skb.goodsapp.di;

import android.content.Context;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.skb.goodsapp.AppConfig;
import com.skb.goodsapp.MyApplication;
import com.skb.goodsapp.data.managers.DataManager;
import com.skb.goodsapp.data.managers.PreferencesManager;
import com.skb.goodsapp.data.managers.RealmManager;
import com.skb.goodsapp.data.network.ErrorUtils;
import com.skb.goodsapp.data.network.HostSelectionInterceptor;
import com.skb.goodsapp.data.network.RestService;
import com.skb.goodsapp.jobs.BaseJob;
import com.skb.goodsapp.mvp.models.RootModel;
import com.skb.goodsapp.utils.ConnectionChecker;
import com.squareup.moshi.Moshi;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * Created by pc on 04.04.2017.
 */
@dagger.Module
public class AppModule {
    //region ================= network providers =================
/*
    @VisibleForTesting(otherwise = NONE)
    @Singleton
    @Provides
    MockWebServer provideMockWebServer() {
        return new MockWebServer();
    }
*/


    private Context mContext;

    public AppModule(Context context) {
        mContext = context;
    }

    @Singleton
    @Provides
    protected OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor(new HostSelectionInterceptor())
                .connectTimeout(AppConfig.MAX_CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(AppConfig.MAX_READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(AppConfig.MAX_WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
                .build();
    }

    //to override for test
    @Singleton
    @Provides
    protected String provideBaseUrl() {
        return AppConfig.BASE_URL;
    }

    @Provides
    @Singleton
    protected Retrofit provideRetrofit(OkHttpClient okHttpClient, String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(createConverterFactory())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    protected RestService provideRestService(Retrofit retrofit) {
        return retrofit.create(RestService.class);
    }

    @Provides
    @Singleton
    protected ErrorUtils provideErrorParser(Retrofit retrofit) {
        return new ErrorUtils();
    }

    private Converter.Factory createConverterFactory() {
        return MoshiConverterFactory.create(new Moshi.Builder()
                .build());
    }
    //endregion

    @Provides
    @Singleton
    protected Context provideAppContext() {
        return mContext;
    }


    @Provides
    @Singleton
    protected PreferencesManager providePreferencesManager() {
        return new PreferencesManager(mContext);
    }

    @Provides
    @Singleton
    protected ConnectionChecker provideConnectionChecker() {
        return new ConnectionChecker(mContext);
    }

    @Provides
    @Singleton
    protected RealmManager provideRealmManager() {
        return new RealmManager();
    }

    @Provides
    @Singleton
    protected DataManager provideDataManager() {
        DataManager dm = new DataManager();
        dm.enableCatalogSynchronization();
        return dm;
    }

    @Provides
    @Singleton
    protected RootModel provideRootModel() {
        return new RootModel();
    }

    @Provides
    @Singleton
    protected JobManager provideJobManager() {
        Configuration configuration = new Configuration.Builder(mContext)
                .minConsumerCount(AppConfig.MIN_CONSUMER_COUNT) //always keep at least one consumer alive
                .maxConsumerCount(AppConfig.MAX_CONSUMER_COUNT) //up to 3 consumers at a time
                .loadFactor(AppConfig.LOAD_FACTOR) //3 jobs per consumer
                .consumerKeepAlive(AppConfig.KEEP_ALIVE) //wait 2 minute for thread with consumer
                .injector(job -> {
                    if (job instanceof BaseJob) {
                        ((BaseJob) job).inject(MyApplication.getDaggerComponent());
                    }
                })
                .build();
        return new JobManager(configuration);
    }

}
