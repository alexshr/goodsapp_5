package com.skb.goodsapp.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.skb.goodsapp.BuildConfig;
import com.skb.goodsapp.MyApplication;
import com.skb.goodsapp.RealmServerConfig;
import com.skb.goodsapp.data.managers.DataManager;
import com.skb.goodsapp.data.managers.RealmManager;

import javax.inject.Inject;

import io.realm.ObjectServerError;
import io.realm.Realm;
import io.realm.SyncConfiguration;
import io.realm.SyncCredentials;
import io.realm.SyncUser;

import static android.content.ContentValues.TAG;
import static com.skb.goodsapp.ConstantManager.LOG_TAG;
import static com.skb.goodsapp.RealmServerConfig.REALM_DB_URL;
import static com.skb.goodsapp.RealmServerConfig.REALM_PASSWORD;
import static com.skb.goodsapp.RealmServerConfig.REALM_USER;


public class SplashActivity extends Activity {

    private String LOG_TEST = LOG_TAG + " TEST";

    public DataManager getDataManager() {
        return mDataManager;
    }

    @Inject
    DataManager mDataManager;

    RealmManager mRealmManager;
    private SyncUser mUser;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Utils.logFbKeyHash(this); //only to get fb key hash first time

        MyApplication.getDaggerComponent().inject(this);
        mRealmManager = mDataManager.getRealmManager();
        if (BuildConfig.FLAVOR.equals("realmMp")) {
            mUser = SyncUser.currentUser();
            if (mUser == null || !mUser.isValid()) {
                loginRealmServer();
                return;
            }
        }
        configureAndStart();
    }

    private void loginRealmServer() {
        SyncCredentials syncCredentials = SyncCredentials.usernamePassword(REALM_USER,
                REALM_PASSWORD, false);
        SyncUser.loginAsync(syncCredentials, RealmServerConfig.REALM_AUTH_URL, new SyncUser.Callback() {
            @Override
            public void onSuccess(SyncUser user) {
                Log.d(LOG_TEST, "loginRealmServer onSuccess");
                mUser = user;
                configureAndStart();
            }

            @Override
            public void onError(ObjectServerError error) {
                Log.d(LOG_TEST, "loginRealmServer onError", error);
                configureAndStart();
            }
        });
    }

    private void configureAndStart() {
        if (mUser != null && mUser.isValid()) {
            configureSyncRealm(mUser);
        } else {
            RealmManager.configureRealm();
        }
        startActivity(new Intent(SplashActivity.this, RootActivity.class));
        finish();
    }


    private void configureSyncRealm(SyncUser user) {
        SyncConfiguration syncConfig = new SyncConfiguration.Builder(user, REALM_DB_URL)
                //.initialData(mInitDataTrans)
                //.name("app-realm8")
                //.schemaVersion(1)
                .errorHandler((session, error) -> Log.e(TAG, "SyncSession errorHandler: error=" + error + " session" + session))
                .build();
        Realm.setDefaultConfiguration(syncConfig);
        //mRealmManager.deleteAllFromRealm();
    }
/*
    private void configureRealm() {
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                //.initialData(mInitDataTrans)
                .build();
        Realm.setDefaultConfiguration(config);


    }
*/

}
