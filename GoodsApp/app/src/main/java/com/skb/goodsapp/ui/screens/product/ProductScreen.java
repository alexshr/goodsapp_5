package com.skb.goodsapp.ui.screens.product;

import android.os.Bundle;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.realm.BasketProductRealm;
import com.skb.goodsapp.data.storage.realm.FavoriteProductRealm;
import com.skb.goodsapp.data.storage.realm.ProductRealm;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.AbstractScreen;
import com.skb.goodsapp.flow.Screen;
import com.skb.goodsapp.mvp.models.CatalogModel;
import com.skb.goodsapp.ui.screens.catalog.CatalogScreen;
import com.skb.goodsapp.ui.screens.product_detail.ProductDetailScreen;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionInflater;
import com.transitionseverywhere.TransitionManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;
import javax.inject.Scope;

import dagger.Provides;
import flow.Flow;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import mortar.MortarScope;
import mortar.ViewPresenter;


@Screen(R.layout.screen_product)
public class ProductScreen extends AbstractScreen {
    private String mProductId;

    public ProductScreen(String productId) {
        mProductId = productId;
        setParentScopeScreen(new CatalogScreen());
    }

    @Override
    public String getScopeName() {
        return String.format("%s_%s", super.getScopeName(), mProductId);
    }

    @Override
    public Object[] getModules() {
        return new Object[]{new Module()};
    }

    @Override
    public Class getComponentClass() {
        return Component.class;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ProductScreen && mProductId.equals(((ProductScreen) o).mProductId);
    }

    @Override
    public int hashCode() {
        return mProductId.hashCode();
    }


    //region ============================= DI =============================
    @dagger.Module
    public class Module {
        @Provides
        @ProductScope
        ProductPresenter provideProductPresenter() {
            return new ProductPresenter();
        }

    }

    @dagger.Component(dependencies = CatalogScreen.Component.class, modules = Module.class)
    @ProductScope
    public interface Component {
        void inject(ProductPresenter presenter);

        void inject(ProductView view);
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface ProductScope {
    }
    //endregion

    //region ============================= Presenter =============================
    public class ProductPresenter extends ViewPresenter<ProductView> {

        @Inject
        CatalogModel mCatalogModel;

        @Inject
        CatalogScreen.CatalogPresenter mCatalogPresenter;


        private boolean mIsZoomedIn;

        ProductRealm mProduct;
        RealmResults<BasketProductRealm> mBasket;
        RealmResults<FavoriteProductRealm> mFavorites;

        //private RealmChangeListener<ProductRealm> mProductRealmListener = product -> {if(product.isValid()) showProductArea(product);};
        private RealmChangeListener<ProductRealm> mProductRealmListener = product -> showProductArea(product);


        private RealmChangeListener<RealmResults<BasketProductRealm>> mBasketRealmListener = basket -> showBasketArea();

        private RealmChangeListener<RealmResults<FavoriteProductRealm>> mFavoritesRealmListener = favorite -> showFavoriteArea();


        //отдельно отслеживаем поля формы, связанные с различными realm объектами
        private void showProductArea(ProductRealm product) {
            if (getView() != null) getView().showProductArea(product);
        }

        private void showFavoriteArea() {
            boolean isFavorite = mCatalogModel.isFavorite(mProductId);
            if (getView() != null) getView().showFavoriteArea(isFavorite);
        }

        private void showBasketArea() {

            int count = !mCatalogModel.isInBasket(mProductId) ? 1 : mCatalogModel.getBasketProductById(mProductId).getCount();
            if (getView() != null) getView().showBasketArea(count);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            mProduct = mCatalogModel.getProductById(mProductId);
            mBasket = mCatalogModel.getBasket();
            mFavorites = mCatalogModel.getFavorites();

            showProductArea(mProduct);
            showBasketArea();
            showFavoriteArea();

            mProduct.addChangeListener(mProductRealmListener);
            mBasket.addChangeListener(mBasketRealmListener);
            mFavorites.addChangeListener(mFavoritesRealmListener);

            //show(mProduct);//перввй показ - нужен при синхронных запросах

            if (mIsZoomedIn) {
                getView().provideZoomState(mIsZoomedIn);
            }
        }

        @Override
        public void dropView(ProductView view) {
            super.dropView(view);

            //mProduct.removeAllChangeListeners(); //убрал, т.к. вылетало, но в целом было бы правильно
            //mBasket.removeAllChangeListeners();
            //mFavorites.removeAllChangeListeners();
        }

        @Override
        protected void onExitScope() {
            super.onExitScope();
        }

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);

        }

        public void clickOnPlus() {
            int count = getView().getProductCount();
            getView().mProductCountTextView.setText(++count + "");
        }


        public void clickOnMinus() {
            int count = getView().getProductCount();
            count = count > 1 ? --count : count;
            getView().mProductCountTextView.setText(count + "");
        }

        public void clickOnDetail() {
            Flow.get(getView()).set(new ProductDetailScreen(mProductId));
        }


        public void onFavoriteChange(boolean isChecked) {
            if (mCatalogModel.isUserAuth()) {
                mCatalogModel.setFavorite(mProductId, isChecked);
            } else {
                showFavoriteArea();
                mCatalogPresenter.getRootPresenter().showError(new MessageDto(R.string.not_authorised));
            }
        }

        public void clickProduct() {
            changeZoomWithAnim();
        }

        public boolean isZoomedIn() {
            return mIsZoomedIn;
        }

        public void changeZoomWithAnim() {
            Transition transitionSet = TransitionInflater.from(getView().getContext()).inflateTransition(R.transition.product_image_zoom);
            TransitionManager.beginDelayedTransition(getView(), transitionSet);
            mIsZoomedIn = !mIsZoomedIn;
            getView().provideZoomState(mIsZoomedIn);
        }
    }
    //endregion
}
