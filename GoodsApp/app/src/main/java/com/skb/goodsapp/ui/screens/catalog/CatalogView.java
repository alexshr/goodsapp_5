package com.skb.goodsapp.ui.screens.catalog;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.skb.goodsapp.ConstantManager;
import com.skb.goodsapp.R;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.mvp.views.IBackPressedView;
import com.skb.goodsapp.ui.screens.product.ProductScreen;
import com.skb.goodsapp.ui.screens.product.ProductView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import me.relex.circleindicator.CircleIndicator;

import static com.skb.goodsapp.ConstantManager.LOG_TAG;

public class CatalogView extends RelativeLayout implements IBackPressedView {

    //region ================= butterknife =================
    @BindView(R.id.product_view_pager)
    ViewPager mProductViewPager;
    @BindView(R.id.add_to_card_button)
    Button mAddToCardButton;
    @BindView(R.id.indicator)
    CircleIndicator mIndicator;

    @OnClick(R.id.add_to_card_button)
    public void onClick() {
        int count = getCurrentProductView().getProductCount();
        Log.d(LOG_TAG, "@OnClick(R.id.add_to_card_button) count=" + count);
        mPresenter.addProductToBasket(count);
    }
    //endregion

    @Inject
    CatalogScreen.CatalogPresenter mPresenter;


    private Unbinder mUnbinder;

    public CatalogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<CatalogScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region ================= lifecycle =================


    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            mUnbinder = ButterKnife.bind(this);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
            mProductViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    mPresenter.setViewPagerPosition(position);
                    ProductScreen.ProductPresenter selProductPresenter = getCurrentProductView().getPresenter();
                    if (selProductPresenter.isZoomedIn()) {
                        selProductPresenter.changeZoomWithAnim();//при листании убираем zoom
                    }
                }
            });
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mUnbinder.unbind();
            mPresenter.dropView(this);
        }
    }
    //endregion

    public void showCatalog(CatalogRealmAdapter adapter, int pos) {
        mProductViewPager.setOffscreenPageLimit(ConstantManager.OFFSCREEN_PAGES);
        mProductViewPager.setAdapter(adapter);
        mIndicator.setViewPager(mProductViewPager);
        mProductViewPager.setCurrentItem(pos);
    }

    @Override
    public boolean onViewBackPressed() {
        ProductScreen.ProductPresenter productPresenter = getCurrentProductView().getPresenter();
        if (productPresenter.isZoomedIn()) {
            productPresenter.changeZoomWithAnim();
            return true;
        } else {
            return false;
        }
    }

    public ProductView getCurrentProductView() {
        return ((CatalogRealmAdapter) mProductViewPager.getAdapter()).getSelectedProductView();
    }

    public int getCurrentPosition() {
        return mProductViewPager.getCurrentItem();
    }

}