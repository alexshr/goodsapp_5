package com.skb.goodsapp.mvp.presenters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.callback.JobManagerCallbackAdapter;
import com.facebook.login.LoginManager;
import com.skb.goodsapp.MyApplication;
import com.skb.goodsapp.R;
import com.skb.goodsapp.StubEntityFactory;
import com.skb.goodsapp.TestSchedulerRule;
import com.skb.goodsapp.data.storage.dto.ActivityResultDto;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.dto.UserInfoDto;
import com.skb.goodsapp.di.AppComponent;
import com.skb.goodsapp.di.AppModule;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.NavigationDrawerHelper;
import com.skb.goodsapp.flow.ScreenMenu;
import com.skb.goodsapp.jobs.BaseJob;
import com.skb.goodsapp.mvp.models.RootModel;
import com.skb.goodsapp.ui.activities.RootActivity;
import com.skb.goodsapp.ui.screens.catalog.CatalogScreen;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import flow.Direction;
import flow.Flow;
import flow.History;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.PERMISSION_DENIED;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static com.birbit.android.jobqueue.callback.JobManagerCallback.RESULT_CANCEL_CANCELLED_VIA_SHOULD_RE_RUN;
import static com.birbit.android.jobqueue.callback.JobManagerCallback.RESULT_CANCEL_CANCELLED_WHILE_RUNNING;
import static com.birbit.android.jobqueue.callback.JobManagerCallback.RESULT_CANCEL_REACHED_RETRY_LIMIT;
import static com.birbit.android.jobqueue.callback.JobManagerCallback.RESULT_CANCEL_SINGLE_INSTANCE_WHILE_RUNNING;
import static com.birbit.android.jobqueue.callback.JobManagerCallback.RESULT_SUCCEED;
import static com.skb.goodsapp.ConstantManager.REQUEST_CAMERA_CODE;
import static com.skb.goodsapp.ConstantManager.REQUEST_GALLERY_CODE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MyApplication.class, RootPresenter.class, Flow.class, ContextCompat.class, NavigationDrawerHelper.class, History.class, Build.VERSION.class, VKSdk.class})

public class RootPresenterTest {

    @Rule
    public TestSchedulerRule testSchedulerRule = new TestSchedulerRule();

    @Mock
    private RootModel mockRootModel;
    @Mock
    RootActivity mockRootActivity;
    @Mock
    RootActivity.Component mockRootActivityComponent;
    @Mock
    JobManager mockJobManager;
    @Mock
    Intent mockIntent;
    @Mock
    Flow mockFlow;

    @Mock
    BaseJob mockBaseJob;

    @Mock
    NavigationView mockNavigationView;

    @Mock
    History mockHistory;

    @Mock
    ScreenMenu mockScreenMenu;

    @Mock
    ViewPager mockViewPager;

    @Mock
    LoginManager mockFbLoginManager;

    @Mock
    TwitterAuthClient mockTwAuthClient;

    @Mock
    Callback<TwitterSession> mockTwSessionCallback;

    private MortarScope mMortarScope;

    private TestScheduler mTestScheduler;

    private RootPresenter mRootPresenter;
    private BundleServiceRunner mBundleServiceRunner;
    //private MortarScope mMortarScope;

    @Before
    public void setUp() throws Exception {
        prepareStubs();

        mRootPresenter = new RootPresenter();
        //mRootPresenter.takeView(mockRootActivity);
    }

    private void prepareStubs() {

        mockStatic(MyApplication.class, Flow.class, ContextCompat.class, NavigationDrawerHelper.class, Build.VERSION.class, VKSdk.class);
        mTestScheduler = testSchedulerRule.getTestScheduler();

        //для inject используется именно AppComponent
        given(MyApplication.getDaggerComponent()).willReturn(DaggerService.buildComponent(AppComponent.class, new AppModule(mockRootActivity) {

            @Override
            protected RootModel provideRootModel() {
                return mockRootModel;
            }

            @Override
            protected JobManager provideJobManager() {
                return mockJobManager;
            }
        }));

        mBundleServiceRunner = new BundleServiceRunner();
        mMortarScope = MortarScope.buildRootScope()
                .withService(BundleServiceRunner.SERVICE_NAME, mBundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, mockRootActivityComponent)
                .build("MockRoot");
        //noinspection WrongConstant
        given(mockRootActivity.getSystemService(BundleServiceRunner.SERVICE_NAME)).willReturn(mBundleServiceRunner);
        //noinspection WrongConstant
        given(mockRootActivity.getSystemService(MortarScope.class.getName())).willReturn(mMortarScope);

    }


    @After
    public void tearDown() throws Exception {
        //mRootPresenter.dropView(mockRootActivity);
    }

    //region ================= tests =================
    @Test
    public void onActivityResult_getActivityResultPublishUrlSubject_gallery() throws Exception {
        Uri data = Uri.parse("content://media/external/images/1");
        onActivityResult_getActivityResultPublishUrlSubject(REQUEST_GALLERY_CODE, Activity.RESULT_OK, data);
    }

    @Test
    public void onActivityResult_getActivityResultPublishUrlSubject_camera() throws Exception {
        onActivityResult_getActivityResultPublishUrlSubject(REQUEST_CAMERA_CODE, Activity.RESULT_OK, null);
    }

    @Test
    public void onActivityResult_getActivityResultPublishUrlSubject_result_code_error() throws Exception {
        onActivityResult_getActivityResultPublishUrlSubject(REQUEST_CAMERA_CODE, Activity.RESULT_CANCELED, null);
    }

    @Test
    public void onActivityResult_getActivityResultPublishUrlSubject_gallery_data_error() throws Exception {
        onActivityResult_getActivityResultPublishUrlSubject(REQUEST_GALLERY_CODE, Activity.RESULT_OK, null);
    }

    @Test
    public void onActivityResult_getActivityResultPublishUrlSubject_unknown_request_code() throws Exception {
        onActivityResult_getActivityResultPublishUrlSubject(12345, Activity.RESULT_OK, null);
    }


    private void onActivityResult_getActivityResultPublishUrlSubject(int requestCode, int resultCode, Uri data) throws Exception {
        //given
        mRootPresenter = spy(mRootPresenter);
        ActivityResultDto activityResultDto = new ActivityResultDto(requestCode, resultCode, mockIntent);
        given(mockIntent.getData()).willReturn(data);
        String fileUri = "file_uri";
        Whitebox.setInternalState(mRootPresenter, "mPhotoFileUrl", fileUri);


/*
        BehaviorSubject<ActivityResultDto> subject = Whitebox.getInternalState(mRootPresenter, "mActivityResultPublishSubject");
        subject.onNext(activityResultDto);
*/
        //when
        mRootPresenter.onActivityResult(requestCode, resultCode, mockIntent);
        TestObserver<String> testObserver = mRootPresenter.getActivityResultAvatarUrlSubject().test();
        //mTestScheduler.advanceTimeBy(1, TimeUnit.SECONDS);
        mTestScheduler.triggerActions();

        //then
        boolean isError = false;
        testObserver
                .assertNoErrors();
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_GALLERY_CODE) {
                if (data != null) {
                    testObserver.assertValue(data.toString());
                } else {
                    testObserver.assertNoValues();
                    isError = true;
                }
            } else if (requestCode == REQUEST_CAMERA_CODE) {
                if (fileUri != null) {
                    testObserver.assertValue(fileUri);
                } else {
                    testObserver.assertNoValues();
                    isError = true;
                }
            } else {
                testObserver.assertNoValues();
            }
        } else {
            testObserver.assertNoValues();
            isError = true;
        }

        verifyPrivate(mockRootModel, times(isError ? 1 : 0)).invoke("sendError", new MessageDto(R.string.avatar_not_created));
    }


    @Test
    public void doAfterJob_SUCCEED() throws Exception {
        doAfterJob(RESULT_SUCCEED, new MessageDto(R.string.avatar_uploaded), null);
    }

    @Test
    public void doAfterJob_SUCCEED_NO_MES() throws Exception {
        doAfterJob(RESULT_SUCCEED, null, null);
    }

    @Test
    public void doAfterJob_CANCEL_NO_MES() throws Exception {
        doAfterJob(RESULT_CANCEL_REACHED_RETRY_LIMIT, null, null);
    }

    @Test
    public void doAfterJob_CANCEL_CANCELLED_VIA_SHOULD_RE_RUN() throws Exception {
        doAfterJob(RESULT_CANCEL_CANCELLED_VIA_SHOULD_RE_RUN, null, new MessageDto(R.string.avatar_upload_cancelled));
    }

    @Test
    public void doAfterJob_CANCEL_REACHED_RETRY_LIMIT() throws Exception {
        doAfterJob(RESULT_CANCEL_REACHED_RETRY_LIMIT, null, new MessageDto(R.string.avatar_upload_cancelled));
    }

    @Test
    public void doAfterJob_CANCEL_CANCELLED_WHILE_RUNNING() throws Exception {
        doAfterJob(RESULT_CANCEL_CANCELLED_WHILE_RUNNING, null, new MessageDto(R.string.avatar_upload_cancelled));
    }

    @Test
    public void doAfterJob_CANCEL_SINGLE_INSTANCE_WHILE_RUNNING() throws Exception {
        doAfterJob(RESULT_CANCEL_SINGLE_INSTANCE_WHILE_RUNNING, null, new MessageDto(R.string.avatar_upload_cancelled));
    }


    private void doAfterJob(int resultCode, MessageDto resultMes, MessageDto cancelMes) throws Exception {
        //given
        //mRootPresenter.takeView(mockRootActivity);
        RootPresenter rootPresenter = spy(mRootPresenter);
        Whitebox.setInternalState(rootPresenter, RootActivity.class, mockRootActivity);
        //Whitebox.getInternalState(rootPresenter, RootActivity.class,mockRootActivity);
        /*
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                return null;
            }
        }).when(rootPresenter, "showMessage",anyInt(),any());
*/
        //doNothing().when(rootPresenter, "showMessage",anyInt(),any());


        //JobManagerCallbackAdapter callbackAdapter=Whitebox.getInternalState(rootPresenter,JobManagerCallbackAdapter.class);

        //given(mRootPresenter.getView()).willReturn(resultMes);
        given(mockBaseJob.getSuccessMes()).willReturn(resultMes);
        given(mockBaseJob.getCancelMes()).willReturn(cancelMes);

        //when
        Whitebox.<Void>invokeMethod(rootPresenter, "doAfterJob", mockBaseJob, resultCode);

        //then
        int resultCount = 0;
        int cancelCount = 0;
        switch (resultCode) {
            case RESULT_SUCCEED:
                if (resultMes != null) {
                    resultCount = 1;
                }
                break;
            case RESULT_CANCEL_REACHED_RETRY_LIMIT:
            case RESULT_CANCEL_CANCELLED_VIA_SHOULD_RE_RUN:
            case RESULT_CANCEL_CANCELLED_WHILE_RUNNING:
            case RESULT_CANCEL_SINGLE_INSTANCE_WHILE_RUNNING:
                if (cancelMes != null) {
                    cancelCount = 1;
                }
        }
        then(mockRootActivity).should(times(resultCount)).showMessage(resultMes);
        then(mockRootActivity).should(times(cancelCount)).showError(cancelMes);
    }

    @Test
    public void jobManagerCallbackAdapter_test() throws Exception {
        //given
        //RootPresenter spyRootPresenter = spy(mRootPresenter);
        //doNothing().when(mRootPresenter, "doAfterJob",any(BaseJob.class),anyInt());
        JobManagerCallbackAdapter callbackAdapter = Whitebox.getInternalState(mRootPresenter, "mJobManagerAppCallback");


        //when
        callbackAdapter.onAfterJobRun(mockBaseJob, RESULT_SUCCEED);

        // RootPresenter spyRootPresenter = spy(mRootPresenter);
        //then
        verifyPrivate(mRootPresenter, times(1)).invoke("doAfterJob", mockBaseJob, RESULT_SUCCEED);
    }

    @Test
    public void takeView_onLoad_test_dropView() throws Exception {
        //given
        PublishSubject<MessageDto> messageSubject = PublishSubject.create();
        MessageDto messageDto = new MessageDto(R.string.comment_added);

        PublishSubject<MessageDto> errorSubject = PublishSubject.create();
        MessageDto errorDto = new MessageDto(R.string.app_error);

        PublishSubject<String> avatarSubject = PublishSubject.create();
        String avatarUrl = "http://skba1.mgbeta.ru/assets/avatars/1491972833904.png";

        PublishSubject<UserInfoDto> userInfoSubject = PublishSubject.create();
        UserInfoDto userInfoDto = StubEntityFactory.makeStub(UserInfoDto.class);

        BehaviorSubject<Boolean> progressSubject = BehaviorSubject.create();

        given(mockRootModel.getMessageSubject()).willReturn(messageSubject);
        given(mockRootModel.getErrorSubject()).willReturn(errorSubject);
        given(mockRootModel.getProgressSubject()).willReturn(progressSubject);
        given(mockRootModel.getUserInfoSubject()).willReturn(userInfoSubject);
        given(mockRootModel.getAvatarSubject()).willReturn(avatarSubject);

        JobManagerCallbackAdapter callbackAdapter = Whitebox.getInternalState(mRootPresenter, "mJobManagerAppCallback");

        Consumer<Integer> rxNextCall = n -> {
            for (int i = 0; i < n; i++) {
                callbackAdapter.onAfterJobRun(mockBaseJob, RESULT_SUCCEED);
                messageSubject.onNext(messageDto);
                errorSubject.onNext(errorDto);
                userInfoSubject.onNext(userInfoDto);
                avatarSubject.onNext(avatarUrl);
                progressSubject.onNext(true);
            }
        };

        Consumer<Integer> rxHandlerChecking = n -> {
            then(mockRootActivity).should(times(n)).showMessage(messageDto);
            then(mockRootActivity).should(times(n)).showError(errorDto);
            then(mockRootActivity).should(times(n)).showNavigationUserName(userInfoDto.getName());
            then(mockRootActivity).should(times(n)).showNavigationAvatar(avatarUrl);
        };

        //region ================= step 1 =================
        //when
        mRootPresenter.takeView(mockRootActivity);

        int count = 10;
        rxNextCall.accept(10);
        mTestScheduler.advanceTimeBy(1, TimeUnit.SECONDS);

        //then
        Assert.assertEquals(mockRootActivity, Whitebox.<RootActivity>invokeMethod(mRootPresenter, "getView"));

        then(mockJobManager).should(times(1)).addCallback(callbackAdapter);

        then(mockRootModel).should(times(1)).requestAvatar();
        then(mockRootModel).should(times(1)).requestUserInfo();

        rxHandlerChecking.accept(count);
        //endregion

        //region ================= step 2 =================
        //when
        mRootPresenter.dropView(mockRootActivity);

        rxNextCall.accept(1);
        mTestScheduler.triggerActions();

        //then
        Assert.assertNull(Whitebox.<RootActivity>invokeMethod(mRootPresenter, "getView"));

        then(mockJobManager).should(times(1)).removeCallback(callbackAdapter);

        rxHandlerChecking.accept(count + 0);
        //endregion
    }

    @Test
    public void logout_test() throws Exception {
        //given
        mRootPresenter = spy(mRootPresenter);
        doReturn(mockRootActivity).when(mRootPresenter, "getView");
        given(Flow.get(mockRootActivity)).willReturn(mockFlow);

        //when
        mRootPresenter.logout();

        //then
        then(mockRootModel).should(times(1)).logout();
        then(mockFlow).should(times(1)).replaceHistory(new CatalogScreen(), Direction.FORWARD);
    }

    @Test
    public void checkPermissions_granted_test() throws Exception {
        checkPermissions(true, READ_EXTERNAL_STORAGE);
    }

    @Test
    public void checkPermissions_denied_test() throws Exception {
        checkPermissions(false, WRITE_EXTERNAL_STORAGE, CAMERA);
    }

    private void checkPermissions(boolean isGranted, String... permissions) throws Exception {
        //given
        mRootPresenter = spy(mRootPresenter);
        doReturn(mockRootActivity).when(mRootPresenter, "getView");
        given(ContextCompat.checkSelfPermission(mockRootActivity, READ_EXTERNAL_STORAGE)).willReturn(PERMISSION_GRANTED);
        given(ContextCompat.checkSelfPermission(mockRootActivity, CAMERA)).willReturn(PERMISSION_GRANTED);
        given(ContextCompat.checkSelfPermission(mockRootActivity, WRITE_EXTERNAL_STORAGE)).willReturn(PERMISSION_DENIED);

        //when //then
        Assert.assertEquals(isGranted, mRootPresenter.checkPermissions(permissions));
    }

    @Test
    public void navigationBuilder_1_test() throws Exception {
        navigationBuilder("title", null, mockViewPager, mockScreenMenu, 2, true);
    }

    @Test
    public void navigationBuilder_2_test() throws Exception {
        navigationBuilder(null, R.string.title, null, mockScreenMenu, 1, false);
    }

    private void navigationBuilder(String title, Integer titleRes, ViewPager pager, ScreenMenu menu, int historySize, boolean isUserAuth) throws Exception {
        //given
        String emptyTitle = "some title";
        String titleFromRes = "title from res";

        mRootPresenter = spy(mRootPresenter);
        doReturn(mockRootActivity).when(mRootPresenter, "getView");
        given(mockRootActivity.getNavigationView()).willReturn(mockNavigationView);
        given(mockRootActivity.getString(anyInt())).willReturn(titleFromRes);
        given(Flow.get(mockRootActivity)).willReturn(mockFlow);

        given(mockFlow.getHistory()).willReturn(mockHistory);

        given(mockHistory.size()).willReturn(historySize);
        given(mockRootModel.isUserAuth()).willReturn(isUserAuth);

        doNothing().when(NavigationDrawerHelper.class, "prepareNavigationMenu", any(NavigationView.class), any(History.class), anyBoolean());

        //when
        RootPresenter.NavigationBuilder navigationBuilder = mRootPresenter.createNavigationBuilder();
        if (title != null) {
            navigationBuilder.setTitle(title);
        }
        if (titleRes != null) {
            navigationBuilder.setTitle(titleRes);
        }
        if (menu != null) {
            navigationBuilder.setMenu(menu);
        }
        if (pager != null) {
            navigationBuilder.setTabs(pager);
        }

        navigationBuilder.build();

        //then
        String name = emptyTitle;
        if (titleRes != null) {
            name = titleFromRes;
        } else if (title != null) {
            name = title;
        }
        then(mockRootActivity).should(times(1)).setTitle(name);

        verifyStatic(times(1));
        NavigationDrawerHelper.prepareNavigationMenu(mockNavigationView, mockHistory, isUserAuth);

        then(mockRootActivity).should(times(1)).setupDrawerBackArrow(historySize > 1);

        then(mockRootActivity).should(times(pager != null ? 1 : 0)).setTabLayout(pager);
        then(mockRootActivity).should(times(pager != null ? 0 : 1)).removeTabLayout();

        then(mockRootActivity).should(times(1)).setScreenMenu(menu);
        then(mockRootActivity).should(times(1)).invalidateOptionsMenu();
    }

    @Test
    public void requestPermissions_old_sdk_test() throws Exception {
        requestPermissions(Build.VERSION_CODES.KITKAT);
    }

    @Test
    public void requestPermissions_new_sdk_test() throws Exception {
        requestPermissions(Build.VERSION_CODES.M);
    }

    @SuppressLint("NewApi")
    private void requestPermissions(int sdkVersion) throws Exception {
        //given
        mRootPresenter = spy(mRootPresenter);

        int requestCode = 1;
        String[] permissions = new String[]{WRITE_EXTERNAL_STORAGE, CAMERA};
        Whitebox.setInternalState(Build.VERSION.class, "SDK_INT", sdkVersion);
        doReturn(mockRootActivity).when(mRootPresenter, "getView");

        boolean isExpected = sdkVersion >= Build.VERSION_CODES.M;

        //when
        boolean isRequested = mRootPresenter.requestPermissions(permissions, requestCode);

        //then
        then(mockRootActivity).should(times(isExpected ? 1 : 0)).requestPermissions(permissions, requestCode);
        Assert.assertEquals(isExpected, isRequested);
    }


    @Test
    public void vkSdkLogin_test() throws Exception {

        //given
        mRootPresenter = spy(mRootPresenter);
        doReturn(mockRootActivity).when(mRootPresenter, "getView");

        //when
        mRootPresenter.vkSdkLogin();

        //then
        verifyStatic(times(1));
        VKSdk.login(mockRootActivity, VKScope.EMAIL);
    }

    @Test
    public void fbLogin_test() throws Exception {

        //given
        mRootPresenter = spy(mRootPresenter);
        doReturn(mockRootActivity).when(mRootPresenter, "getView");

        //when
        mRootPresenter.fbLogin(mockFbLoginManager);

        //then
        then(mockFbLoginManager).should(times(1)).logInWithReadPermissions(mockRootActivity, Arrays.asList("email"));
    }

    @Test
    public void twLogin_test() throws Exception {

        //given
        mRootPresenter = spy(mRootPresenter);
        doReturn(mockRootActivity).when(mRootPresenter, "getView");

        //when
        mRootPresenter.twLogin(mockTwAuthClient, mockTwSessionCallback);

        //then
        then(mockTwAuthClient).should(times(1)).authorize(mockRootActivity, mockTwSessionCallback);
    }

//endregion
}