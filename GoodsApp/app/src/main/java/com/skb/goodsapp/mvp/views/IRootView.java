package com.skb.goodsapp.mvp.views;

import android.view.View;

/**
 * методы необходимые для root activity в подобных приложениях
 */

public interface IRootView extends IMessagesSupportView, IProgressSupportView {

    //void setCurrentScreen(AbstractScreen screen);

    View getCurrentView();

    void showNavigationAvatar(String url);

    void showNavigationUserName(String name);

    void setupDrawerBackArrow(boolean isEnabled);


}
