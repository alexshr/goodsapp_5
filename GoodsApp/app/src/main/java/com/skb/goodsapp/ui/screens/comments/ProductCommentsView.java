package com.skb.goodsapp.ui.screens.comments;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.ProductCommentRealm;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.mvp.views.IBackPressedView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.RealmResults;

public class ProductCommentsView extends RelativeLayout implements IBackPressedView {

    @BindView(R.id.comments_list)
    RecyclerView mRecyclerView;

    @BindView(R.id.add_comment_fab)
    FloatingActionButton mAddCommentBtn;

    @OnClick(R.id.add_comment_fab)
    public void onClick(View view) {
        showAddCommentDialog();
    }

    @Inject
    ProductCommentsScreen.ProductCommentsPresenter mPresenter;

    private Context mContext;

    //for dialog control
    private AlertDialog mDialog;
    AppCompatRatingBar mDialogRatingBar;
    TextInputEditText mDialogCommentEt;


    private ProductCommentsRealmAdapter mAdapter;

    public ProductCommentsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        if (!isInEditMode()) {
            DaggerService.<ProductCommentsScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
            if (mDialog != null && mDialog.isShowing()) {
                mPresenter.saveDialogState(mDialogRatingBar.getRating(),mDialogCommentEt.getText().toString());
                mDialog.dismiss();
            }else{
                mPresenter.resetDialogState();
            }
        }
    }

    public void showComments(RealmResults<ProductCommentRealm> comments) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new ProductCommentsRealmAdapter(mContext, comments);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void showAddCommentDialog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());

        View dialogView = layoutInflater.inflate(R.layout.dialog_comment, null);
        mDialogRatingBar = (AppCompatRatingBar) dialogView.findViewById(R.id.comment_rb);
        mDialogCommentEt = (TextInputEditText) dialogView.findViewById(R.id.comment);
        mDialogCommentEt.setText(mPresenter.mDialogComment);
        mDialogRatingBar.setRating(mPresenter.mDialogRating);

        dialogBuilder.setTitle(R.string.rate_product)
                .setView(dialogView)
                .setPositiveButton(R.string.save, (dialog, which) -> mPresenter.confirmDialog(mDialogRatingBar.getRating(), mDialogCommentEt.getText().toString()))
                .setNegativeButton("Отмена", (dialog, which) -> {
                    mPresenter.cancelDialog();
                    dialog.cancel();
                });
        mDialog = dialogBuilder.show();

    }


    @Override
    public boolean onViewBackPressed() {
        return false;
    }

}
