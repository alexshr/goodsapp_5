package com.skb.goodsapp.mvp.presenters;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.callback.JobManagerCallbackAdapter;
import com.facebook.login.LoginManager;
import com.skb.goodsapp.AppConfig;
import com.skb.goodsapp.MyApplication;
import com.skb.goodsapp.R;
import com.skb.goodsapp.data.rx.SimpleDisposableObserver;
import com.skb.goodsapp.data.storage.dto.ActivityPermissionsResultDto;
import com.skb.goodsapp.data.storage.dto.ActivityResultDto;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.dto.UserInfoDto;
import com.skb.goodsapp.flow.NavigationDrawerHelper;
import com.skb.goodsapp.flow.ScreenMenu;
import com.skb.goodsapp.jobs.BaseJob;
import com.skb.goodsapp.mvp.models.RootModel;
import com.skb.goodsapp.ui.activities.RootActivity;
import com.skb.goodsapp.ui.screens.catalog.CatalogScreen;
import com.skb.goodsapp.utils.FileUtils;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;

import java.io.File;
import java.util.Arrays;

import javax.inject.Inject;

import flow.Direction;
import flow.Flow;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.BehaviorSubject;
import mortar.Presenter;
import mortar.bundler.BundleService;

import static com.birbit.android.jobqueue.callback.JobManagerCallback.RESULT_CANCEL_CANCELLED_VIA_SHOULD_RE_RUN;
import static com.birbit.android.jobqueue.callback.JobManagerCallback.RESULT_CANCEL_CANCELLED_WHILE_RUNNING;
import static com.birbit.android.jobqueue.callback.JobManagerCallback.RESULT_CANCEL_REACHED_RETRY_LIMIT;
import static com.birbit.android.jobqueue.callback.JobManagerCallback.RESULT_CANCEL_SINGLE_INSTANCE_WHILE_RUNNING;
import static com.birbit.android.jobqueue.callback.JobManagerCallback.RESULT_SUCCEED;
import static com.skb.goodsapp.ConstantManager.LOG_TAG;
import static com.skb.goodsapp.ConstantManager.LOG_UPLOAD;
import static com.skb.goodsapp.ConstantManager.REQUEST_CAMERA_CODE;
import static com.skb.goodsapp.ConstantManager.REQUEST_GALLERY_CODE;


public class RootPresenter extends Presenter<RootActivity> {
    @Inject
    RootModel mRootModel;

    @Inject
    JobManager mJobManager;

    private CompositeDisposable mCompositeDisposable;

    private JobManagerCallbackAdapter mJobManagerAppCallback = new JobManagerCallbackAdapter() {
        @Override
        public void onAfterJobRun(@NonNull Job job, int resultCode) {
            Log.d(LOG_TAG, "JobManagerAppCallback runAfterJob job=" + job + " resultCode=" + resultCode);
            doAfterJob((BaseJob) job, resultCode);
        }
    };


    private BehaviorSubject<ActivityResultDto> mActivityResultSubject = BehaviorSubject.create();
    private BehaviorSubject<ActivityPermissionsResultDto> mActivityPermissionsResultPublishSubject = BehaviorSubject.create();
    private String mPhotoFileUrl;

    public Observable<String> getActivityResultAvatarUrlSubject() {
        return mActivityResultSubject
                .filter(res -> res.getRequestCode() == REQUEST_GALLERY_CODE
                        || res.getRequestCode() == REQUEST_CAMERA_CODE)
                .filter(res -> {
                    if (res.getResultCode() != Activity.RESULT_OK) {
                        mRootModel.sendError(new MessageDto(R.string.avatar_not_created));
                        return false;
                    } else return true;
                })
                .filter(res -> {
                    if ((res.getRequestCode() == REQUEST_GALLERY_CODE && res.getData() == null)
                            || (res.getRequestCode() == REQUEST_CAMERA_CODE && mPhotoFileUrl == null)) {
                        mRootModel.sendError(new MessageDto(R.string.avatar_not_created));
                        return false;
                    } else return true;
                })
                .map(res ->
                        res.getRequestCode() == REQUEST_GALLERY_CODE ? res.getData().toString() : mPhotoFileUrl
                );
    }

    public Observable<ActivityPermissionsResultDto> getActivityPermissionsResultPublishSubject() {
        return mActivityPermissionsResultPublishSubject;
    }

    public RootPresenter() {
        MyApplication.getDaggerComponent().inject(this);
    }

    @Override
    protected BundleService extractBundleService(RootActivity view) {
        return BundleService.getBundleService(view);
    }

    @Override
    protected void onLoad(Bundle savedInstanceState) {
        super.onLoad(savedInstanceState);
        mJobManager.addCallback(mJobManagerAppCallback);

        mCompositeDisposable = new CompositeDisposable();
        mCompositeDisposable.add(mRootModel.getMessageSubject()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mes -> {
                    getView().showMessage(mes);
                }));
        mCompositeDisposable.add(mRootModel.getErrorSubject()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<MessageDto>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull MessageDto mes) throws Exception {
                        RootPresenter.this.getView().showError(mes);
                    }
                }));


        mCompositeDisposable.add(mRootModel.getUserInfoSubject().subscribeWith(new SimpleDisposableObserver<UserInfoDto>() {
            @Override
            public void onNext(UserInfoDto userInfoDto) {
                getView().showNavigationUserName(userInfoDto.getName());
            }
        }));

        mCompositeDisposable.add(mRootModel.getProgressSubject()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SimpleDisposableObserver<Boolean>() {
                    @Override
                    public void onNext(Boolean isLoading) {
                        getView().setupProgress(isLoading);
                    }
                }));

        mCompositeDisposable.add(mRootModel.getAvatarSubject()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SimpleDisposableObserver<String>() {
                    @Override
                    public void onNext(String avatarUrl) {
                        getView().showNavigationAvatar(avatarUrl);
                    }
                }));

        mRootModel.requestAvatar();
        mRootModel.requestUserInfo();
    }

    @Override
    public void dropView(RootActivity view) {
        super.dropView(view);
        mJobManager.removeCallback(mJobManagerAppCallback);
        mCompositeDisposable.dispose();
    }

    public BehaviorSubject<ActivityResultDto> getActivityResultSubject() {
        return mActivityResultSubject;
    }


    public void logout() {
        mRootModel.logout();
        //replaceHistory с Direction.FORWARD заново загружает экран,
        //даже если он повторяет предыдущий
        Flow.get(getView()).replaceHistory(new CatalogScreen(), Direction.FORWARD);
    }

    public Uri createFileForPhoto() {
        File file = FileUtils.createFileForPhoto();

        mPhotoFileUrl = Uri.fromFile(file).toString();

        return FileProvider.getUriForFile(getView(),
                AppConfig.FILE_PROVIDER_AUTHORITY,
                file);
    }


    public void startActivityForResult(Intent intent, int requestCode) {
        getView().startActivityForResult(intent, requestCode);
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mActivityPermissionsResultPublishSubject.onNext(new ActivityPermissionsResultDto(requestCode, permissions, grantResults));
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(LOG_UPLOAD, "onActivityResult requestCode=" + requestCode + " resultCode=" + resultCode);
        mActivityResultSubject.onNext(new ActivityResultDto(requestCode, resultCode, data));
    }

    public RootModel getRootModel() {
        return mRootModel;
    }

    public boolean checkPermissions(@NonNull String... permissions) {
        boolean allGranted = true;
        for (String permission : permissions) {
            int selfPermission = ContextCompat.checkSelfPermission(getView(), permission);
            if (selfPermission != PackageManager.PERMISSION_GRANTED) {
                allGranted = false;
                break;
            }
        }
        return allGranted;
    }

    public boolean requestPermissions(@NonNull String[] permissions, int requestCode) {
        boolean isRequested = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getView().requestPermissions(permissions, requestCode);
            isRequested = true;
        }
        return isRequested;
    }


    public void closeRealm() {
        mRootModel.getRealmManager().closeRealm();
    }

    public NavigationBuilder createNavigationBuilder() {
        return new NavigationBuilder();
    }

    public void showMessage(MessageDto mes) {
        getView().showMessage(mes);
    }

    public void showError(MessageDto mes) {
        getView().showError(mes);
    }

    private void doAfterJob(@NonNull BaseJob job, int resultCode) {
        Log.d(LOG_TAG, "JobManagerAppCallback runAfterJob job=" + job + " resultCode=" + resultCode);
        switch (resultCode) {
            case RESULT_SUCCEED:
                if (job.getSuccessMes() != null) {
                    showMessage(job.getSuccessMes());
                }
                break;
            case RESULT_CANCEL_CANCELLED_VIA_SHOULD_RE_RUN:
            case RESULT_CANCEL_REACHED_RETRY_LIMIT:
            case RESULT_CANCEL_CANCELLED_WHILE_RUNNING:
            case RESULT_CANCEL_SINGLE_INSTANCE_WHILE_RUNNING:
                if (job.getCancelMes() != null) {
                    showError(job.getCancelMes());
                }
        }
    }

    public void vkSdkLogin() {
        VKSdk.login(getView(), VKScope.EMAIL);
    }

    public void fbLogin(LoginManager fbLoginManager) {
        fbLoginManager.logInWithReadPermissions(getView(), Arrays.asList("email"));
    }

    public void twLogin(TwitterAuthClient client, Callback<TwitterSession> twLoginCallback) {
        client.authorize(getView(), twLoginCallback);
    }


    public class NavigationBuilder {
        private CharSequence title = "some title";
        private ScreenMenu menu;
        private ViewPager pager;
        private Integer titleRes;

        public NavigationBuilder() {
            this.menu = new ScreenMenu(getView());
        }

        public NavigationBuilder setTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        public NavigationBuilder setTitle(int titleRes) {
            this.titleRes = titleRes;
            return this;
        }

        public NavigationBuilder setMenu(ScreenMenu menu) {
            this.menu = menu;
            return this;
        }

        public NavigationBuilder setTabs(ViewPager pager) {
            this.pager = pager;
            return this;
        }

        public void build() {
            RootActivity activity = getView();

            if (titleRes != null) {
                title = activity.getString(titleRes);
            }

            activity.setTitle(title);

            NavigationDrawerHelper.prepareNavigationMenu(activity.getNavigationView(), Flow.get(getView()).getHistory(), mRootModel.isUserAuth());

            activity.setupDrawerBackArrow(Flow.get(getView()).getHistory().size() > 1);

            if (pager != null) {
                activity.setTabLayout(pager);
            } else {
                activity.removeTabLayout();
            }
            activity.setScreenMenu(menu);
            activity.invalidateOptionsMenu();
        }
    }

}
