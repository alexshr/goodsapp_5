package com.skb.goodsapp.ui.screens.basket;

import android.os.Bundle;

import com.skb.goodsapp.R;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.AbstractScreen;
import com.skb.goodsapp.flow.Screen;
import com.skb.goodsapp.mvp.models.BasketModel;
import com.skb.goodsapp.mvp.presenters.RootPresenter;
import com.skb.goodsapp.ui.activities.RootActivity;
import com.skb.goodsapp.ui.screens.product_detail.ProductDetailScreen;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;
import javax.inject.Scope;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import mortar.ViewPresenter;


@Screen(R.layout.screen_basket)
public class BasketScreen extends AbstractScreen {

    @Override
    public Object[] getModules() {
        return new Object[]{new Module()};
    }

    @Override
    public Class getComponentClass() {
        return Component.class;
    }


    //region ============================== DI ==============================

    @dagger.Module
    public class Module {

        @BasketScope
        @Provides
        BasketModel provideModel() {
            return new BasketModel();
        }

        @BasketScope
        @Provides
        BasketPresenter providePresenter() {
            return new BasketPresenter();
        }

    }

    @BasketScope
    @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
    public interface Component {
        void inject(BasketPresenter presenter);

        void inject(BasketView view);
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface BasketScope {
    }

    //endregion

    //region ============================== Presenter ==============================

    public class BasketPresenter extends ViewPresenter<BasketView> {

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        BasketModel mBasketModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            setupNavigation();
            getView().initView(mBasketModel.getBasket());
        }


        private void setupNavigation() {
            mRootPresenter.createNavigationBuilder()
                    .setTitle(R.string.title_basket)
                    .build();
        }

        public void deleteFromBasket(String productId) {
            mBasketModel.deleteFromBasket(productId);
        }


        public void goToProductDetail(String productId) {
            Flow.get(getView().getContext()).set(new ProductDetailScreen(productId));
        }

        public void updateBasketFromUser(String productId, int count) {
            mBasketModel.updateBasketFromUser(productId, count);
        }
    }
}
