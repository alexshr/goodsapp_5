package com.skb.goodsapp.mvp.views;

import android.support.design.widget.NavigationView;

public interface INavigatedView {
    NavigationView getNavigationView();
}
