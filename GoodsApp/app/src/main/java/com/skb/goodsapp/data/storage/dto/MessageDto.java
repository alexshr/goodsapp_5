package com.skb.goodsapp.data.storage.dto;

import java.util.Arrays;

/**
 * Created by pc on 21.04.2017.
 */

public class MessageDto {
    String text;
    Integer messageRes;
    Object[] args = new Object[0];

    public MessageDto(int res, Object... args) {
        this.messageRes = res;
        this.args = args;
    }

    public MessageDto(Integer messageRes) {
        this.messageRes = messageRes;
    }

    public MessageDto(String text) {
        this.text = text;
    }

    public MessageDto() {
    }

    /*
        public boolean hasMessage(){
            return text!=null||messageRes!=null;
        }
    */
    public int getMessageRes() {
        return messageRes;
    }

    public Object[] getArgs() {
        return args;
    }

    public String getText() {
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageDto that = (MessageDto) o;

        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        if (messageRes != null ? !messageRes.equals(that.messageRes) : that.messageRes != null)
            return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(args, that.args);

    }

}
