package com.skb.goodsapp.ui.screens.product_detail;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skb.goodsapp.R;
import com.skb.goodsapp.flow.AbstractScreen;
import com.skb.goodsapp.mortar.ScreenScoper;
import com.skb.goodsapp.ui.screens.comments.ProductCommentsScreen;
import com.skb.goodsapp.ui.screens.description.ProductDescriptionScreen;

import mortar.MortarScope;


public class ProductDetailAdapter extends PagerAdapter {

    private static final int TABS_COUNT = 2;
    private final String[] mTabsTitles;

    private String mProductId;

    public ProductDetailAdapter(Context context, String productId) {
        mTabsTitles = context.getResources().getStringArray(R.array.detail_tabs);
        mProductId = productId;
    }

    @Override
    public int getCount() {
        return TABS_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabsTitles[position];
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        AbstractScreen screen = position == 0 ? new ProductDescriptionScreen(mProductId) : new ProductCommentsScreen(mProductId);

        MortarScope mortarScope=ScreenScoper.provideScopeFromParentContext(screen,container.getContext());
        Context context=mortarScope.createContext(container.getContext());
        View view = LayoutInflater.from(context).inflate(screen.getLayoutId(), container, false);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        MortarScope screenScope = MortarScope.getScope(((View) object).getContext());
        container.removeView((View) object);
        screenScope.destroy();
    }
}
