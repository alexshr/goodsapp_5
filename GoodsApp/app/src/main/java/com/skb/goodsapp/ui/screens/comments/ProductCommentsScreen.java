package com.skb.goodsapp.ui.screens.comments;

import android.os.Bundle;
import android.util.Log;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.network.ProductCommentRes;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.AbstractScreen;
import com.skb.goodsapp.flow.Screen;
import com.skb.goodsapp.mvp.models.ProductCommentsModel;
import com.skb.goodsapp.ui.screens.product_detail.ProductDetailScreen;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;
import javax.inject.Scope;

import dagger.Provides;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import mortar.MortarScope;
import mortar.ViewPresenter;

import static com.skb.goodsapp.ConstantManager.LOG_TAG;

@Screen(R.layout.screen_product_comments)
public class ProductCommentsScreen extends AbstractScreen {
    private final String mProductId;

    //private ProductDetailScreen mProductDetailScreen;

    public ProductCommentsScreen(String productId) {
        mProductId = productId;
        setParentScopeScreen(new ProductDetailScreen());
    }

    @Override
    public Object[] getModules() {
        return new Object[]{new Module()};
    }

    @Override
    public Class getComponentClass() {
        return Component.class;
    }


    //region ============================== DI ==============================

    @dagger.Component(dependencies = ProductDetailScreen.Component.class, modules = Module.class)
    @ProductCommentsScope
    public interface Component {
        void inject(ProductCommentsPresenter presenter);

        void inject(ProductCommentsView view);
    }

    @dagger.Module
    public class Module {
        @Provides
        @ProductCommentsScope
        ProductCommentsPresenter provideCommentsPresenter() {
            return new ProductCommentsPresenter();
        }

        @Provides
        @ProductCommentsScope
        ProductCommentsModel provideCommentsModel() {
            return new ProductCommentsModel();
        }
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface ProductCommentsScope {
    }

    //endregion

    //region ============================== Presenter ==============================


    public class ProductCommentsPresenter extends ViewPresenter<ProductCommentsView> {

        //@Inject
        //ProductDetailScreen.ProductDetailPresenter mProductDetailPresenter;

        @Inject
        ProductCommentsModel mProductCommentsModel;

        //private RootPresenter mRootPresenter;

        //private ProductRealm mProduct;

        /* To prevent request duplication during screen rotation.
        It is to delete only after getting any result*/
        private Observable<ProductCommentRes> mRetrofitPostCommentCacheObservable;

        private CompositeDisposable mCompositeDisposable;

        //dialog state
        String mDialogComment;
        float mDialogRating;
        boolean mIsDialogActive;

        @Override
        protected void onEnterScope(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
            //mCatalogModel = mProductDetailPresenter.getCatalogPresenter().getCatalogModel();
            //mRootPresenter = mProductDetailPresenter.getCatalogPresenter().getRootPresenter();
        }

        @Override
        public void dropView(ProductCommentsView view) {
            super.dropView(view);
            mCompositeDisposable.dispose();
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            mCompositeDisposable = new CompositeDisposable();

            if (mRetrofitPostCommentCacheObservable != null) {
                //обработать ответ запроса, который не успели обработать (до поворота экрана)
                Log.d(LOG_TAG, "ProductCommentScreen повторная подписка (результат не был обработан)");
                //subscribeToPostCommentObservable();
            }

            getView().showComments(mProductCommentsModel.getProductComments(mProductId));

            if (mIsDialogActive) {
                getView().showAddCommentDialog();
            }
        }


        public void confirmDialog(float rating, String comment) {
            mProductCommentsModel.postComment(rating, comment, mProductId);
            resetDialogState();
        }

        public void cancelDialog() {
            resetDialogState();
        }

        public void resetDialogState() {
            mDialogRating = 0f;
            mDialogComment = "";
            mIsDialogActive = false;
        }

        public void saveDialogState(float rating, String comment) {
            mDialogRating = rating;
            mDialogComment = comment;
            mIsDialogActive = true;
        }
    }
    //endregion
}
