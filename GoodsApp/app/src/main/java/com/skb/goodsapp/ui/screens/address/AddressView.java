package com.skb.goodsapp.ui.screens.address;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.UserAddressRealm;
import com.skb.goodsapp.di.DaggerService;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressView extends RelativeLayout {

    //region ================= butterknife =================
    @BindView(R.id.add_button)
    Button mAddButton;
    @BindView(R.id.name)
    TextInputEditText mNameEt;
    @BindView(R.id.street)
    TextInputEditText mStreetEt;
    @BindView(R.id.house)
    TextInputEditText mHouseEt;
    @BindView(R.id.apartment)
    TextInputEditText mApartmentEt;
    @BindView(R.id.floor)
    TextInputEditText mFloorEt;
    @BindView(R.id.comment)
    TextInputEditText mCommentEt;

    @OnClick(R.id.add_button)
    void clickSaveAddress() {
        mPresenter.onSaveAddressClicked();
    }
    //endregion

    @Inject
    AddressScreen.AddressPresenter mPresenter;

    public AddressView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            DaggerService.<AddressScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }


    public void fillAddress(UserAddressRealm userAddress) {

        userAddress.setName(mNameEt.getText().toString());
        userAddress.setStreet(mStreetEt.getText().toString());
        userAddress.setHouse(mHouseEt.getText().toString());
        userAddress.setApartment(mApartmentEt.getText().toString());
        userAddress.setFloor(mFloorEt.getText().toString());
        if (mCommentEt.getText() != null && mCommentEt.getText().length() > 0) {
            userAddress.setComment(mCommentEt.getText().toString());
        }
    }


    public void initView(UserAddressRealm address) {

        mNameEt.setText(address.getName());
        mStreetEt.setText(address.getStreet());
        mHouseEt.setText(address.getHouse());
        mApartmentEt.setText(address.getApartment());
        mFloorEt.setText(address.getFloor());
        if (address.getComment() != null) {
            mCommentEt.setText(address.getComment());
        }
    }

}
