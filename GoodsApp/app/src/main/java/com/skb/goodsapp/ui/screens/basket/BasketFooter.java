package com.skb.goodsapp.ui.screens.basket;


import com.skb.goodsapp.data.storage.realm.BasketProductRealm;

import io.realm.RealmResults;

import static com.skb.goodsapp.AppConfig.DISCOUNT_PRC;
import static com.skb.goodsapp.AppConfig.MIN_COST_TO_DISCOUNT;

public class BasketFooter {

    int mProductCount;
    int mCost;
    int mDiscount;

    public BasketFooter(RealmResults<BasketProductRealm> basketProducts) {

        for (BasketProductRealm basketProduct : basketProducts) {
            mProductCount += basketProduct.getCount();
            mCost = basketProduct.getProduct().getPrice() * mProductCount;
        }
        if (mCost > MIN_COST_TO_DISCOUNT) {
            mDiscount = mCost * DISCOUNT_PRC / 100;
            mCost -= mDiscount;
        }

    }

    public int getProductCount() {
        return mProductCount;
    }

    public int getDiscount() {
        return mDiscount;
    }

    public int getCost() {
        return mCost;
    }
}
