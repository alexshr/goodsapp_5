package com.skb.goodsapp.ui.screens.account;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.daimajia.swipe.implments.SwipeItemRecyclerMangerImpl;
import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.UserAddressRealm;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class AddressRealmAdapter extends RecyclerSwipeAdapter<AddressRealmAdapter.ViewHolder> {

    public SwipeItemRecyclerMangerImpl mItemManger = new SwipeItemRecyclerMangerImpl(this);

    private RealmResults<UserAddressRealm> mAdapterData;
    private Context mContext;
    private ActionItemListener mActionListener;

    private Integer mPositionToDel;

    private RealmChangeListener mRealmListener = new RealmChangeListener() {
        @Override
        public void onChange(Object res) {
            if (mPositionToDel == null) {
                notifyDataSetChanged();
            } else {
                notifyItemRemoved(mPositionToDel);
                notifyItemRangeChanged(mPositionToDel, mAdapterData.size());
                mItemManger.closeAllItems();
            }
        }
    };

    public AddressRealmAdapter(@Nullable RealmResults<UserAddressRealm> data, ActionItemListener actionListener) {
        mAdapterData = data;
        mActionListener = actionListener;
    }

    @Override
    public void onAttachedToRecyclerView(final RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        if (isDataValid()) {
            //noinspection ConstantConditions
            mAdapterData.addChangeListener(mRealmListener);
        }
    }

    @Override
    public void onDetachedFromRecyclerView(final RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        if (isDataValid()) {
            //noinspection ConstantConditions
            mAdapterData.removeChangeListener(mRealmListener);
        }
    }

    private boolean isDataValid() {
        return mAdapterData != null && mAdapterData.isValid();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.mSwipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

        //не нужно, если есть layout_gravity
        //holder.mSwipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.mSwipeLayout.findViewById(R.id.bottom_delete_wrapper));

        UserAddressRealm userAddress = mAdapterData.get(position);


        holder.mAddressLabel.setText(mContext.getString(R.string.address_name_format, userAddress.getName()));
        holder.mAddress.setText(mContext.getString(R.string.address_format,
                userAddress.getStreet(), userAddress.getHouse(), userAddress.getApartment(), userAddress.getFloor()));

        if (userAddress.getComment() != null && !userAddress.getComment().isEmpty()) {
            holder.mComment.setText(userAddress.getComment());
            holder.mCommentLayout.setVisibility(View.VISIBLE);
        } else {
            holder.mCommentLayout.setVisibility(View.GONE);
        }

        holder.mSwipeLayout.findViewById(R.id.bottom_delete_wrapper).setOnClickListener(view -> holder.mSwipeLayout.close());

        holder.mSwipeLayout.findViewById(R.id.bottom_edit_wrapper).setOnClickListener(view -> {
            holder.mSwipeLayout.close();
            mActionListener.onEditClicked(userAddress.getId());
        });

        holder.mDeleteBtn.setOnClickListener(v -> {
            mItemManger.removeShownLayouts(holder.mSwipeLayout);
            mPositionToDel = position;
            mActionListener.onDeleteClicked(userAddress.getId());
        });

        mItemManger.bindView(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return mAdapterData.size();
    }


    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        //region ================= butterknife =================
        @BindView(R.id.swipe)
        SwipeLayout mSwipeLayout;
        @BindView(R.id.name)
        TextView mAddressLabel;
        @BindView(R.id.address)
        TextView mAddress;
        @BindView(R.id.comment)
        TextView mComment;
        @BindView(R.id.comment_layout)
        LinearLayout mCommentLayout;
        @BindView(R.id.delete_btn)
        Button mDeleteBtn;
        //endregion

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface ActionItemListener {
        public void onDeleteClicked(long id);

        public void onEditClicked(long id);
    }
}
