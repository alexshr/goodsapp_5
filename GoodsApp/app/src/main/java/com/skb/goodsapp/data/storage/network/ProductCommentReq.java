package com.skb.goodsapp.data.storage.network;

import com.squareup.moshi.Json;

public class ProductCommentReq {

    private int remoteId = 911;//заглушка бессмысленного поле для серверного API

    //на данный момент просто заглушка
    private String avatar="http://skill-branch.ru/img/app/avatar-1.png";

    @Json(name = "raiting")
    private float rating;
    private String comment;
    private String userName;
    private boolean active=true;


    public ProductCommentReq(float rating, String comment, String userName, String avatar) {
        this.avatar = avatar;
        this.rating = rating;
        this.comment = comment;
        this.userName = userName;
    }


    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductCommentReq that = (ProductCommentReq) o;

        if (remoteId != that.remoteId) return false;
        if (Float.compare(that.rating, rating) != 0) return false;
        if (active != that.active) return false;
        if (avatar != null ? !avatar.equals(that.avatar) : that.avatar != null) return false;
        if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
        return userName != null ? userName.equals(that.userName) : that.userName == null;

    }

}
