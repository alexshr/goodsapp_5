package com.skb.goodsapp.data.network;

import com.skb.goodsapp.AppConfig;
import com.skb.goodsapp.data.storage.network.AvatarUrlRes;
import com.skb.goodsapp.data.storage.network.ProductCommentReq;
import com.skb.goodsapp.data.storage.network.ProductCommentRes;
import com.skb.goodsapp.data.storage.network.ProductRes;
import com.skb.goodsapp.data.storage.network.UserLoginReq;
import com.skb.goodsapp.data.storage.network.UserRes;
import com.skb.goodsapp.data.storage.network.UserSocReq;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface RestService {

    @GET("products")
    Observable<Response<List<ProductRes>>> getProductsObservable(
            @Header(AppConfig.IF_MODIFIED_SINCE_HEADER) String lastEntityUpdate);
/*
    @POST("products/{id}/comments")
    Observable<Response<ProductCommentRes>> postComment(@Path("id") String id,
                                                        @Body ProductCommentReq commentReq);
                                                        */

    @POST("products/{id}/comments")
    Call<ProductCommentRes> postComment(@Path("id") String id,
                                        @Body ProductCommentReq commentReq);

    @Multipart
    @POST("avatar")
    Call<AvatarUrlRes> uploadUserAvatar(@Part MultipartBody.Part file);

    //спец адрес пока api не работает
    //@POST("https://private-8967d4-middleappskillbranch.apiary-mock.com/login")
    //Observable<Response<UserRes>> loginUser(@Body UserLoginReq userLoginReq);

    @POST("login")
    Observable<Response<UserRes>> loginUser(@Body UserLoginReq userLoginReq);

    //unsafe server api method
    @POST("socialLogin")
    Observable<Response<UserRes>> loginUser(@Body UserSocReq userSocReq);
}
