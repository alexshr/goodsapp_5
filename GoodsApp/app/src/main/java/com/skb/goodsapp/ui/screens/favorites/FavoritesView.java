package com.skb.goodsapp.ui.screens.favorites;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.FavoriteProductRealm;
import com.skb.goodsapp.di.DaggerService;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.RealmResults;

public class FavoritesView extends FrameLayout {

    @Inject
    FavoritesScreen.FavoritesPresenter mPresenter;


    //region ================= butterknife =================
    @BindView(R.id.favorite_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.empty_view)
    TextView mEmptyView;
    @BindView(R.id.view_switcher)
    ViewSwitcher mViewSwitcher;

    //endregion

    private FavoritesRealmAdapter mFavoritesAdapter;

    private RecyclerView.AdapterDataObserver mAdapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            if ((mViewSwitcher.getDisplayedChild() == 0 && mFavoritesAdapter.getItemCount() == 0)
                    || (mViewSwitcher.getDisplayedChild() == 1 && mFavoritesAdapter.getItemCount() > 0)) {
                mViewSwitcher.showNext();
            }
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            onChanged();
        }
    };


    private Unbinder mUnbinder;

    public FavoritesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        DaggerService.<FavoritesScreen.Component>getDaggerComponent(context).inject(this);
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mUnbinder = ButterKnife.bind(this);

    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mFavoritesAdapter.unregisterAdapterDataObserver(mAdapterDataObserver);
            mUnbinder.unbind();
            mPresenter.dropView(this);
        }
    }

    public void initView(RealmResults<FavoriteProductRealm> favorites) {

        mViewSwitcher.setInAnimation(AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_in));
        mViewSwitcher.setOutAnimation(AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_out));


        mFavoritesAdapter = new FavoritesRealmAdapter(getContext(), favorites, new FavoritesRealmAdapter.ActionItemListener() {
            @Override
            public void onFavoriteDeleted(String id) {
                mPresenter.deleteFavorite(id);
            }

            @Override
            public void onBasketClicked(String id, int count) {
                mPresenter.updateBasketFromUser(id, count);
            }

            @Override
            public void onImageClicked(String id) {
                mPresenter.goToProductDetail(id);
            }
        });

        mFavoritesAdapter.registerAdapterDataObserver(mAdapterDataObserver);

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        mRecyclerView.setLayoutManager(layoutManager);
        /*
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                layoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        */

        mRecyclerView.setAdapter(mFavoritesAdapter);

        mAdapterDataObserver.onChanged();
    }


}
