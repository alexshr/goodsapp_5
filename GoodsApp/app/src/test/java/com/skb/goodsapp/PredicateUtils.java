package com.skb.goodsapp;

import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;

/**
 * for AssertJ usage into TestObserver consumer (to have smart error messages)
 * AssertJ improves error messages
 */
public class PredicateUtils {
    public static <T> Predicate<T> check(Consumer<T> consumer) {
        return t -> {
            consumer.accept(t);
            return true;
        };
    }
}
