package com.skb.goodsapp.jobs;

public interface JobPriority {
    int LOW = 0;
    int MID = 500;
    int HIGH = 1000;
}
