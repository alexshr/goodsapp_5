package com.skb.goodsapp.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.skb.goodsapp.data.storage.network.UserRes;

import static com.skb.goodsapp.AppConfig.DEFAULT_LAST_MODIFIED_DATE;
import static com.skb.goodsapp.ConstantManager.LOG_UPLOAD;

public class PreferencesManager {

    private final static String AUTH_TOKEN_KEY = "AUTH_TOKEN_KEY";
    private final static String IS_ORDER_NOTIFICATION_KEY = "IS_ORDER_NOTIFICATION_KEY";
    private final static String IS_PROMO_NOTIFICATION_KEY = "IS_PROMO_NOTIFICATION_KEY";
    private final static String PRODUCT_LAST_MODIFIED_KEY = "PRODUCT_LAST_MODIFIED_KEY";

    private final static String PHONE_KEY = "PHONE_KEY";
    private final static String USER_NAME_KEY = "USER_NAME_KEY";
    private final static String AVATAR_KEY = "AVATAR_KEY";
    //private final static String AVATAR_LOCAL_KEY = "AVATAR_LOCAL_KEY";
    //private static final String AVATAR_SERVER_KEY = "AVATAR_SERVER_KEY";
    private static final String USER_ID_KEY = "USER_ID_KEY";

    private SharedPreferences mSharedPreferences;

    public PreferencesManager(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }


    public void saveAuthToken(String authToken) {
        mSharedPreferences.edit().putString(AUTH_TOKEN_KEY, authToken).apply();
    }

    /*
        public void saveAvatarUrl(String userAvatar) {
            mSharedPreferences.edit().putString(AVATAR_LOCAL_KEY, userAvatar).apply();
            Log.d(LOG_UPLOAD, "PreferencesManager saveAvatarUrl avatarLocalUrl=" + userAvatar);

        }

        public void saveAvatarServerUrl(String userAvatar) {
            mSharedPreferences.edit()
            .putString(AVATAR_SERVER_KEY, userAvatar)
            .apply();
            Log.d(LOG_UPLOAD, "PreferencesManager saveAvatarServerUrl avatarServerUrl=" + userAvatar);
        }
    */
    public void saveAvatarUrl(String userAvatar) {
        mSharedPreferences.edit()
                .putString(AVATAR_KEY, userAvatar)
                .apply();
        Log.d(LOG_UPLOAD, "PreferencesManager saveAvatarUrl avatarUrl=" + userAvatar);
    }

    public void saveUserName(String userName) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_NAME_KEY, userName);
        editor.apply();
    }

    /*
        private String getAvatarLocalUrl() {
            return mSharedPreferences.getString(AVATAR_LOCAL_KEY, "");
        }

        private String getAvatarServerUrl() {
            return mSharedPreferences.getString(AVATAR_SERVER_KEY, "");
        }

        public String getAvatarUrl() {
            return !getAvatarLocalUrl().isEmpty()?getAvatarLocalUrl():getAvatarServerUrl();
        }
    */
    public String getAvatarUrl() {
        return mSharedPreferences.getString(AVATAR_KEY, "");
    }


    public String getUserName() {
        return mSharedPreferences.getString(USER_NAME_KEY, "");
    }

    public String getPhone() {
        return mSharedPreferences.getString(PHONE_KEY, "");
    }

    public boolean isOrderNotification() {
        return mSharedPreferences.getBoolean(IS_ORDER_NOTIFICATION_KEY, false);
    }

    public void saveIsOrderNotification(boolean isOrderNotification) {
        mSharedPreferences.edit().putBoolean(IS_ORDER_NOTIFICATION_KEY, isOrderNotification).apply();
    }

    public void saveIsPromoNotification(boolean isPromoNotification) {
        mSharedPreferences.edit().putBoolean(IS_PROMO_NOTIFICATION_KEY, isPromoNotification).apply();
    }

    public void saveProductLastModified(String lastModified) {
        mSharedPreferences.edit().putString(PRODUCT_LAST_MODIFIED_KEY, lastModified).apply();
    }

    public String getProductLastModified() {
        //return DEFAULT_LAST_MODIFIED_DATE;//for test
        return mSharedPreferences.getString(PRODUCT_LAST_MODIFIED_KEY, DEFAULT_LAST_MODIFIED_DATE);
    }

    public boolean isPromoNotification() {
        return mSharedPreferences.getBoolean(IS_PROMO_NOTIFICATION_KEY, false);
    }


    public String getAuthToken() {
        return mSharedPreferences.getString(AUTH_TOKEN_KEY, null);
    }

    public boolean checkAuthToken() {
        return getAuthToken() != null;
    }


    public void savePhone(String phone) {
        mSharedPreferences.edit().putString(PHONE_KEY, phone).apply();
    }

    public void saveProfileInfo(@NonNull UserRes userRes) {
        mSharedPreferences.edit()
                .putString(USER_ID_KEY, userRes.getId())
                .putString(AUTH_TOKEN_KEY, userRes.getToken())
                .putString(USER_NAME_KEY, userRes.getFullName())
                .putString(AVATAR_KEY, userRes.getAvatarUrl())
                .putString(PHONE_KEY, userRes.getPhone())
                .apply();
    }

    public void removeProfileInfo() {
        mSharedPreferences.edit()
                .remove(USER_ID_KEY)
                .remove(AUTH_TOKEN_KEY)
                .remove(USER_NAME_KEY)
                .remove(AVATAR_KEY)
                .remove(PHONE_KEY)
                .apply();
    }
}

