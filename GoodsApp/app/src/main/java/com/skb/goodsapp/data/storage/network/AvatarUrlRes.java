package com.skb.goodsapp.data.storage.network;

public class AvatarUrlRes {

    private String avatarUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
