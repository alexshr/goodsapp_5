package com.skb.goodsapp;

import com.skb.goodsapp.di.AppComponent;
import com.skb.goodsapp.di.AppModule;
import com.skb.goodsapp.di.DaggerService;

/**
 * to replace MyApplication (using MockTestRunner)
 * for MockWebServer usage
 *
 *   from manifest
 *   <instrumentation
 *    android:name="com.skb.goodsapp.MockTestRunner"
 *    android:functionalTest="false"
 *    android:handleProfiling="false"
 *    android:label="Tests for com.skb.goodsapp"
 *    android:targetPackage="com.skb.goodsapp" />
 *
 *   public class MockTestRunner extends AndroidJUnitRunner {
 *   @Override
 *   public Application newApplication(ClassLoader cl, String className, Context context)
 *           throws InstantiationException, IllegalAccessException, ClassNotFoundException {
 *       return super.newApplication(cl, MockMyApplication.class.getName(), context);
 *       }
 *}
 */
public class MockMyApplication extends MyApplication {

    private static String baseUrl;

    @Override
    public Object createComponent() {

        return DaggerService.buildComponent(AppComponent.class, new AppModule(this) {
            @Override
            protected String provideBaseUrl() {
                return baseUrl != null ? baseUrl : super.provideBaseUrl();
            }
        });
    }

    public static void setupBaseUrl(String baseUrl) {
        MockMyApplication.baseUrl = baseUrl;
    }
}
