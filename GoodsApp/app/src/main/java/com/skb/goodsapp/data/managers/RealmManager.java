package com.skb.goodsapp.data.managers;


import android.content.Context;
import android.util.Log;

import com.skb.goodsapp.data.storage.network.BasketProductRes;
import com.skb.goodsapp.data.storage.network.ProductCommentRes;
import com.skb.goodsapp.data.storage.network.ProductRes;
import com.skb.goodsapp.data.storage.realm.BasketProductRealm;
import com.skb.goodsapp.data.storage.realm.FavoriteProductRealm;
import com.skb.goodsapp.data.storage.realm.ProductCommentRealm;
import com.skb.goodsapp.data.storage.realm.ProductRealm;
import com.skb.goodsapp.data.storage.realm.UserAddressRealm;

import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.Realm.Transaction;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

import static com.skb.goodsapp.ConstantManager.LOG_TAG;
import static com.skb.goodsapp.ConstantManager.STATUS_CONFIRMED;
import static com.skb.goodsapp.ConstantManager.STATUS_DELETE_LOCAL;
import static com.skb.goodsapp.ConstantManager.STATUS_EXIST_LOCAL;
import static com.skb.goodsapp.data.storage.realm.BasketProductRealm.getByPrimaryKey;


public class RealmManager {

    public static boolean isRealmInited;

    private Realm mRealm;

    private static RealmConfiguration mConfig;

    //To avoid multiple init call (e.g. for test)
    //but not safe from error of testing with Realm server
    public static void initRealm(Context context) {
        //if(!isRealmInited){
        Realm.init(context);
        //isRealmInited=true;
        //}
    }

    public static void configureRealm() {
        mConfig = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                //.initialData(mInitDataTrans)
                .build();
        Realm.setDefaultConfiguration(mConfig);
    }

/*
    public static void initAndConfigureRealm(Context context ){
        if(!isRealmInited){
            Realm.init(context);
            isRealmInited=true;
            configureRealm();
        }
    }
    */

    //only for ui thread
    private Realm getRealm() {
        if (mRealm == null || mRealm.isClosed()) {
            mRealm = Realm.getDefaultInstance();
        }
        return mRealm;
    }

    public ProductRealm getProductById(String id) {
        return ProductRealm.getByPrimaryKey(getRealm(), id);
    }

    public ProductRealm getProductById(String id, Realm realm) {
        return ProductRealm.getByPrimaryKey(realm, id);
    }


    public void closeRealm() {
        mRealm.close();
    }

    public void deleteRealm() {

        getRealm().close();

        Realm.deleteRealm(mConfig);
    }


    public void executeTransactionAsync(Transaction transaction, Transaction.OnSuccess onSuccess, Transaction.OnError onError) {
        getRealm().executeTransactionAsync(transaction, onSuccess, onError);
    }

    public void executeTransactionAsync(Transaction transaction, Transaction.OnSuccess onSuccess) {
        getRealm().executeTransactionAsync(transaction, onSuccess, e -> Log.e(LOG_TAG, "", e));
    }

    public void executeTransactionAsync(Transaction transaction) {
        getRealm().executeTransactionAsync(transaction, () -> Log.d(LOG_TAG, "async transaction success"), e -> Log.e(LOG_TAG, "async transaction error", e));
    }


    public void executeTransaction(Transaction transaction) {
        try {
            getRealm().executeTransaction(transaction);
            Log.d(LOG_TAG, "sync transaction success");
        } catch (Throwable throwable) {
            Log.e(LOG_TAG, "sync transaction error", throwable);
        }
    }


    //асинхронная транзакция (executeTransactionAsync)
    //если сами получаем realm в background - возникают проблемы c realm синхронизацией)
    public void synchronizeWithServerData(List<ProductRes> productServerList, Transaction.OnSuccess onSuccess) {
        executeTransactionAsync(realm -> {

            //синхронизация с результатами api запроса
            for (ProductRes productRes : productServerList) {
                ProductRealm product = ProductRealm.getByPrimaryKey(realm, productRes.id);
                if (productRes.isActive) {
                    if (product == null) {
                        product = realm.createObject(ProductRealm.class, productRes.id);
                    }
                    product.updateFromNetworkRes(productRes);
                } else if (product != null) {
                    product.deleteFromRealm();
                }


                List<ProductCommentRes> commentResList = productRes.comments;
                for (ProductCommentRes commentRes : commentResList) {
                    ProductCommentRealm comment = ProductCommentRealm.getByPrimaryKey(realm, commentRes.id);
                    if (commentRes.isActive) {
                        if (comment == null) {
                            comment = realm.createObject(ProductCommentRealm.class, commentRes.id);
                        }
                        comment.updateFromNetworkRes(commentRes, productRes.id);
                    } else if (comment != null) {
                        comment.deleteFromRealm();
                    }
                }
            }
        }, onSuccess, error -> Log.e(LOG_TAG, "synchronizeWithServerData error", error));
    }

    //заменить лок запись пришедшей с сервера
    public void replaceCommentFromServer(ProductCommentRes commentRes, long localId) {
        Realm realm = Realm.getDefaultInstance();//for background
        realm.beginTransaction();
        ProductCommentRealm comment = ProductCommentRealm.getInstanceFromNetworkRes(commentRes);
        realm.insert(comment);

        ProductCommentRealm localComment = realm.where(ProductCommentRealm.class).equalTo("localId", localId).findFirst();
        localComment.deleteFromRealm();

        Log.d(LOG_TAG, "replaceCommentFromServer comment added: " + comment + " deleted id=: " + localId);
        realm.commitTransaction();
        realm.close();
    }

    //заменить лок запись пришедшей с сервера
    public void deleteLocalComment(long localId) {
        Realm realm = Realm.getDefaultInstance();//for background

        ProductCommentRealm localComment = realm.where(ProductCommentRealm.class).equalTo("localId", localId).findFirst();
        localComment.deleteFromRealm();

        Log.d(LOG_TAG, "deleteLocalComment comment: " + localComment);
        realm.commitTransaction();
        realm.close();
    }

    //сохранение лок записи (до отправки на сервер)
    public void saveComment(float rating, String text, long localId, String productId) {
        Realm realm = Realm.getDefaultInstance();//for background
        realm.beginTransaction();
        ProductCommentRealm comment = ProductCommentRealm.getLocalInstance(productId, rating, text, localId);
        realm.insert(comment);
        realm.commitTransaction();
        realm.close();
        Log.d(LOG_TAG, "saveComment temp comment added " + comment);
    }


    public void deleteUserAddress(long id) {

        executeTransactionAsync(realm -> realm.where(UserAddressRealm.class).equalTo("id", id)
                        .findAll()
                        .deleteAllFromRealm(),
                () -> Log.d(LOG_TAG, "user address was deleted"),
                error -> Log.e(LOG_TAG, "", error));
    }

    public void addOrUpdateUserAddress(UserAddressRealm userAddress) {
        executeTransactionAsync(realm -> {
                    if (userAddress.getId() == null) {
                        userAddress.setId(Calendar.getInstance().getTimeInMillis());//для уникальности на realm сервере
                        Log.d(LOG_TAG, "user address to add " + userAddress);
                    }
                    Log.d(LOG_TAG, "user address to update " + userAddress);
                    realm.copyToRealmOrUpdate(userAddress);
                },
                () -> Log.d(LOG_TAG, "user address was saved"),
                error -> Log.e(LOG_TAG, "", error));
    }


    public UserAddressRealm getUserAddressById(long userAddressId) {
        return getRealm().where(UserAddressRealm.class).equalTo("id", userAddressId).findFirst();
    }

    public RealmResults<UserAddressRealm> getUserAddressListAsync() {
        return getRealm().where(UserAddressRealm.class).findAllAsync();
    }

    public RealmResults<ProductRealm> getProductListAsync() {
        return getRealm().where(ProductRealm.class).findAllAsync();
    }

    public RealmResults<ProductRealm> getProductList() {
        return getRealm().where(ProductRealm.class).findAll();
    }

    public RealmResults<BasketProductRealm> getBasket() {
        return getRealm().where(BasketProductRealm.class).notEqualTo("status", STATUS_DELETE_LOCAL).findAllAsync();
    }

    public RealmResults<BasketProductRealm> getBasket(Realm realm) {
        return realm.where(BasketProductRealm.class).notEqualTo("status", STATUS_DELETE_LOCAL).findAll();
    }

    public RealmResults<FavoriteProductRealm> getFavorites() {
        return getRealm().where(FavoriteProductRealm.class).notEqualTo("status", STATUS_DELETE_LOCAL).findAll();
    }

    public void updateBasketFromServer(List<BasketProductRes> basketProductResList) {
        executeTransactionAsync(realm -> {
            for (BasketProductRes basketRes : basketProductResList) {
                BasketProductRealm basketProduct = getByPrimaryKey(realm, basketRes.id);
                if (basketProduct == null || basketProduct.getStatus() == STATUS_CONFIRMED) {
                    ProductRealm product = ProductRealm.getByPrimaryKey(realm, basketRes.id);
                    basketProduct = BasketProductRealm.getInstance(product, basketRes.count, STATUS_CONFIRMED);
                    realm.copyToRealmOrUpdate(basketProduct);
                }
            }
        });
    }

    public void deleteFromBasketAsync(String productId) {
        executeTransactionAsync(realm -> BasketProductRealm.getByPrimaryKey(realm, productId).setStatus(STATUS_DELETE_LOCAL));
    }

    public void deleteFromBasket(String productId) {
        executeTransaction(realm -> BasketProductRealm.getByPrimaryKey(realm, productId).setStatus(STATUS_DELETE_LOCAL));
    }

    public void clearBasket() {
        executeTransaction(realm -> realm.where(BasketProductRealm.class).findAll().deleteAllFromRealm());
    }

    public void deleteAll() {
        getRealm().deleteAll();
    }


    public void updateBasketFromUser(String productId, int count) {
        executeTransactionAsync(realm -> {
            ProductRealm product = ProductRealm.getByPrimaryKey(realm, productId);
            BasketProductRealm basketProduct = BasketProductRealm.getInstance(product, count, STATUS_EXIST_LOCAL);
            realm.copyToRealmOrUpdate(basketProduct);
        });
    }

    public void addFavoriteFromUser(String productId) {
        executeTransactionAsync(realm -> {
            ProductRealm product = ProductRealm.getByPrimaryKey(realm, productId);
            FavoriteProductRealm favoriteProduct = FavoriteProductRealm.getLocalInstance(product);
            realm.copyToRealmOrUpdate(favoriteProduct);
        });
    }


    public void deleteFavorite(String productId) {
        executeTransactionAsync(realm -> FavoriteProductRealm.getByPrimaryKey(realm, productId).setStatus(STATUS_DELETE_LOCAL));
    }

    //кол-во продуктов (этого вида) в корзине
    public int getBasketCount(String productId) {
        BasketProductRealm basketProduct = getRealm().where(BasketProductRealm.class).equalTo("productId", productId).findFirst();
        return basketProduct == null ? 0 : basketProduct.getCount();
    }

    public BasketProductRealm getBasketProductById(String productId, Realm realm) {
        return BasketProductRealm.getByPrimaryKey(realm, productId);
    }

    public BasketProductRealm getBasketProductById(String productId) {
        return getByPrimaryKey(getRealm(), productId);
    }

    public boolean isInBasket(String productId) {
        BasketProductRealm basketProduct = getByPrimaryKey(getRealm(), productId);
        return basketProduct != null && basketProduct.getStatus() != STATUS_DELETE_LOCAL;
    }

    public FavoriteProductRealm getFavoriteProductById(String productId) {
        return FavoriteProductRealm.getByPrimaryKey(getRealm(), productId);
    }

    public boolean isFavorite(String productId) {
        FavoriteProductRealm favoriteProduct = FavoriteProductRealm.getByPrimaryKey(getRealm(), productId);
        return favoriteProduct != null && favoriteProduct.getStatus() != STATUS_DELETE_LOCAL;
    }

    public RealmResults<ProductCommentRealm> getProductComments(String productId) {
        return ProductCommentRealm.getByProductId(getRealm(), productId);
    }


}
