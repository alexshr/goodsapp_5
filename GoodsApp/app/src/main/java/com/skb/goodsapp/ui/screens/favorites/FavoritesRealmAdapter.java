package com.skb.goodsapp.ui.screens.favorites;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.FavoriteProductRealm;
import com.skb.goodsapp.data.storage.realm.ProductRealm;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.mvp.models.FavoritesModel;
import com.skb.goodsapp.utils.ImageUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import me.angrybyte.numberpicker.view.ActualNumberPicker;

public class FavoritesRealmAdapter extends RealmRecyclerViewAdapter<FavoriteProductRealm, FavoritesRealmAdapter.ViewHolder> {

    private Context mContext;
    private ActionItemListener mActionListener;
    private OrderedRealmCollection<FavoriteProductRealm> mAdapterData;

    @Inject
    FavoritesModel mFavoritesModel;

    public FavoritesRealmAdapter(Context context, OrderedRealmCollection<FavoriteProductRealm> data, ActionItemListener actionListener) {
        super(data, true);

        ((FavoritesScreen.Component) DaggerService.getDaggerComponent(context)).inject(this);
        mAdapterData = data;
        mContext = context;
        mActionListener = actionListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorites, parent, false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ProductRealm product = getData().get(position).getProduct();
        holder.mProductId = product.getId();

        ImageUtils.showImage(mContext, holder.mProductImageView, product.getImageUrl(), R.drawable.img_product_nophoto);
        holder.mProductImageView.setOnClickListener(v -> mActionListener.onImageClicked(product.getId()));

        holder.mProductNameView.setText(product.getProductName());
        holder.mProductDescriptionView.setText(product.getDescription());
        holder.mFavoriteCheckbox.setChecked(true);

        holder.mProductPriceView.setText(ProductRealm.formatCost(product.getPrice()));

        int productBasketCount = mFavoritesModel.getBasketCount(product.getId());
        productBasketCount = productBasketCount == 0 ? 1 : productBasketCount;
        holder.mProductNumberPicker.setValue(productBasketCount);

        holder.price = product.getPrice();
        holder.mProductPriceView.setText(ProductRealm.formatCost(holder.price * productBasketCount));

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        //region ================= butterknife =================
        @BindView(R.id.product_image)
        ImageView mProductImageView;
        @BindView(R.id.product_name)
        TextView mProductNameView;
        @BindView(R.id.product_description)
        TextView mProductDescriptionView;
        @BindView(R.id.price)
        TextView mProductPriceView;
        @BindView(R.id.basket_button)
        AppCompatImageButton mBasketBtn;
        @BindView(R.id.favorite_checkbox)
        AppCompatCheckBox mFavoriteCheckbox;

        @BindView(R.id.product_number_picker)
        ActualNumberPicker mProductNumberPicker;
        //endregion

        String mProductId;
        int price;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mProductNumberPicker.setListener((oldValue, newValue) -> {
                mProductPriceView.setText(ProductRealm.formatCost(price * newValue));
            });
            mFavoriteCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (!isChecked) mActionListener.onFavoriteDeleted(mProductId);
            });

            mBasketBtn.setOnClickListener(v -> mActionListener.onBasketClicked(mProductId, mProductNumberPicker.getValue()));
        }
    }

    public interface ActionItemListener {
        public void onFavoriteDeleted(String id);

        public void onBasketClicked(String id, int count);

        public void onImageClicked(String id);
    }
}
