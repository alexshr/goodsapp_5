package com.skb.goodsapp.ui.screens.account;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.dto.UserInfoDto;
import com.skb.goodsapp.data.storage.dto.UserSettingsDto;
import com.skb.goodsapp.data.storage.realm.UserAddressRealm;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.mvp.views.IBackPressedView;
import com.skb.goodsapp.ui.views.ValidatedTextInputLayout;
import com.skb.goodsapp.utils.BorderedCircleTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.RealmResults;

import static com.skb.goodsapp.ConstantManager.LOG_TAG;

public class AccountView extends CoordinatorLayout implements IBackPressedView {

    @Inject
    AccountScreen.AccountPresenter mPresenter;


    //region ================= butterknife =================
    @BindView(R.id.header)
    RelativeLayout mHeader;

    @BindView(R.id.header_name)
    TextView mUserHeaderName;
    @BindView(R.id.avatar_wrapper)
    FrameLayout mAvatarWrapper;
    @BindView(R.id.take_avatar)
    ImageView mTakeAvatar;
    @BindView(R.id.user_avatar)
    ImageView mUserAvatar;


    @BindView(R.id.phone_layout)
    ValidatedTextInputLayout mUserPhoneValidatedTextLayout;
    @BindView(R.id.phone)
    TextView mUserPhone;

    @BindView(R.id.user_name_area)
    LinearLayout mUserNameLayout;

    @BindView(R.id.phone_area)
    LinearLayout mUserPhoneLayout;

    @BindView(R.id.validate_input_user_name_layout)
    ValidatedTextInputLayout mUserNameValidatedTextLayout;
    @BindView(R.id.user_name)
    TextView mUserName;

    @BindView(R.id.address_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.add_address_button)
    Button mAddAddressButton;
    @BindView(R.id.notify_order_switch)
    SwitchCompat mOrderNotificationSwitch;
    @BindView(R.id.notify_promotion_switch)
    SwitchCompat mPromotionNotificationSwitch;
    @BindView(R.id.appbar_account)
    AppBarLayout mAppBarLayout;

    @OnCheckedChanged({R.id.notify_order_switch, R.id.notify_promotion_switch})
    void onOrderNotificationCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        mPresenter.onUserSettingsChanged(getUserSettings());
    }

    @OnClick(R.id.add_address_button)
    void onAddAddressClicked() {
        mPresenter.onAddAddressClicked();
    }

    @OnClick(R.id.avatar_wrapper)
    void onSetAvatarClicked() {
        mPresenter.onAvatarClicked();
    }

    @OnClick(R.id.header)
    void onToolbarClicked() {
        mPresenter.onToolbarClicked();
    }

    //endregion

    private AddressRealmAdapter mAddressListAdapter;
    private Unbinder mUnbinder;

    public AccountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        DaggerService.<AccountScreen.Component>getDaggerComponent(context).inject(this);
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mUnbinder = ButterKnife.bind(this);

    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mUnbinder.unbind();
            mPresenter.dropView(this);
        }
    }

    public void initView(RealmResults<UserAddressRealm> userAddressList) {

        refreshToCurrentMode();

        mAddressListAdapter = new AddressRealmAdapter(userAddressList, new AddressRealmAdapter.ActionItemListener() {
            @Override
            public void onDeleteClicked(long id) {
                mPresenter.onDeleteAddressClicked(id);
            }

            @Override
            public void onEditClicked(long id) {
                mPresenter.onEditAddressClicked(id);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                layoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setAdapter(mAddressListAdapter);
    }

    public void refreshToCurrentMode() {
        mUserNameLayout.setVisibility(mPresenter.isEditMode() ? VISIBLE : GONE);

        mUserPhoneValidatedTextLayout.setInEditMode(mPresenter.isEditMode());
        mUserNameValidatedTextLayout.setInEditMode(mPresenter.isEditMode());
        mUserAvatar.setVisibility(isInEditMode() ? INVISIBLE : VISIBLE);
        mTakeAvatar.setVisibility(isInEditMode() ? VISIBLE : INVISIBLE);
    }

    public void setUserSettings(UserSettingsDto userSettings) {
        mOrderNotificationSwitch.setChecked(userSettings.isOrderNotification());
        mPromotionNotificationSwitch.setChecked(userSettings.isPromoNotification());
    }

    public void showUserInfo(UserInfoDto userInfo) {
        mUserHeaderName.setText(userInfo.getName());
        mUserName.setText(userInfo.getName());
        mUserPhone.setText(userInfo.getPhone());
    }

    public UserSettingsDto getUserSettings() {
        return new UserSettingsDto(mOrderNotificationSwitch.isChecked(),
                mPromotionNotificationSwitch.isChecked());
    }


    public UserInfoDto getUserInfo() {
        return new UserInfoDto(mUserName.getText().toString(), mUserPhone.getText().toString());
    }


    /*простейший вариант
    private void initSwipe() {

        //touch helper для удаления карточек (влево)
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                showRemoveAddressDialog(position);
            }
        });
        itemTouchHelper.attachToRecyclerView(mRecyclerView);

    }
*/
    //@UiThread
    public void showAccountAvatar(String uri) {
        mTakeAvatar.setVisibility(uri.isEmpty() ? VISIBLE : INVISIBLE);
        mUserAvatar.setVisibility(uri.isEmpty() ? INVISIBLE : VISIBLE);
        if (!uri.isEmpty()) {
            Picasso.with(getContext())
                    .load(uri)
                    .transform(new BorderedCircleTransform())
                    .fit()
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(mUserAvatar, new Callback() {
                        @Override
                        public void onSuccess() {
                            Log.d(LOG_TAG, "image is loaded from cache");
                        }

                        @Override
                        public void onError() {
                            Picasso.with(AccountView.this.getContext())
                                    .load(uri)
                                    //.error(R.drawable.img_nophoto)
                                    //.placeholder(R.drawable.img_nophoto)
                                    .transform(new BorderedCircleTransform())
                                    .fit()
                                    //.centerInside()
                                    .into(mUserAvatar, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d(LOG_TAG, "image is loaded from network");
                                        }

                                        @Override
                                        public void onError() {
                                            Log.d(LOG_TAG, "image loading error");
                                            mPresenter.mRootPresenter.showError(new MessageDto(R.string.avatar_error));
                                            showAccountAvatar("");
                                        }
                                    });
                        }
                    });
        }

            /* глючит
            Glide.with(getContext())
                    .load(uri)
                    //.placeholder(R.drawable.ic_add_a_photo_140dp)
                    .skipMemoryCache(true)
                    .transform(new CropCircleTransformation(getContext()))
                    .dontAnimate()
                    .into(new GlideDrawableImageViewTarget(mUserAvatar) {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                            super.onResourceReady(resource, animation);
                            //never called
                            //Log.e(TAG, "onLoadFailed: success");
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                            //never called
                            //Log.e(TAG, "onLoadFailed: failed");

                            Glide.with(getContext())
                                    .load("")
                                    .placeholder(R.drawable.ic_add_a_photo_140dp)
                                    //.transform(new CropCircleTransformation(getContext()))
                                    .dontAnimate()
                                    .into(mUserAvatar);
                        }
                    });
                    */

    }

    public void showPhotoSourceDialog() {

        new MaterialDialog.Builder(getContext())
                .title(R.string.account_set_photo)
                .items(R.array.photo_source_items)
                .negativeText(R.string.account_cancel)
                .onNegative((dialog, which) -> {
                })
                .itemsCallback((dialog, view, which, text) -> {
                    switch (which) {
                        case 0:
                            mPresenter.chooseGallery();
                            break;
                        case 1:
                            mPresenter.chooseCamera();
                    }
                }).show();
    }

    @Override
    public boolean onViewBackPressed() {
        return mPresenter.isEditMode() && mPresenter.onToolbarClicked();
    }


    public boolean validatePhoneAndName() {
        return mUserNameValidatedTextLayout.isTextValid()
                && mUserPhoneValidatedTextLayout.isTextValid();
    }

    //endregion
}
