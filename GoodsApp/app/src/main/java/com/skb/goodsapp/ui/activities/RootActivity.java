package com.skb.goodsapp.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.AppDispatcher;
import com.skb.goodsapp.flow.NavigationDrawerHelper;
import com.skb.goodsapp.flow.ScreenMenu;
import com.skb.goodsapp.mvp.presenters.RootPresenter;
import com.skb.goodsapp.mvp.views.IBackPressedView;
import com.skb.goodsapp.mvp.views.INavigatedView;
import com.skb.goodsapp.mvp.views.IRootView;
import com.skb.goodsapp.ui.screens.catalog.CatalogScreen;
import com.skb.goodsapp.utils.BorderedCircleTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;
import javax.inject.Scope;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.Provides;
import flow.Direction;
import flow.Flow;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;
import mortar.bundler.Bundler;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.skb.goodsapp.ConstantManager.LOG_TAG;
import static com.skb.goodsapp.ConstantManager.LOG_UPLOAD;
import static com.skb.goodsapp.R.color;
import static com.skb.goodsapp.R.id;
import static com.skb.goodsapp.R.string;
import static com.skb.goodsapp.R.style;
import static mortar.MortarScope.buildChild;
import static mortar.MortarScope.findChild;

public class RootActivity extends AppCompatActivity implements IRootView, INavigatedView {

    //region ================= butterknife =================
    @BindView(id.coordinator_layout)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(id.navigation_view)
    NavigationView mNavigationView;
    @BindView(id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(id.toolbar)
    Toolbar mToolbar;
    @BindView(id.root_container)
    ViewFlipper mViewFlipper;
    @BindView(id.appbar_layout)
    AppBarLayout mAppBarLayout;
    //endregion

    private ImageView mNavigationAvatarView;
    private TextView mNavigationUserNameView;

    public static final String MORTAR_ROOT_ACTIVITY_SCOPE_NAME = "RootActivity";

    private ProgressDialog mProgressDialog;

    private MaterialDialog mExitDialog;

    @Inject
    RootPresenter mPresenter;

    Unbinder mUnbinder;

    private ActionBarDrawerToggle mToggle;
    private ActionBar mActionBar;
    private ScreenMenu mScreenMenu;

    @Override
    public Object getSystemService(String name) {
        MortarScope activityScope = findChild(getApplicationContext(), MORTAR_ROOT_ACTIVITY_SCOPE_NAME);

        if (activityScope == null) {
            activityScope = buildChild(getApplicationContext()) //
                    .withService(BundleServiceRunner.SERVICE_NAME, new BundleServiceRunner())
                    .withService(DaggerService.SERVICE_NAME, DaggerService.buildComponent(Component.class, new RootPresenterModule()))
                    .build(MORTAR_ROOT_ACTIVITY_SCOPE_NAME);
        }

        return activityScope.hasService(name) ? activityScope.getService(name)
                : super.getSystemService(name);
    }


    //region ================= lifecycle =================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(LOG_UPLOAD, "RootActivity onCreate started");
        setTheme(style.AppTheme);
        super.onCreate(savedInstanceState);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        BundleServiceRunner.getBundleServiceRunner(this).onCreate(savedInstanceState);

        DaggerService.<Component>getDaggerComponent(this).inject(this);

        setContentView(R.layout.activity_root);

        mUnbinder = ButterKnife.bind(this);
        mNavigationAvatarView = (ImageView) mNavigationView.getHeaderView(0).findViewById(id.avatar);
        mNavigationUserNameView = (TextView) mNavigationView.getHeaderView(0).findViewById(id.header_name);

        mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
        mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));

        //hide status bar
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setupToolbar();
        setupDrawer();

        //region ================= exit dialog =================
        mExitDialog = new MaterialDialog.Builder(this)
                .title(string.app_exit)
                .content(string.confirm_app_exit)
                .positiveText(string.yes)
                .negativeText(string.no)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    }
                })
                .build();
        //endregion

        mPresenter.takeView(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.dropView(this);
        mPresenter.closeRealm();//the only place to do it with guarantee
        mUnbinder.unbind();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        /**
         * To be called from the host {@link android.app.Activity}'s {@link
         * android.app.Activity#onSaveInstanceState}. Calls the registrants' {@link Bundler#onSave}
         * methods.
         */
        BundleServiceRunner.getBundleServiceRunner(this).onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        View view = getCurrentView();

        if ((!(view instanceof IBackPressedView) || !((IBackPressedView) view).onViewBackPressed()) && !Flow.get(this).goBack()) {
            DrawerLayout drawer = (DrawerLayout) findViewById(id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                mExitDialog.show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_UPLOAD, "RootActivity onResume started");
        //mPresenter.showNavigationUserName();
        //mPresenter.showNavigationAvatar();
        //mPresenter.showNavigationUser();
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mScreenMenu.onCreateMenu(menu, getMenuInflater());
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mScreenMenu.onOptionsItemSelected(item);
        return true;
    }

    @Override
    public void showNavigationUserName(String name) {
        mNavigationUserNameView.setText(name);
    }

    @Override
    public void showNavigationAvatar(String url) {
        if (!url.isEmpty()) {
            mNavigationAvatarView.setVisibility(View.VISIBLE);
            Picasso.with(this)
                    .load(url)
                    .transform(new BorderedCircleTransform())
                    .fit()
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(mNavigationAvatarView, new Callback() {
                        @Override
                        public void onSuccess() {
                            Log.d(LOG_TAG, "image is loaded from cache");
                        }

                        @Override
                        public void onError() {
                            Picasso.with(RootActivity.this)
                                    .load(url)
                                    //.error(R.drawable.img_product_nophoto)
                                    //.placeholder(R.drawable.img_nophoto)
                                    .transform(new BorderedCircleTransform())
                                    .fit()
                                    //.centerInside()
                                    .into(mNavigationAvatarView, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d(LOG_TAG, "image is loaded from network");
                                        }

                                        @Override
                                        public void onError() {
                                            Log.d(LOG_TAG, "image loading error");
                                            showError(R.string.avatar_error);
                                        }
                                    });
                        }
                    });
        } else {
            mNavigationAvatarView.setVisibility(View.INVISIBLE);
        }

        /*TODO not work correctly by glide
        Glide.with(this)
                .load(url)
                .skipMemoryCache(true)
                .transform(new CircleTransform(this))
                .dontAnimate()
                .into(new GlideDrawableImageViewTarget(mNavigationAvatarView) {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                        super.onResourceReady(resource, animation);
                        //never called
                        //Log.e(TAG, "onLoadFailed: success");
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        //never called
                        //Log.e(TAG, "onLoadFailed: failed");

                    }
                });
        */
    }

    /**
     * for Calligraphy library (custom fonts) and flow
     */
    @Override
    protected void attachBaseContext(Context baseContext) {
        baseContext = Flow.configure(baseContext, this)
                .dispatcher(new AppDispatcher(this))
                .defaultKey(new CatalogScreen())
                .install();
        super.attachBaseContext(CalligraphyContextWrapper.wrap(baseContext));
    }
    //endregion


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mPresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, string.navigation_drawer_open, string.navigation_drawer_close);
        drawer.addDrawerListener(mToggle);
        mActionBar = getSupportActionBar();
        mToggle.syncState();
    }

    private void setupDrawer() {

        mNavigationView.setNavigationItemSelectedListener(item -> {
            Object key = null;
            key = NavigationDrawerHelper.getScreenByMenuItem(item.getItemId());
            if (item.getItemId() == id.nav_exit) {
                mPresenter.logout();
            } else {
                Flow.get(RootActivity.this).replaceHistory(key, Direction.REPLACE);//при нажатии на меню навигатора - начинаем историю заново
            }
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return true;
        });
    }


    private void showMessage(int message, Object... formatArgs) {
        showMessage(getString(message, formatArgs));
        Snackbar.make(mCoordinatorLayout, getString(message, formatArgs), Snackbar.LENGTH_LONG).show();
    }

    private void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }


    private void showError(int message, Object... formatArgs) {
        showError(getString(message, formatArgs));
    }


    private void showError(String text) {
        try {
            Snackbar snack = Snackbar.make(mCoordinatorLayout, text, Snackbar.LENGTH_LONG);
            View view = snack.getView();
            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            tv.setTextColor(ContextCompat.getColor(this, color.colorAccent));
            snack.show();
        } catch (Exception e) {
            Log.e(LOG_TAG, "", e);
        }
    }

    @Override
    public void showError(MessageDto messageDto) {
        if (messageDto.getText() == null) {
            showError(messageDto.getMessageRes(), messageDto.getArgs());
        } else {
            showError(messageDto.getText());
        }
    }

    @Override
    public void showMessage(MessageDto messageDto) {
        if (messageDto.getText() == null) {
            showMessage(messageDto.getMessageRes(), messageDto.getArgs());
        } else {
            showMessage(messageDto.getText());
        }
    }



    /*
    @Override
    public void showError(Throwable e) {
        Log.e(LOG_TAG, "", e);
        showError(getString(string.app_error, e.getMessage()));
    }
*/

    private void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, null, null);
            mProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.setContentView(new ProgressBar(this));
        }
        mProgressDialog.show();
    }


    private void hideProgress() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.hide();
            }
        }
    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void setupProgress(boolean isLoading) {
        if (isLoading) {
            showProgress();
        } else {
            hideProgress();
        }
    }

    /*
        @Override
        public void setCurrentScreen(AbstractScreen screen) {
            mCurrentScreen = screen;
        }
    */
    @Nullable
    @Override
    public View getCurrentView() {
        return mViewFlipper.getChildAt(0);
    }

    @Override
    public NavigationView getNavigationView() {
        return mNavigationView;
    }

    public void setupDrawerBackArrow(boolean enabled) {
        if (mToggle != null && mActionBar != null) {
            if (enabled) {
                mToggle.setDrawerIndicatorEnabled(false); //скрываем индикатор toggle
                mActionBar.setDisplayHomeAsUpEnabled(true); // устанавливаем индикатор тулбара
                if (mToggle.getToolbarNavigationClickListener() == null) {
                    mToggle.setToolbarNavigationClickListener(v -> onBackPressed()); //вешаем обработчик
                }
            } else {
                mActionBar.setDisplayHomeAsUpEnabled(false);// скрываем индикатор тулбара
                mToggle.setDrawerIndicatorEnabled(true); //активируем индикатор toggle
                mToggle.setToolbarNavigationClickListener(null); //зануляем обработчик на toggle
            }

            //если есть возможность вернуться назад(стрелка назад в Action bar) то блокируем раскрытие NavDrawer
            mDrawerLayout.setDrawerLockMode(
                    enabled ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_UNLOCKED);
            mToggle.syncState();
        }
    }

    public void setTabLayout(ViewPager pager) {
        View view = mAppBarLayout.getChildAt(1);
        TabLayout tabView;
        if (view == null) {
            tabView = new TabLayout(this); //создаем TabLayout
            tabView.setupWithViewPager(pager); //связываем его с ViewPager
            mAppBarLayout.addView(tabView); //добавляем табы в Appbar
            pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabView)); // регистрируем обработчик переключения по табам для ViewPager
        } else {
            tabView = (TabLayout) view;
            tabView.setupWithViewPager(pager); //связываем его с ViewPager
            pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabView));
        }
    }

    public void removeTabLayout() {
        View tabView = mAppBarLayout.getChildAt(1);
        if (tabView != null && tabView instanceof TabLayout) { //проверяем если у аппбара есть дочерняя View являющаяся TabLayout
            mAppBarLayout.removeView(tabView); //то удаляем ее
        }
    }

    public void setScreenMenu(ScreenMenu screenMenu) {
        mScreenMenu = screenMenu;
    }


    //region ================= DI =================

    @dagger.Module
    public static class RootPresenterModule {
        @Provides
        @RootScope
        protected RootPresenter provideRootPresenter() {
            return new RootPresenter();
        }
    }

    @dagger.Component(modules = RootPresenterModule.class)
    @RootScope
    public interface Component {
        void inject(RootActivity activity);

        //for child components
        RootPresenter getRootPresenter();
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface RootScope {
    }
    //endregion
}
