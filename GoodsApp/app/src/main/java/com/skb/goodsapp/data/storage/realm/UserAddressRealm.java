package com.skb.goodsapp.data.storage.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UserAddressRealm extends RealmObject {
    @PrimaryKey
    private Long id;
    private String name;
    private String street;
    private String house;
    private String apartment;
    private String floor;
    private String comment;

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getApartment() {
        return apartment;
    }

    public String getFloor() {
        return floor;
    }

    public String getComment() {
        return comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean validate() {
        return name != null && !name.isEmpty()
                && street != null && !street.isEmpty()
                && house != null && !house.isEmpty()
                && apartment != null && !apartment.isEmpty()
                && floor != null && !floor.isEmpty();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserAddressRealm{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", street='").append(street).append('\'');
        sb.append(", house='").append(house).append('\'');
        sb.append(", apartment='").append(apartment).append('\'');
        sb.append(", floor='").append(floor).append('\'');
        sb.append(", comment='").append(comment).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
