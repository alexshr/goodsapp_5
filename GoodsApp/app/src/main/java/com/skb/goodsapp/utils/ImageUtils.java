package com.skb.goodsapp.utils;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.skb.goodsapp.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import static com.skb.goodsapp.ui.screens.catalog.CatalogScreen.LOG_TAG;

public class ImageUtils {

    /**
     * prepare picasso instance to use by Picasso.with()
     *
     * @param context
     */
    public static void initPicasso(Context context) {
        OkHttp3Downloader okHttp3Downloader = new OkHttp3Downloader(context, Integer.MAX_VALUE);
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(okHttp3Downloader);
        Picasso picassoInstance = builder.build();
        try {
            Picasso.setSingletonInstance(picassoInstance);
            Log.d(LOG_TAG, "initPicasso");
        } catch (IllegalStateException e) {
            Log.d(LOG_TAG, "invalid attemption to show picasso (already done before)");
            // Picasso instance was already set
            // cannot set it after Picasso.with(Context) was already in use
        }
    }

    public static void showImage(Context context, ImageView imageView, String imageUrl, int placeHolderRes) {

        Picasso.with(context)
                .load(imageUrl)
                .placeholder(placeHolderRes)
                .fit()
                .centerInside()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d(LOG_TAG, "image is loaded from cache");
                    }

                    @Override
                    public void onError() {
                        Picasso.with(context)
                                .load(imageUrl)
                                .error(R.drawable.img_product_nophoto)
                                .placeholder(R.drawable.img_product_nophoto)
                                .fit()
                                .centerInside()
                                .into(imageView, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        Log.d(LOG_TAG, "image is loaded from network");
                                    }

                                    @Override
                                    public void onError() {
                                        Log.d(LOG_TAG, "image loading error");
                                    }
                                });
                    }
                });
    }
}
