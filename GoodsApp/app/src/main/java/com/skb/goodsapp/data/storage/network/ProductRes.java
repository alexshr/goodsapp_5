package com.skb.goodsapp.data.storage.network;

import com.squareup.moshi.Json;

import java.util.List;

public class ProductRes  {
    @Json(name = "_id")
    public String id;
    public Integer remoteId;
    public String productName;
    public String imageUrl;
    public String description;
    public Integer price;
    @Json(name = "raiting")
    public Float rating;
    @Json(name = "active")
    public Boolean isActive;
    public List<ProductCommentRes> comments = null;
}
