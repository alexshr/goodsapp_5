package com.skb.goodsapp.mvp.models;

import com.birbit.android.jobqueue.JobManager;
import com.skb.goodsapp.data.storage.dto.UserSettingsDto;
import com.skb.goodsapp.data.storage.realm.UserAddressRealm;
import com.skb.goodsapp.jobs.UploadAvatarJob;

import io.reactivex.Observable;
import io.realm.RealmResults;


public class AccountModel extends RootModel {

    //region ================= rx observables =================
    /*
    public Observable<String> getUserAvatarObservable() {
        String avatarUrl = mDataManager.getPreferencesManager().getAvatarLocalUrl();
        return avatarUrl != null&&isUserAuth() ? Observable.just(avatarUrl) : Observable.never();
    }
*/
    public Observable<UserSettingsDto> getUserSettingsObservable() {
        return Observable.just(mDataManager.getUserSettings());
    }
    //endregion


    public void saveUserSettings(UserSettingsDto userSettings) {
        mDataManager.saveUserSettings(userSettings);
    }

    public void uploadUserAvatar(String avatarLocalUri) {
        UploadAvatarJob job = new UploadAvatarJob(avatarLocalUri);
        getJobManager().addJobInBackground(job);

    }

    public void deleteAddress(long id) {
        mDataManager.getRealmManager().deleteUserAddress(id);
    }

    public void addOrUpdateUserAddress(UserAddressRealm userAddress) {
        mDataManager.getRealmManager().addOrUpdateUserAddress(userAddress);
    }


    public UserAddressRealm getUserAddressById(long userAddressId) {
        return getRealmManager().getUserAddressById(userAddressId);
    }

    public RealmResults<UserAddressRealm> findUserAddressListAsync() {
        return getRealmManager().getUserAddressListAsync();
    }

    public void saveAvatarUrl(String uri) {
        mDataManager.saveAvatarUrl(uri);
    }

    public JobManager getJobManager() {
        return mJobManager;
    }
}
