package com.skb.goodsapp.mvp.views;

public interface IProgressSupportView {
    void setupProgress(boolean isLoading);
}
