package com.skb.goodsapp.flow;

import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.view.Menu;
import android.view.MenuItem;

import com.skb.goodsapp.R;
import com.skb.goodsapp.ui.screens.account.AccountScreen;
import com.skb.goodsapp.ui.screens.basket.BasketScreen;
import com.skb.goodsapp.ui.screens.catalog.CatalogScreen;
import com.skb.goodsapp.ui.screens.favorites.FavoritesScreen;
import com.skb.goodsapp.ui.screens.login.LoginScreen;

import flow.History;

public class NavigationDrawerHelper {

    private static int getNavigationMenuLayout(boolean isAuthorized) {
        return isAuthorized ? R.menu.activity_root_drawer_autorized : R.menu.activity_root_drawer_anonym;
    }

    public static AbstractScreen getScreenByMenuItem(int itemId) {
        switch (itemId) {
            case R.id.nav_exit:
                return new LoginScreen();
            case R.id.nav_login:
                return new LoginScreen();
            case R.id.nav_account:
                return new AccountScreen();
            case R.id.nav_catalog:
                return new CatalogScreen();
            case R.id.nav_favorites:
                return new FavoritesScreen();
            case R.id.nav_basket:
                return new BasketScreen();
            default:
                throw new IllegalArgumentException("illegal navigation menu itemId");
        }
    }

    /**
     * по root screen в текущей истории находим пункт меню в навигаторе
     */
    private static Integer getSelectedMenuItem(Object rootKey) {
        if (rootKey.equals(new AccountScreen())) {
            return R.id.nav_account;
        } else if (rootKey.equals(new CatalogScreen())) {
            return R.id.nav_catalog;
        } else if (rootKey.equals(new LoginScreen())) {
            return R.id.nav_login;
        } else if (rootKey.equals(new FavoritesScreen())) {
            return R.id.nav_favorites;
        } else if (rootKey.equals(new BasketScreen())) {
            return R.id.nav_basket;
        }
        return null;
    }


    public static void prepareNavigationMenu(NavigationView mNavigationView, History history, boolean isAuthorized) {
        Menu menu = mNavigationView.getMenu();
        menu.clear();
        mNavigationView.inflateMenu(NavigationDrawerHelper.getNavigationMenuLayout(isAuthorized));

        Object rootKey = history.peek(history.size() - 1);//находим root screen в истории
        MenuItem item = menu.findItem(NavigationDrawerHelper.getSelectedMenuItem(rootKey));//пункт для выделения в navigation drawer

        new Handler().postDelayed(() -> {
            if (item != null)
                item.setChecked(true);
        }, 50);
    }


}
