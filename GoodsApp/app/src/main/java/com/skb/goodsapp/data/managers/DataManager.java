package com.skb.goodsapp.data.managers;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.facebook.login.LoginManager;
import com.skb.goodsapp.AppConfig;
import com.skb.goodsapp.ConstantManager;
import com.skb.goodsapp.MyApplication;
import com.skb.goodsapp.R;
import com.skb.goodsapp.data.network.ErrorUtils;
import com.skb.goodsapp.data.network.LoginApiException;
import com.skb.goodsapp.data.network.RestService;
import com.skb.goodsapp.data.rx.SimpleDisposableObserver;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.dto.UserInfoDto;
import com.skb.goodsapp.data.storage.dto.UserSettingsDto;
import com.skb.goodsapp.data.storage.network.AvatarUrlRes;
import com.skb.goodsapp.data.storage.network.ProductCommentReq;
import com.skb.goodsapp.data.storage.network.ProductCommentRes;
import com.skb.goodsapp.data.storage.network.ProductRes;
import com.skb.goodsapp.data.storage.network.UserLoginReq;
import com.skb.goodsapp.data.storage.network.UserReq;
import com.skb.goodsapp.data.storage.network.UserRes;
import com.skb.goodsapp.data.storage.network.UserSocReq;
import com.skb.goodsapp.utils.ConnectionChecker;
import com.skb.goodsapp.utils.FileUtils;
import com.skb.goodsapp.utils.Utils;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import id.zelory.compressor.Compressor;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

import static com.skb.goodsapp.AppConfig.LAST_MODIFIED_HEADER;
import static com.skb.goodsapp.AppConfig.RETRY_INITIAL_DELAY;
import static com.skb.goodsapp.AppConfig.UPDATE_DATA_INTERVAL;
import static com.skb.goodsapp.ConstantManager.LOG_TAG;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

//import com.skb.goodsapp.EspressoIdlingResource;

public class DataManager {

    private static final String LOG_SYNC = LOG_TAG + "SYNC";
    //public static final int RETRY_INITIAL_DELAY = 100;
    @Inject
    Context mContext;

    @Inject
    PreferencesManager mPreferencesManager;

    @Inject
    RestService mRestService;

    @Inject
    RealmManager mRealmManager;

    @Inject
    ConnectionChecker mConnectionChecker;

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    @Inject
    OkHttpClient mOkHttpClient;

    //@Inject
    //JobManager mJobManager;


    private boolean mIsCatalogLoading;//каталог загружается (чтобы не прерывать например при повороте экрана)

    private int mErrorCount;//

    //Subjects для инициализации и обновления имени и телефона
    private PublishSubject<UserInfoDto> mUserInfoSubject = PublishSubject.create();

    private PublishSubject<String> mAvatarSubject = PublishSubject.create();

    //auth state listener
    private PublishSubject<Boolean> mAuthStateSubject = PublishSubject.create();

    public PublishSubject<Boolean> getAuthStateSubject() {
        return mAuthStateSubject;
    }

    private PublishSubject<MessageDto> mMessageSubject = PublishSubject.create();
    private PublishSubject<MessageDto> mErrorSubject = PublishSubject.create();

    private BehaviorSubject<Boolean> mProgressSubject = BehaviorSubject.create();

    private DisposableObserver<Long> mSyncDisposable;

    public DataManager() {
        MyApplication.getDaggerComponent().inject(this);
        /*
        if (mPreferencesManager.checkAuthToken()) {
            generateMockUserData();
        } else {
            logout();
        }
        */
        //enableCatalogSynchronization();//moved to di module
    }


    private boolean isSynchronizationStarted() {
        return mSyncDisposable != null && !mSyncDisposable.isDisposed();
    }

    private boolean checkConnection() {
        boolean isConnected = mConnectionChecker.isConnected();
        if (mConnectionChecker.isConnected()) {
            return true;
        } else {
            //mErrorSubject.onNext(R.string.no_internet);
            sendError(new MessageDto(R.string.no_internet));
            return false;
        }
    }


    private Observable<Long> getCatalogSynchronizationObservable() {
        return Observable.interval(0, UPDATE_DATA_INTERVAL, SECONDS)
                .doOnNext(aLong -> {
                    Log.d(LOG_SYNC, "catalog synchronization count=" + aLong);
                })
                .filter(aLong -> DataManager.this.checkConnection())

                .doOnNext(aLong -> {
                    DataManager.this.setCatalogLoading(true);
                    DataManager.this.loadProductsFromServer();
                })
                .doOnError(e -> {
                    sendError(new MessageDto(R.string.sync_stopped_error));
                    Log.e(LOG_SYNC, "Error: catalog synchronization stopped doOnError " + e);
                })
                .doOnComplete(() -> Log.e(LOG_SYNC, "Error: catalog synchronization stopped doOnComplete"))
                .subscribeOn(Schedulers.io());
    }


    public void enableCatalogSynchronization() {
        if (!isSynchronizationStarted()) {
            mSyncDisposable =
                    getCatalogSynchronizationObservable()
                            .subscribeWith(new SimpleDisposableObserver<>());
            Log.d(LOG_SYNC, "enableCatalogSynchronization enabled");
        }
    }

    //перезапускаем (переподписываем), чтобы не ждать
    //но если сейчас обраб. запрос, то не прерывать
    public void forceCatalogSynchronization() {
        Log.d(LOG_TAG, "DataManager forceCatalogSynchronization - " + !mIsCatalogLoading);
        if (!mIsCatalogLoading) {
            disableCatalogSynchronization();
            enableCatalogSynchronization();
        }

    }

    private void disableCatalogSynchronization() {
        if (isSynchronizationStarted()) {
            mSyncDisposable.dispose();
            Log.d(LOG_SYNC, "disableCatalogSynchronization disabled");
        }
    }


    /*
        public void generateMockUserData() {
            mPreferencesManager.saveAuthToken(TEST_TOKEN);
            if (mPreferencesManager.getUserName().isEmpty()) {
                mPreferencesManager.saveUserName("Ваше имя");
            }
            if (mPreferencesManager.getPhone().isEmpty()) {
                mPreferencesManager.savePhone("+1 111 111 11 11");
            }
            refreshUserInfo();
            refreshAuthState();
        }
    */
    private void refreshAuthState(boolean newIsAuthState) {
        Log.d(LOG_TAG, "DataManager refreshAuthState newIsAuthState=" + newIsAuthState + " subject=" + mAuthStateSubject);
        mAuthStateSubject.onNext(newIsAuthState);
    }

    private void refreshProgress(boolean isLoading) {
        mProgressSubject.onNext(isLoading);
    }

    public BehaviorSubject<Boolean> getProgressSubject() {
        return mProgressSubject;
    }

    public void logout() {
        LoginManager.getInstance().logOut();
        mPreferencesManager.removeProfileInfo();
        refreshUserInfo();
        refreshAvatar();
        //refreshAuthState(false);
    }

    public void removeProfileInfo() {
        mPreferencesManager.removeProfileInfo();
    }

    public boolean isUserAuth() {
        return mPreferencesManager.checkAuthToken();
    }


    public void saveUserInfo(UserInfoDto userInfo) {
        mPreferencesManager.saveUserName(userInfo.getName());
        mPreferencesManager.savePhone(userInfo.getPhone());
        refreshUserInfo();
    }

    private UserInfoDto getUserInfo() {

        return isUserAuth() ? new UserInfoDto(mPreferencesManager.getUserName(), mPreferencesManager.getPhone())
                : UserInfoDto.getEmptyInstance();
    }

    public void saveUserSettings(UserSettingsDto userSettings) {
        mPreferencesManager.saveIsOrderNotification(userSettings.isOrderNotification());
        mPreferencesManager.saveIsPromoNotification(userSettings.isPromoNotification());
    }

    public UserSettingsDto getUserSettings() {
        return new UserSettingsDto(mPreferencesManager.isOrderNotification(), mPreferencesManager.isPromoNotification());
    }

    /*
        public Observable<Response<List<ProductRes>>> getRetrofitGetProductsObservable() {
            return mRestService.getProductsObservable(getProductLastModified());
        }
    */
    public ProductCommentRes postComment(String productId, float rating, String mes) throws Exception {
        String userName = getUserInfo().getName();
        if (userName == null || userName.isEmpty()) {
            userName = AppConfig.NO_NAME;
        }
        mes = mes == null || mes.isEmpty() ? " " : mes;//api не разрешает пустой комментарий

        String avatar = mPreferencesManager.getAvatarUrl();
        ProductCommentReq commentReq = new ProductCommentReq(rating, mes, userName, avatar);
//сеть проверяется в job
        Call<ProductCommentRes> callComment = postComment(productId, commentReq);

        Response<ProductCommentRes> response = callComment.execute();
        if (response.code() != 201) {
            throw new Exception(ErrorUtils.parseError(response));
        }
        return response.body();

    }

    private Call<ProductCommentRes> postComment(String productId, ProductCommentReq commentReq) {
        return mRestService.postComment(productId, commentReq);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String uploadAvatar(String fileLocalUrl) throws Exception {
        //File file=new File(fileLocalUrl);
        String filePath = FileUtils.getPathByUri(mContext, Uri.parse(fileLocalUrl));
        if (filePath == null) return null;

        File file = new File(filePath);
        if (!file.isFile()) {
            Log.e(LOG_TAG, "uploadAvatar " + filePath + " is not a file!");
            return null;
        }

        file = Compressor.getDefault(mContext).compressToFile(file);//resize (too large for uploading)
        String url = uploadFile(file);
        sendMessage(new MessageDto(R.string.avatar_uploaded));
        return url;
    }

    private String uploadFile(File file) throws Exception {

        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part fileBody = MultipartBody.Part.createFormData("avatar", file.getName(), requestBody);
        Call<AvatarUrlRes> callAvatarUrl = mRestService.uploadUserAvatar(fileBody);
        Response<AvatarUrlRes> response = callAvatarUrl.execute();
        if (response.code() != 200) {
            throw new Exception(ErrorUtils.parseError(response));
        }
        return response.body().getAvatarUrl();
    }

    public RealmManager getRealmManager() {
        return mRealmManager;
    }

    private void loadProductsFromServer() {
        getNetworkProductsObservable()
                .subscribe(productList -> mRealmManager.synchronizeWithServerData(productList,
                        () -> {
                            Log.d(LOG_SYNC, "synchronize with response done!");
                            sendMessage(new MessageDto(R.string.catalog_updated));
                        }));
    }


    //сеть уже проверили до вызова
    private Observable<List<ProductRes>> getNetworkProductsObservable() {
        return mRestService.getProductsObservable(mPreferencesManager.getProductLastModified()) //запрашиваем данные из сети
                .doOnNext(response -> {
                    Log.d(LOG_SYNC, "getNetworkProductsObservable doOnNext response code=" + response.code());
                })
                .flatMap(response -> {
                    switch (response.code()) {
                        case 200:
                            String lastModified = response.headers().get(LAST_MODIFIED_HEADER);
                            if (lastModified != null) {
                                mPreferencesManager.saveProductLastModified(lastModified);
                            }
                            return Observable.just(response.body());
                        case 304:
                            return Observable.empty();
                        default:
                            return Observable.error(ErrorUtils.parseError(response));
                    }
                })
                .doOnNext(response -> {
                    //setCatalogLoading(true);
                    Log.d(LOG_SYNC, "getNetworkProductsObservable doOnNext");
                })
                .retryWhen(attempts -> attempts
                        .zipWith(Observable.range(1, ConstantManager.RETRY_COUNT + 1), (e, i) -> {
                            Log.e(LOG_SYNC, "product loading error  retry i=" + i
                                    + " delay=" + Utils.getExponentialDelayInMs(i, RETRY_INITIAL_DELAY), e);
                            if (i == ConstantManager.RETRY_COUNT + 1) {//потому что не появится onError
                                sendMessage(new MessageDto(R.string.update_error));
                                return Observable.empty();
                            } else {
                                return Observable.timer(Utils.getExponentialDelayInMs(i, RETRY_INITIAL_DELAY), MILLISECONDS);
                            }
                        }))
                .doOnNext(response -> {
                    //setCatalogLoading(true);
                    Log.d(LOG_SYNC, "getNetworkProductsObservable doOnNext");
                })
                .doOnComplete(() -> {
                    setCatalogLoading(false);
                    Log.d(LOG_SYNC, "getNetworkProductsObservable doOnComplete");
                })
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void sendMessage(MessageDto messageDto) {
        mMessageSubject.onNext(messageDto);
    }

    public void sendError(MessageDto messageDto) {
        mErrorSubject.onNext(messageDto);
    }

    public PublishSubject<MessageDto> getMessageSubject() {
        return mMessageSubject;
    }

    public PublishSubject<MessageDto> getErrorSubject() {
        return mErrorSubject;
    }

    private PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    /*
        public JobManager getJobManager() {
            return mJobManager;
        }
    */
    public boolean isCatalogLoading() {
        return mIsCatalogLoading;
    }

    public PublishSubject<UserInfoDto> getUserInfoSubject() {
        return mUserInfoSubject;
    }

    public PublishSubject<String> getAvatarSubject() {
        return mAvatarSubject;
    }

    public void refreshUserInfo() {
        mUserInfoSubject.onNext(getUserInfo());
    }

    public void refreshAvatar() {
        mAvatarSubject.onNext(getAvatarUrl());
    }

    public String getAvatarUrl() {
        return mPreferencesManager.getAvatarUrl();
    }

    private void setCatalogLoading(boolean catalogLoading) {
        mIsCatalogLoading = catalogLoading;
    }


    public void loginToApi(UserReq userReq) {
        //EspressoIdlingResource.increment();
        Observable.just(checkConnection())
                .filter(isCon -> isCon)
                .doOnNext(this::refreshProgress)
                .flatMap(isCon -> loginToApiObservable(userReq))
                .flatMap(response -> {
                    switch (response.code()) {
                        case 200:
                            return Observable.just(response.body());
                        case 403:
                            return Observable.error(new LoginApiException());
                        default:
                            return Observable.error(ErrorUtils.parseError(response));
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SimpleDisposableObserver<UserRes>() {
                    @Override
                    public void onNext(UserRes userRes) {
                        mPreferencesManager.saveProfileInfo(userRes);
                        refreshAuthState(true);
                        refreshProgress(false);
                        refreshUserInfo();
                        refreshAvatar();
                    }

                    @Override
                    public void onError(Throwable e) {
                        refreshProgress(false);
                        if (e instanceof LoginApiException)
                            sendError(new MessageDto(((LoginApiException) e).getMessageRes()));
                        else sendError(new MessageDto(e.getMessage()));
                    }
                });
    }

    private Observable<Response<UserRes>> loginToApiObservable(UserReq userReq) {
        Log.d(LOG_TAG, "loginToApiObservable userReq=" + userReq);
        if (userReq instanceof UserLoginReq) {
            return mRestService.loginUser((UserLoginReq) userReq);
        } else {
            return mRestService.loginUser((UserSocReq) userReq);
        }
    }

    /*
        public void saveAvatarServerUrl(String avatarServerUrl) {
            mPreferencesManager.saveAvatarServerUrl(avatarServerUrl);
            requestAvatar();
        }

        public void saveAvatarUrl(String avatarUrl) {
            mPreferencesManager.saveAvatarUrl(avatarUrl);
            requestAvatar();
        }
        */
    public void saveAvatarUrl(String avatarUrl) {
        mPreferencesManager.saveAvatarUrl(avatarUrl);
        refreshAvatar();
    }

}