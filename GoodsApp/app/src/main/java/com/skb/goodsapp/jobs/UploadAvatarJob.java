package com.skb.goodsapp.jobs;


import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.Log;

import com.birbit.android.jobqueue.Params;
import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.dto.MessageDto;

import static com.skb.goodsapp.ConstantManager.JOB_GROUP_UPLOADS;
import static com.skb.goodsapp.ConstantManager.LOG_UPLOAD;


public class UploadAvatarJob extends BaseJob {

    public final static int SUCCESS_MES = R.string.avatar_uploaded;
    public final static int CANCEL_MES = R.string.avatar_upload_cancelled;

    private String mLocalUrl;

    public UploadAvatarJob(String localUrl) {
        super(new Params(JobPriority.HIGH)
                .requireNetwork()
                .groupBy(JOB_GROUP_UPLOADS)
                .persist());
        mLocalUrl = localUrl;
        setSuccessMes(new MessageDto(SUCCESS_MES));
        setCancelMes(new MessageDto(CANCEL_MES));
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onRun() throws Throwable {
        super.onRun();
        String avatarServerUrl = mDataManager.uploadAvatar(mLocalUrl);
        if (avatarServerUrl == null) {
            Log.e(LOG_UPLOAD, "UploadAvatarJob onRun can't get file to upload!");
            throw new Exception();
        }
        mDataManager.saveAvatarUrl(avatarServerUrl);
        Log.d(LOG_UPLOAD, "UploadAvatarJob onRun finished");
    }

    @Override
    protected void onCancel(int i, @Nullable Throwable throwable) {
        super.onCancel(i, throwable);
    }
}
