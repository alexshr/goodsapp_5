package com.skb.goodsapp.ui.screens.description;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.ProductRealm;
import com.skb.goodsapp.di.DaggerService;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class ProductDescriptionView extends RelativeLayout {

    @BindView(R.id.product_description)
    TextView mProductDescriptionView;
    @BindView(R.id.product_count)
    TextView mProductCountView;
    @BindView(R.id.product_price)
    TextView mProductCostView;
    @BindView(R.id.product_rating)
    RatingBar mProductRatingBar;

    @BindView(R.id.minus_button)
    ImageButton mMinusButton;
    @BindView(R.id.plus_button)
    ImageButton mPlusButton;
    @BindView(R.id.favorite_checkbox)
    AppCompatCheckBox mFavoriteBtn;
    @BindView(R.id.basket_button)
    AppCompatImageButton mBasketBtn;

    @OnClick({R.id.minus_button, R.id.plus_button, R.id.basket_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.minus_button:
                mPresenter.decrementProductCount();
                break;
            case R.id.plus_button:
                mPresenter.incrementProductCount();
                break;
            case R.id.basket_button:
                mPresenter.addOrUpdateBasketProduct();
        }
    }

    @OnCheckedChanged(R.id.favorite_checkbox)
    public void onCheckedChangeListener(CompoundButton checkBox, boolean isChecked) {
        mPresenter.onFavoriteChange(isChecked);
    }

    @Inject
    ProductDescriptionScreen.ProductDescriptionPresenter mPresenter;

    private int mPrice;

    public ProductDescriptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<ProductDescriptionScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
            mProductCountView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int count = Integer.parseInt(s + "");
                    mProductCostView.setText(ProductRealm.formatCost(mPrice * count));
                }
            });
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    public void showProductArea(ProductRealm product) {
        mProductDescriptionView.setText(product.getDescription());
        mPrice = product.getPrice();
    }

    public void showBasketArea(int count) {
        mProductCountView.setText(count + "");
    }

    public void showFavoriteArea(boolean isFavorite) {
        mFavoriteBtn.setChecked(isFavorite);
    }

    public int getProductCount() {
        return Integer.parseInt(mProductCountView.getText().toString());
    }

    public void setFavorite(boolean isFavorite) {
        mFavoriteBtn.setChecked(isFavorite);
    }
}
