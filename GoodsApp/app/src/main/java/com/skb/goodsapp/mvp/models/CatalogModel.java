package com.skb.goodsapp.mvp.models;

public class CatalogModel extends RootModel {

    //работает синхронизация с опред. интервалом, однако можно форсировать
    public void forceCatalogSynchronization() {
        mDataManager.forceCatalogSynchronization();
    }
}
