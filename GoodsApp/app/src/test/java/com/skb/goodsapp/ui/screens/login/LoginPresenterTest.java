package com.skb.goodsapp.ui.screens.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;

import com.birbit.android.jobqueue.JobManager;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.skb.goodsapp.MyApplication;
import com.skb.goodsapp.R;
import com.skb.goodsapp.TestSchedulerRule;
import com.skb.goodsapp.data.storage.dto.ActivityResultDto;
import com.skb.goodsapp.data.storage.network.UserSocReq;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.NavigationDrawerHelper;
import com.skb.goodsapp.flow.ScreenMenu;
import com.skb.goodsapp.jobs.BaseJob;
import com.skb.goodsapp.mvp.models.LoginModel;
import com.skb.goodsapp.mvp.models.RootModel;
import com.skb.goodsapp.mvp.presenters.RootPresenter;
import com.skb.goodsapp.ui.activities.RootActivity;
import com.skb.goodsapp.ui.screens.login.LoginScreen.LoginPresenter;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKServiceActivity;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKRequest.VKRequestListener;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.methods.VKApiUsers;

import junit.framework.Assert;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import flow.Flow;
import flow.History;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import mortar.MortarScope;
import mortar.bundler.BundleServiceRunner;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
//@PrepareEverythingForTest

@PrepareForTest({MyApplication.class, RootPresenter.class, LoginPresenter.class, Flow.class, ContextCompat.class, NavigationDrawerHelper.class, History.class, Build.VERSION.class, VKSdk.class, TransitionManager.class, Profile.class, GraphRequest.class, AccessToken.class, Uri.class, Bundle.class, FacebookCallback.class, VKApi.class})


public class LoginPresenterTest {

    @Rule
    public TestSchedulerRule testSchedulerRule = new TestSchedulerRule();

    @Mock
    private RootModel mockRootModel;
    @Mock
    RootActivity mockRootActivity;
    @Mock
    RootActivity.Component mockRootActivityComponent;
    @Mock
    JobManager mockJobManager;
    @Mock
    Intent mockIntent;

    @Mock
    BaseJob mockBaseJob;

    @Mock
    NavigationView mockNavigationView;

    @Mock
    History mockHistory;

    @Mock
    ScreenMenu mockScreenMenu;

    @Mock
    ViewPager mockViewPager;

    @Mock
    LoginManager mockFbLoginManager;

    @Mock
    CallbackManager mockFbCallbackManager;

    @Mock
    TwitterAuthClient mockTwAuthClient;

    @Mock
    Callback<TwitterSession> mockTwSessionCallback;

    @Mock
    LoginView mockLoginView;

    @Mock
    Context mockContext;

    @Mock
    Flow mockFlow;

    @Mock
    RootPresenter mockRootPresenter;

    @Mock
    LoginModel mockLoginModel;

    @Mock
    VKCallback<VKAccessToken> mockVkLoginCallback;

    @Mock
    VKResponse mockVkResponse;

    @Mock
    RootPresenter.NavigationBuilder mockNavigationBuilder;

    @Mock
    Transition mockTransition;

    @Mock
    JSONObject mockJSONObject;

    @Mock
    GraphRequest mockGraphRequest;

    @Mock
    GraphResponse mockGraphResponse;

    @Mock
    LoginResult mockLoginResult;

    @Mock
    AccessToken mockAccessToken;

    @Mock
    Profile mockProfile;

    @Mock
    GraphRequest.GraphJSONObjectCallback mockFbRequestEmailCallback;

    @Mock
    Uri mockUri;

    @Mock
    FacebookException mockFacebookException;

    @Mock
    VKAccessToken mockVkAccessToken;

    @Mock
    VKApiUsers mockVkApiUsers;

    @Mock
    VKRequest mockVkRequest;

    @Mock
    VKRequestListener mockVkUserListener;

    @Mock
    VKError mockVkError;


    //@Mock
    //CompositeDisposable mockCompositeDisposable;


    //private MortarScope mMortarScope;

    private String mTestAvatarUrl = "http://skba1.mgbeta.ru/assets/avatars/1491972833904.png";

    private TestScheduler mTestScheduler;

    private RootPresenter mRootPresenter;

    private LoginPresenter mLoginPresenter;


    private static int sVkRequestCode = VKServiceActivity.VKServiceType.Authorization.getOuterCode();
    private static int sFbMockRequestCode = 999;//mock code
    private static int sTwMockRequestCode = TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE;

    @Before
    public void setUp() throws Exception {
        mockStatic(MyApplication.class, Flow.class, ContextCompat.class, NavigationDrawerHelper.class, Build.VERSION.class, VKSdk.class, TransitionManager.class, Profile.class, GraphRequest.class, Uri.class, VKApi.class);

        mTestScheduler = testSchedulerRule.getTestScheduler();

        mLoginPresenter = new LoginPresenter();
    }


    private LoginScreen.Component prepareDaggerComponent() {
        RootActivity.Component rootActivityComponent = DaggerService.buildComponent(RootActivity.Component.class, new RootActivity.RootPresenterModule() {
            @Override
            protected RootPresenter provideRootPresenter() {
                return mockRootPresenter;
            }
        });

        LoginScreen.Component loginComponent = DaggerService.buildComponent(LoginScreen.Component.class, rootActivityComponent, new LoginScreen.Module() {
            @Override
            protected LoginModel provideLoginModel() {
                return mockLoginModel;
            }

            @Override
            protected LoginManager provideFbLoginManager() {
                return mockFbLoginManager;
            }

            @Override
            protected CallbackManager provideFbCallbackManager() {
                return mockFbCallbackManager;
            }

            @Override
            protected TwitterAuthClient provideTwitterAuthClient() {
                return mockTwAuthClient;
            }
        });

        return loginComponent;
    }


    private MortarScope prepareMortarScope(BundleServiceRunner bundleServiceRunner, LoginScreen.Component loginComponent) {
        return MortarScope.buildRootScope()
                .withService(BundleServiceRunner.SERVICE_NAME, bundleServiceRunner)
                .withService(DaggerService.SERVICE_NAME, loginComponent)
                .build("MockRoot");
    }

    /**
     * метод позволяющий запустить takeView, замокать mortarScope и dagger service.
     * Однако инициализировать моками поля класса гораздо проще через PowerMockito
     * <p>
     * Не удалять - крутой пример как не надо мучиться)
     */
    private void prepareTakeView() {
        BundleServiceRunner bundleServiceRunner = new BundleServiceRunner();

        LoginScreen.Component loginComponent = prepareDaggerComponent();

        MortarScope mortarScope = prepareMortarScope(bundleServiceRunner, loginComponent);

        given(mockLoginView.getContext()).willReturn(mockContext);
        //noinspection WrongConstant
        given(mockContext.getSystemService(BundleServiceRunner.SERVICE_NAME)).willReturn(bundleServiceRunner);
        //noinspection WrongConstant
        given(mockContext.getSystemService(MortarScope.class.getName())).willReturn(mortarScope);
        //noinspection WrongConstant
        given(mockContext.getSystemService("flow.InternalContextWrapper.FLOW_SERVICE")).willReturn(mockFlow);
        //noinspection WrongConstant
        given(mockContext.getSystemService(BundleServiceRunner.SERVICE_NAME)).willReturn(bundleServiceRunner);

        given(mockNavigationBuilder.setTitle(any())).willReturn(mockNavigationBuilder);

        given(mockRootPresenter.createNavigationBuilder()).willReturn(mockNavigationBuilder);
        willDoNothing().given(mockNavigationBuilder).build();

        Mockito.when(TransitionManager.getDefaultTransition()).thenReturn(mockTransition);
        given(mockTransition.addListener(any())).willReturn(mockTransition);

        willDoNothing().given(mockLoginView).onLoginPanelStateChanged(anyBoolean());

        given(mockLoginModel.getAuthStateSubject()).willReturn(PublishSubject.create());
    }


    @After
    public void tearDown() throws Exception {
        //mRootPresenter.dropView(mockRootActivity);
    }

    //region ================= tests =================

    @Test
    public void getVkActivityResultObservable_req_vk_res_ok_test() throws Exception {
        getVkActivityResultObservable_test(sVkRequestCode, Activity.RESULT_OK);
    }

    @Test
    public void getVkActivityResultObservable_req_vk_res_cancelled_test() throws Exception {
        getVkActivityResultObservable_test(sVkRequestCode, Activity.RESULT_CANCELED);
    }

    @Test
    public void getVkActivityResultObservable_req_no_vk_res_ok_test() throws Exception {
        getVkActivityResultObservable_test(sVkRequestCode + 1, Activity.RESULT_OK);
    }

    @Test
    public void getVkActivityResultObservable_req_no_vk_res_cancelled_test() throws Exception {
        getVkActivityResultObservable_test(sVkRequestCode + 1, Activity.RESULT_CANCELED);
    }


    private void getVkActivityResultObservable_test(int requestCode, int resultCode) throws Exception {

        Whitebox.setInternalState(mLoginPresenter, "mRootPresenter", mockRootPresenter);

        ActivityResultDto activityResultDto = new ActivityResultDto(requestCode, resultCode, new Intent());
        BehaviorSubject<ActivityResultDto> activityResultSubject = BehaviorSubject.create();

        given(mockRootPresenter.getActivityResultSubject()).willReturn(activityResultSubject);

        Whitebox.setInternalState(mLoginPresenter, "mIsActivityResultWaiting", true);

        Whitebox.setInternalState(mLoginPresenter, "vkLoginCallback", mockVkLoginCallback);

        BDDMockito.BDDMyOngoingStubbing stub = given(VKSdk.onActivityResult(
                activityResultDto.getRequestCode(),
                activityResultDto.getResultCode(),
                activityResultDto.getIntent(),
                mockVkLoginCallback));

        if (requestCode == sVkRequestCode) {
            if (resultCode == Activity.RESULT_OK) {
                stub.willReturn(true);
            } else {
                stub.willThrow(Exception.class);
            }

        } else {
            stub.willReturn(false);
        }

        //when
        mLoginPresenter.getVkActivityResultObservable().test();
        activityResultSubject.onNext(activityResultDto);
        mTestScheduler.triggerActions();

        //then
        PowerMockito.verifyStatic(times(1));
        VKSdk.onActivityResult(activityResultDto.getRequestCode(),
                activityResultDto.getResultCode(),
                activityResultDto.getIntent(),
                mockVkLoginCallback);


        boolean isActivityResultWaiting = Whitebox.getInternalState(mLoginPresenter, "mIsActivityResultWaiting");
        assertEquals(requestCode != sVkRequestCode, isActivityResultWaiting);


    }

    @Test
    public void getFbActivityResultObservable_req_fb_res_ok_test() throws Exception {
        getFbActivityResultObservable_test(sFbMockRequestCode, Activity.RESULT_OK);
    }

    @Test
    public void getFbActivityResultObservable_req_fb_res_cancelled_test() throws Exception {
        getFbActivityResultObservable_test(sFbMockRequestCode, Activity.RESULT_CANCELED);
    }

    @Test
    public void getFbActivityResultObservable_req_no_fb_res_ok_test() throws Exception {
        getFbActivityResultObservable_test(sFbMockRequestCode + 1, Activity.RESULT_OK);
    }

    @Test
    public void getFbActivityResultObservable_req_no_fb_res_cancelled_test() throws Exception {
        getFbActivityResultObservable_test(sFbMockRequestCode + 1, Activity.RESULT_CANCELED);
    }

    private void getFbActivityResultObservable_test(int requestCode, int resultCode) throws Exception {

        Whitebox.setInternalState(mLoginPresenter, "mRootPresenter", mockRootPresenter);
        Whitebox.setInternalState(mLoginPresenter, "mFbCallbackManager", mockFbCallbackManager);

        ActivityResultDto activityResultDto = new ActivityResultDto(requestCode, resultCode, new Intent());
        BehaviorSubject<ActivityResultDto> activityResultSubject = BehaviorSubject.create();

        given(mockRootPresenter.getActivityResultSubject()).willReturn(activityResultSubject);

        Whitebox.setInternalState(mLoginPresenter, "mIsActivityResultWaiting", true);

        BDDMockito.BDDMyOngoingStubbing stub = given(mockFbCallbackManager.onActivityResult(
                activityResultDto.getRequestCode(),
                activityResultDto.getResultCode(),
                activityResultDto.getIntent()));

        stub.willReturn(requestCode == sFbMockRequestCode);


        //when
        mLoginPresenter.getFbActivityResultObservable().test();
        activityResultSubject.onNext(activityResultDto);
        mTestScheduler.triggerActions();

        //then
        then(mockFbCallbackManager).should(times(1)).onActivityResult(
                activityResultDto.getRequestCode(),
                activityResultDto.getResultCode(),
                activityResultDto.getIntent());

        boolean isActivityResultWaiting = Whitebox.getInternalState(mLoginPresenter, "mIsActivityResultWaiting");
        assertEquals(requestCode != sFbMockRequestCode, isActivityResultWaiting);


    }

    @Test
    public void getTwActivityResultObservable_req_tw_res_ok_test() throws Exception {
        getTwActivityResultObservable_test(sTwMockRequestCode, Activity.RESULT_OK);
    }

    @Test
    public void getTwActivityResultObservable_req_tw_res_cancelled_test() throws Exception {
        getTwActivityResultObservable_test(sTwMockRequestCode, Activity.RESULT_CANCELED);
    }

    @Test
    public void getTwActivityResultObservable_req_no_tw_res_ok_test() throws Exception {
        getTwActivityResultObservable_test(sTwMockRequestCode + 1, Activity.RESULT_OK);
    }

    @Test
    public void getTwActivityResultObservable_req_no_tw_res_cancelled_test() throws Exception {
        getTwActivityResultObservable_test(sTwMockRequestCode + 1, Activity.RESULT_CANCELED);
    }

    private void getTwActivityResultObservable_test(int requestCode, int resultCode) throws Exception {

        Whitebox.setInternalState(mLoginPresenter, "mRootPresenter", mockRootPresenter);
        Whitebox.setInternalState(mLoginPresenter, "mTwitterAuthClient", mockTwAuthClient);

        ActivityResultDto activityResultDto = new ActivityResultDto(requestCode, resultCode, new Intent());
        BehaviorSubject<ActivityResultDto> activityResultSubject = BehaviorSubject.create();

        given(mockRootPresenter.getActivityResultSubject()).willReturn(activityResultSubject);

        Whitebox.setInternalState(mLoginPresenter, "mIsActivityResultWaiting", true);

        willDoNothing().given(mockTwAuthClient).onActivityResult(
                activityResultDto.getRequestCode(),
                activityResultDto.getResultCode(),
                activityResultDto.getIntent());

        //when
        mLoginPresenter.getTwActivityResultObservable().test();
        activityResultSubject.onNext(activityResultDto);
        mTestScheduler.triggerActions();

        //then
        then(mockTwAuthClient).should(times(requestCode == sTwMockRequestCode ? 1 : 0)).onActivityResult(
                activityResultDto.getRequestCode(),
                activityResultDto.getResultCode(),
                activityResultDto.getIntent());

        boolean isActivityResultWaiting = Whitebox.getInternalState(mLoginPresenter, "mIsActivityResultWaiting");
        assertEquals(requestCode != sTwMockRequestCode, isActivityResultWaiting);

    }

    @Test
    public void clickOnFb_test() throws Exception {
        //given
        mLoginPresenter = spy(mLoginPresenter);

        when(mLoginPresenter, "getView").thenReturn(mockLoginView);
        Whitebox.setInternalState(mLoginPresenter, "mRootPresenter", mockRootPresenter);
        Whitebox.setInternalState(mLoginPresenter, "mFbLoginManager", mockFbLoginManager);
        Whitebox.setInternalState(mLoginPresenter, "mIsActivityResultWaiting", false);

        //when
        mLoginPresenter.clickOnFb();

        //then
        InOrder inOrder = inOrder(mockLoginView, mockRootPresenter);

        then(mockLoginView).should(inOrder).startFbAnimation();
        then(mockRootPresenter).should(inOrder).fbLogin(mockFbLoginManager);
        boolean isActivityResultWaiting = Whitebox.getInternalState(mLoginPresenter, "mIsActivityResultWaiting");
        assertEquals(true, isActivityResultWaiting);
    }

    @Test
    public void clickOnVk_test() throws Exception {
        //given
        mLoginPresenter = spy(mLoginPresenter);

        when(mLoginPresenter, "getView").thenReturn(mockLoginView);
        Whitebox.setInternalState(mLoginPresenter, "mRootPresenter", mockRootPresenter);
        Whitebox.setInternalState(mLoginPresenter, "mIsActivityResultWaiting", false);

        //when
        mLoginPresenter.clickOnVk();

        //then
        InOrder inOrder = inOrder(mockLoginView, mockRootPresenter);

        then(mockLoginView).should(inOrder).startVkAnimation();
        then(mockRootPresenter).should(inOrder).vkSdkLogin();
        boolean isActivityResultWaiting = Whitebox.getInternalState(mLoginPresenter, "mIsActivityResultWaiting");
        assertEquals(true, isActivityResultWaiting);
    }

    @Test
    public void clickOnTw_test() throws Exception {
        //given
        mLoginPresenter = spy(mLoginPresenter);

        when(mLoginPresenter, "getView").thenReturn(mockLoginView);
        Whitebox.setInternalState(mLoginPresenter, "mRootPresenter", mockRootPresenter);
        Whitebox.setInternalState(mLoginPresenter, "mTwitterAuthClient", mockTwAuthClient);
        Whitebox.setInternalState(mLoginPresenter, "mTwLoginCallback", mockTwSessionCallback);
        Whitebox.setInternalState(mLoginPresenter, "mIsActivityResultWaiting", false);

        //when
        mLoginPresenter.clickOnTwitter();

        //then
        InOrder inOrder = inOrder(mockLoginView, mockRootPresenter);

        then(mockLoginView).should(inOrder).startTwAnimation();
        then(mockRootPresenter).should(inOrder).twLogin(mockTwAuthClient, mockTwSessionCallback);
        boolean isActivityResultWaiting = Whitebox.getInternalState(mLoginPresenter, "mIsActivityResultWaiting");
        assertEquals(true, isActivityResultWaiting);
    }

    @Test
    public void fbRequestEmailCallback_test() throws Exception {
        //given
        String email = "test@test.ru";
        UserSocReq userReq = new UserSocReq(null, "Иван", "Иванов", mTestAvatarUrl, null);

        given(mockJSONObject.optString("email")).willReturn(email);

        Whitebox.setInternalState(mLoginPresenter, "mLoginModel", mockLoginModel);

        Whitebox.setInternalState(mLoginPresenter, "mUserSocReq", userReq);

        GraphRequest.GraphJSONObjectCallback fbRequestEmailCallback = Whitebox.getInternalState(mLoginPresenter, "fbRequestEmailCallback");

        //when
        fbRequestEmailCallback.onCompleted(mockJSONObject, mockGraphResponse);

        //then
        then(mockLoginModel).should(times(1)).login(userReq);
        assertEquals(email, userReq.getEmail());

    }

    @Test
    public void fbLoginCallback_onSuccess_test() throws Exception {

        //given
        given(Profile.getCurrentProfile()).willReturn(mockProfile);

        given(mockProfile.getId()).willReturn("ivan_66");
        given(mockProfile.getProfilePictureUri(200, 200)).willReturn(mockUri);
        given(mockProfile.getFirstName()).willReturn("Иван");
        given(mockProfile.getMiddleName()).willReturn("Иванович");
        given(mockProfile.getLastName()).willReturn("Иванов");
        given(mockProfile.getName()).willReturn("Ivan");

        given(mockUri.toString()).willReturn(mTestAvatarUrl);

        given(mockLoginResult.getAccessToken()).willReturn(mockAccessToken);

        Whitebox.setInternalState(mLoginPresenter, "fbRequestEmailCallback", mockFbRequestEmailCallback);
        given(GraphRequest.newMeRequest(mockAccessToken, mockFbRequestEmailCallback)).willReturn(mockGraphRequest);

        FacebookCallback<LoginResult> fbLoginCallback = Whitebox.getInternalState(mLoginPresenter, "fbLoginCallback");

        System.out.println("fbLoginCallback =" + fbLoginCallback);

        //when
        fbLoginCallback.onSuccess(mockLoginResult);

        //then
        InOrder inOrder = inOrder(mockGraphRequest);

        UserSocReq userSocReq = Whitebox.getInternalState(mLoginPresenter, "mUserSocReq");
        Assert.assertEquals(mockProfile.getFirstName(), userSocReq.getFirstName());
        Assert.assertEquals(mockProfile.getLastName(), userSocReq.getLastName());
        Assert.assertEquals(mockProfile.getProfilePictureUri(200, 200).toString(), userSocReq.getAvatarUrl());

        PowerMockito.verifyStatic(times(1));
        GraphRequest.newMeRequest(mockAccessToken, mockFbRequestEmailCallback);

        then(mockGraphRequest).should(inOrder).setParameters(any());
        then(mockGraphRequest).should(inOrder).executeAsync();
    }

    @Test
    public void fbLoginCallback_onCancel_test() throws Exception {

        //given
        Whitebox.setInternalState(mLoginPresenter, "mRootPresenter", mockRootPresenter);
        FacebookCallback<LoginResult> fbLoginCallback = Whitebox.getInternalState(mLoginPresenter, "fbLoginCallback");

        //when
        fbLoginCallback.onCancel();

        //then
        then(mockRootPresenter).should(times(1)).showMessage(argThat(message -> message.getMessageRes() == R.string.login_cancelled));
    }

    @Test
    public void fbLoginCallback_onError_test() throws Exception {

        //given
        Whitebox.setInternalState(mLoginPresenter, "mRootPresenter", mockRootPresenter);

        FacebookCallback<LoginResult> fbLoginCallback = Whitebox.getInternalState(mLoginPresenter, "fbLoginCallback");
        String errorMessage = "error message";
        given(mockFacebookException.getLocalizedMessage()).willReturn(errorMessage);

        //when
        fbLoginCallback.onError(mockFacebookException);

        //then
        then(mockRootPresenter).should(times(1)).showError(argThat(message -> message.getText().equals(errorMessage)));
    }

    @Test
    public void vkLoginCallback_onResult_test() throws Exception {

        //given
        VKCallback<VKAccessToken> vkLoginCallback = Whitebox.getInternalState(mLoginPresenter, "vkLoginCallback");
        Whitebox.setInternalState(mLoginPresenter, "vkUserListener", mockVkUserListener);

        given(VKApi.users()).willReturn(mockVkApiUsers);
        given(mockVkApiUsers.get()).willReturn(mockVkRequest);

        //when
        vkLoginCallback.onResult(mockVkAccessToken);

        //then
        PowerMockito.verifyStatic(times(1));
        VKApi.users();

        then(mockVkRequest).should(times(1)).executeWithListener(mockVkUserListener);
    }

    @Test
    public void vkLoginCallback_onError_test() throws Exception {

        //given
        VKCallback<VKAccessToken> vkLoginCallback = Whitebox.getInternalState(mLoginPresenter, "vkLoginCallback");
        Whitebox.setInternalState(mLoginPresenter, "mRootPresenter", mockRootPresenter);

        //when
        vkLoginCallback.onError(mockVkError);

        //then
        then(mockRootPresenter).should(times(1)).showError(argThat(message -> message.getMessageRes() == R.string.login_cancelled));
    }
}