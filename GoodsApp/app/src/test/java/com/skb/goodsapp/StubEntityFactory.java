package com.skb.goodsapp;

import com.skb.goodsapp.data.storage.dto.UserInfoDto;


public class StubEntityFactory {

    @SuppressWarnings("unchecked")
    public static <T> T makeStub(Class<T> stubEntityClass) {
        switch (stubEntityClass.getSimpleName()) {
            /*
            case "UserRes":
                return (T) new UserRes("58711631a242690011b1b26d", "Макеев Михаил","https://pp.userapi.com/c313129/v313129097/80ff/5U-iWkuFxEM.jpg","wegfvw;edcnw'lkedm93847983yuhefoij32lkml'kjvj30fewoidvn","89179711111", null);
            case "UserLoginReq":
                return (T) new UserLoginReq("anymail@mail.ru", "password");
            */
            case "UserInfoDto":
                return (T) new UserInfoDto("Михайлов Михаил", "89179711112");
            /*
            case "MenuItemHolder":
                return (T) new MenuItemHolder("Редактировать", R.drawable.ic_account_circle_black_24dp, mock(MenuItem.OnMenuItemClickListener.class));
            case "UserSignInReq":
                return (T) new UserSignInReq("Михаил", "Макеев", "https://pp.userapi.com/c313129/v313129097/80ff/5U-iWkuFxEM.jpg", "makweb@yandex.ru", "89179711111");
*/
            default:
                return null;
        }
    }
}
