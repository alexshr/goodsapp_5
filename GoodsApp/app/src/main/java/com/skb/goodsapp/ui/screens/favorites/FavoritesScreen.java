package com.skb.goodsapp.ui.screens.favorites;

import android.os.Bundle;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.AbstractScreen;
import com.skb.goodsapp.flow.Screen;
import com.skb.goodsapp.mvp.models.FavoritesModel;
import com.skb.goodsapp.mvp.presenters.RootPresenter;
import com.skb.goodsapp.ui.activities.RootActivity;
import com.skb.goodsapp.ui.screens.product_detail.ProductDetailScreen;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;
import javax.inject.Scope;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import mortar.ViewPresenter;


@Screen(R.layout.screen_favorites)
public class FavoritesScreen extends AbstractScreen {


    @Override
    public Object[] getModules() {
        return new Object[]{new Module()};
    }

    @Override
    public Class getComponentClass() {
        return Component.class;
    }


    //region ============================== DI ==============================

    @dagger.Module
    public class Module {

        @FavoritesScope
        @Provides
        FavoritesModel provideFavoritesModel() {
            return new FavoritesModel();
        }

        @FavoritesScope
        @Provides
        FavoritesPresenter provideFavoritesPresenter() {
            return new FavoritesPresenter();
        }

    }

    @FavoritesScope
    @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
    public interface Component {
        void inject(FavoritesPresenter presenter);

        void inject(FavoritesRealmAdapter adapter);

        void inject(FavoritesView view);

        //RootPresenter getRootPresenter();

        //FavoritesModel getFavoritesModel();
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface FavoritesScope {
    }

    //endregion

    //region ============================== Presenter ==============================

    public class FavoritesPresenter extends ViewPresenter<FavoritesView> {

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        FavoritesModel mFavoritesModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            setupNavigation();
            getView().initView(mFavoritesModel.getFavorites());
        }


        private void setupNavigation() {
            mRootPresenter.createNavigationBuilder()
                    .setTitle(R.string.title_favorites)
                    .build();
        }


        public void updateBasketFromUser(String productId, int count) {
            mFavoritesModel.updateBasketFromUser(productId, count);
            mRootPresenter.showMessage(new MessageDto(R.string.product_added_success));
        }

        public void goToProductDetail(String productId) {
            Flow.get(getView().getContext()).set(new ProductDetailScreen(productId));
        }

        public void deleteFavorite(String productId) {
            mFavoritesModel.deleteFavorite(productId);
        }
    }
}
