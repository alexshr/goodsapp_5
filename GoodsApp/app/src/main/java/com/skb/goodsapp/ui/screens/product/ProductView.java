package com.skb.goodsapp.ui.screens.product;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.percent.PercentFrameLayout;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.ProductRealm;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.utils.ImageUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ProductView extends LinearLayout {

    //region ================= butterknife =================
    @BindView(R.id.product_name)
    TextView mProductNameTv;
    @BindView(R.id.product_description)
    TextView mProductDescriptionTv;
    @BindView(R.id.product_image_layout)
    PercentFrameLayout mProductImageLayout;
    @BindView(R.id.product_image)
    ImageView mProductImage;
    @BindView(R.id.minus_button)
    ImageButton mMinusButton;
    @BindView(R.id.product_count_layout)
    LinearLayout mProductCountLayout;
    @BindView(R.id.product_count_text_view)
    TextView mProductCountTextView;
    @BindView(R.id.plus_button)
    ImageButton mPlusButton;
    @BindView(R.id.product_price_text_view)
    TextView mProductCostTextView;
    @BindView(R.id.product_card)
    CardView mProductCard;
    @BindView(R.id.detail_btn)
    Button mDetailBtn;
    @BindView(R.id.favorite_checkbox)
    AppCompatCheckBox mFavoriteCheckbox;

    @BindView(R.id.product_info_layout)
    LinearLayout mProductInfoLayout;//актуально только для land orient

    @OnClick({R.id.minus_button, R.id.plus_button, R.id.detail_btn, R.id.product_image})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.minus_button:
                mPresenter.clickOnMinus();
                break;
            case R.id.plus_button:
                mPresenter.clickOnPlus();
                break;
            case R.id.detail_btn:
                mPresenter.clickOnDetail();
                break;
            case R.id.product_image:
                mPresenter.clickProduct();
        }
    }

    @OnCheckedChanged(R.id.favorite_checkbox)
    public void onCheckedChangeListener(CompoundButton checkBox, boolean isChecked) {
        mPresenter.onFavoriteChange(isChecked);
    }

    //endregion

    @Inject
    ProductScreen.ProductPresenter mPresenter;
    private Unbinder mUnbinder;

    private Integer mProductImageHeight;
    private Integer mProductImageWidth;

    private int mPrice;

    public ProductView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<ProductScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region ================= life cycle =================
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            mUnbinder = ButterKnife.bind(this);
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
            mProductCountTextView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int count = Integer.parseInt(s + "");
                    mProductCostTextView.setText(ProductRealm.formatCost(mPrice * count));
                }
            });
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mUnbinder.unbind();
            mPresenter.dropView(this);
        }
    }
    //endregion

    public void showProductArea(ProductRealm product) {
        ImageUtils.showImage(getContext(), mProductImage, product.getImageUrl(), R.drawable.img_product_nophoto);
        mProductNameTv.setText(product.getProductName());
        mProductDescriptionTv.setText(product.getDescription());
        mPrice = product.getPrice();
    }

    public void showBasketArea(int count) {
        mProductCountTextView.setText(count + "");
    }

    public void showFavoriteArea(boolean isFavorite) {
        mFavoriteCheckbox.setChecked(isFavorite);
    }

    //для добавления в корзину
    public int getProductCount() {
        return Integer.parseInt(mProductCountTextView.getText().toString());
    }

    /*
    private void setupProductImage(ProductRealm product) {

        Picasso.with(getContext().getApplicationContext())
                .load(product.getImageUrl())
                .placeholder(R.drawable.img_nophoto)
                .fit()
                .centerInside()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(mProductImage, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d(LOG_TAG, "product image is loaded from cache");
                    }

                    @Override
                    public void onError() {
                        Picasso.with(getContext().getApplicationContext())
                                .load(product.getImageUrl())
                                .error(R.drawable.img_nophoto)
                                .placeholder(R.drawable.img_nophoto)
                                .fit()
                                .centerInside()
                                .into(mProductImage, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                        Log.d(LOG_TAG, "product image is loaded from network");
                                    }

                                    @Override
                                    public void onError() {
                                        Log.d(LOG_TAG, "product image loading error");
                                    }
                                });
                    }
                });
    }
*/


    public void provideZoomState(boolean isZoomed) {

        View[] viewsToFade = getContext().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ?
                new View[]{mProductNameTv, mProductDescriptionTv, mProductCountLayout, mDetailBtn, mFavoriteCheckbox} :
                new View[]{mProductCountLayout, mProductInfoLayout};

        for (View view : viewsToFade) {
            view.setVisibility(isZoomed ? GONE : VISIBLE);
        }

        PercentFrameLayout.LayoutParams imageLayoutParam = (PercentFrameLayout.LayoutParams) mProductImage.getLayoutParams();
        if (isZoomed) {
            if (mProductImageHeight == null) {
                mProductImageHeight = imageLayoutParam.height;
                mProductImageWidth = imageLayoutParam.width;
            }
            imageLayoutParam.height = ViewGroup.LayoutParams.MATCH_PARENT;
            imageLayoutParam.width = ViewGroup.LayoutParams.MATCH_PARENT;

            mProductCard.setContentPadding(0, 0, 0, 0);
        } else {
            imageLayoutParam.height = mProductImageHeight;
            imageLayoutParam.width = mProductImageWidth;
            Resources res = getContext().getResources();
            mProductCard.setContentPadding(
                    res.getDimensionPixelSize(R.dimen.screen_product_card_padding_h),
                    res.getDimensionPixelSize(R.dimen.screen_product_card_padding_top),
                    res.getDimensionPixelSize(R.dimen.screen_product_card_padding_h),
                    res.getDimensionPixelSize(R.dimen.screen_product_card_padding_bottom)
            );
        }
        mProductImage.setLayoutParams(imageLayoutParam);

        ViewGroup.LayoutParams containerLayoutParam = mProductImageLayout.getLayoutParams();
        containerLayoutParam.height = isZoomed ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.WRAP_CONTENT;
        mProductImageLayout.setLayoutParams(containerLayoutParam);
    }

    public ProductScreen.ProductPresenter getPresenter() {
        return mPresenter;
    }
}