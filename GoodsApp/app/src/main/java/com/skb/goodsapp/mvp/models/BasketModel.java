package com.skb.goodsapp.mvp.models;

import com.skb.goodsapp.data.storage.network.BasketProductRes;

import java.util.List;

public class BasketModel extends RootModel {

    public void deleteFromBasket(String productId) {
        mRealmManager.deleteFromBasketAsync(productId);
    }

    public void updateBasketFromServer(List<BasketProductRes> basketProductResList) {
        mRealmManager.updateBasketFromServer(basketProductResList);
    }


}
