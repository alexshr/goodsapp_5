package com.skb.goodsapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import static com.skb.goodsapp.ConstantManager.LOG_TAG;


public class ConnectionChecker {

    private Context context;

    /*
    public static Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) MyApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }

    public static Observable<Boolean> isNetworkAvailableRx() {
        return Observable.just(ConnectionChecker.isNetworkAvailable());
    }
*/

    public ConnectionChecker(Context context) {
        this.context = context;
    }

    /*
        //NPE при отсутствии сети
        public Observable<Boolean> getConnectionStatusObservable() {
            IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
            return RxBroadcast.fromBroadcast(context, filter)
                    .map(i -> getNetworkInfo())
                    .map(info -> info != null && info.isConnected())
                    .distinctUntilChanged();
        }
    */
    private NetworkInfo getNetworkInfo() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo();
    }

    public Boolean isConnected() {
        NetworkInfo info = getNetworkInfo();
        boolean res = info != null && info.isConnected();
        Log.d(LOG_TAG, "ConnectionChecker isConnected=" + res);
        return res;
    }
}
