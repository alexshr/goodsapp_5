package com.skb.goodsapp.ui.screens.basket;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.BasketProductRealm;
import com.skb.goodsapp.data.storage.realm.ProductRealm;
import com.skb.goodsapp.utils.ImageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import me.angrybyte.numberpicker.listener.OnValueChangeListener;
import me.angrybyte.numberpicker.view.ActualNumberPicker;

public class BasketRealmAdapter extends RealmRecyclerViewAdapter<BasketProductRealm, BasketRealmAdapter.ViewHolder> {

    private Context mContext;
    private ActionItemListener mActionListener;
    private OrderedRealmCollection<BasketProductRealm> mAdapterData;

    public BasketRealmAdapter(Context context, OrderedRealmCollection<BasketProductRealm> data, ActionItemListener actionListener) {
        super(data, true);
        mAdapterData = data;
        mContext = context;
        mActionListener = actionListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_basket, parent, false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        BasketProductRealm basketProduct = getData().get(position);
        ProductRealm product = basketProduct.getProduct();
        holder.mProductId = basketProduct.getProductId();

        ImageUtils.showImage(mContext, holder.productImageView, product.getImageUrl(), R.drawable.img_product_nophoto);
        holder.productImageView.setOnClickListener(v -> mActionListener.onImageClicked(holder.mProductId));

        holder.mProductNameView.setText(product.getProductName());
        holder.mProductDescriptionView.setText(product.getDescription());

        holder.mProductPriceView.setText(ProductRealm.formatCost(product.getPrice()));
        holder.mProductNumberPicker.setValue(basketProduct.getCount());
        holder.mProductCostView.setText(ProductRealm.formatCost(product.getPrice() * basketProduct.getCount()));

        holder.mDeleteBtn.setOnClickListener(v -> mActionListener.onDeleteClicked(holder.mProductId));

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        //region ================= butterknife =================
        @BindView(R.id.product_image)
        ImageView productImageView;

        @BindView(R.id.product_name)
        TextView mProductNameView;
        @BindView(R.id.product_description)
        TextView mProductDescriptionView;

        @BindView(R.id.product_number_picker)
        ActualNumberPicker mProductNumberPicker;
        @BindView(R.id.price)
        TextView mProductPriceView;
        @BindView(R.id.cost)
        TextView mProductCostView;
        @BindView(R.id.delete_btn)
        Button mDeleteBtn;
        //endregion

        String mProductId;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mDeleteBtn.setOnClickListener(v -> mActionListener.onDeleteClicked(mProductId));
            mProductNumberPicker.setListener(new OnValueChangeListener() {
                @Override
                public void onValueChanged(int oldValue, int newValue) {
                    mActionListener.onProductNumberChanged(mProductId, newValue);
                }
            });
        }
    }

    public interface ActionItemListener {
        public void onDeleteClicked(String id);

        public void onImageClicked(String id);

        public void onProductNumberChanged(String productId, int count);
    }
}
