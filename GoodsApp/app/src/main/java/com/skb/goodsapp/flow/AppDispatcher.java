package com.skb.goodsapp.flow;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ViewFlipper;

import com.skb.goodsapp.MyApplication;
import com.skb.goodsapp.R;
import com.skb.goodsapp.data.managers.PreferencesManager;
import com.skb.goodsapp.mortar.ScreenScoper;
import com.skb.goodsapp.ui.activities.RootActivity;

import javax.inject.Inject;

import flow.Dispatcher;
import flow.State;
import flow.Traversal;
import flow.TraversalCallback;
import mortar.MortarScope;

import static com.skb.goodsapp.ConstantManager.LOG_TAG;
import static flow.Direction.FORWARD;

public class AppDispatcher implements Dispatcher {
    private RootActivity mActivity;

    @Inject
    PreferencesManager mPreferencesManager;

    public AppDispatcher(RootActivity activity) {
        mActivity = activity;
        MyApplication.getDaggerComponent().inject(this);
    }

    @Override
    public void dispatch(@NonNull Traversal traversal, @NonNull TraversalCallback callback) {

        State inState = traversal.getState(traversal.destination.top());
        AbstractScreen inScreen = inState.getKey();
        State outState = traversal.origin == null ? null : traversal.getState(traversal.origin.top());
        AbstractScreen outScreen = outState == null ? null : (AbstractScreen) outState.getKey();

        Log.d(LOG_TAG, "AppDispatcher outScreen=" + outScreen + " inScreen=" + inScreen + " direction=" + traversal.direction);

        if (inScreen.equals(outScreen)) {
            if (traversal.direction.equals(FORWARD)) {
                //загружаем заново тот же экран (например, logout)
                outScreen = null;
            } else {
                callback.onTraversalCompleted();
                return;
            }
        }

        if (outScreen != null) {
            if (traversal.direction.equals(FORWARD)) {

                //не удаляем, т.к. parent scope или просто возможен возврат
                ViewFlipper mViewFlipper = (ViewFlipper) mActivity.findViewById(R.id.root_container);
                outState.save(mViewFlipper.getChildAt(0));
            } else {
                //иначе удаляем
                MortarScope outScope = ScreenScoper.provideScopeFromBaseContext(outScreen, mActivity);
                outScope.destroy();
                Log.d(LOG_TAG, "AppDispatcher scope was destroyed: " + outScope.getName());
            }
        }

        callback.onTraversalCompleted();//перенесли с конца сюда, чтобы далее обеспечить правильную историю

        //mActivity.setCurrentScreen(inScreen);

        //создаем контекст (с мортаром) нового экрана
        Context context = traversal.createContext(inScreen, mActivity);
        MortarScope inScope = ScreenScoper.provideScopeFromBaseContext(inScreen, mActivity);
        context = inScope.createContext(context);

        //создаем новый view
        int layout = inScreen.getLayoutId();
        ViewFlipper mViewFlipper = (ViewFlipper) mActivity.findViewById(R.id.root_container);
        LayoutInflater inflater = LayoutInflater.from(context);
        View newView = inflater.inflate(layout, mViewFlipper, false);

        //restore state to new view
        inState.restore(newView);
        mViewFlipper.addView(newView);

        //анимация при смене экрана
        if (mViewFlipper.getChildCount() == 2) {
            mViewFlipper.showNext();
            mViewFlipper.removeView(mViewFlipper.getChildAt(0));
        }
    }
}
