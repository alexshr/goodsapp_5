package com.skb.goodsapp.ui.screens.product_detail;

import android.os.Bundle;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.ProductRealm;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.AbstractScreen;
import com.skb.goodsapp.flow.Screen;
import com.skb.goodsapp.mvp.models.RootModel;
import com.skb.goodsapp.mvp.presenters.RootPresenter;
import com.skb.goodsapp.ui.activities.RootActivity;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;
import javax.inject.Scope;

import dagger.Provides;
import mortar.MortarScope;
import mortar.ViewPresenter;

@Screen(R.layout.screen_product_detail)
public class ProductDetailScreen extends AbstractScreen {

    private String mProductId;

    public ProductDetailScreen() {
    }

    public ProductDetailScreen(String productId) {
        mProductId = productId;
    }

    @Override
    public Object[] getModules() {
        return new Object[]{new Module()};
    }

    @Override
    public Class getComponentClass() {
        return Component.class;
    }

    @Override
    public String getScopeName() {
        return getScopeName(mProductId);
    }


    public String getProductId() {
        return mProductId;
    }

    //region ============================== DI ==============================

    @dagger.Module
    public class Module {
        @Provides
        @ProductDetailScope
        ProductDetailPresenter provideProductDetailPresenter() {
            return new ProductDetailPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
    @ProductDetailScope
    public interface Component {
        void inject(ProductDetailPresenter presenter);
        void inject(ProductDetailView view);
        ProductDetailPresenter getProductDetailPresenter();
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface ProductDetailScope {
    }

    //endregion

    //region ============================== Presenter ==============================


    public class ProductDetailPresenter extends ViewPresenter<ProductDetailView> {

        @Inject
        RootPresenter mRootPresenter;

        private int mViewPagerPosition;
        private ProductRealm mProduct;

        private RootModel mRootModel;


        @Override
        protected void onEnterScope(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
            mRootModel = mRootPresenter.getRootModel();
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            mProduct = getProduct();
            setupNavigation();
            getView().initView(mProductId, mViewPagerPosition);
        }


        public void setViewPagerPosition(int viewPagerPosition) {
            mViewPagerPosition = viewPagerPosition;
        }


        private void setupNavigation() {
            mRootPresenter.createNavigationBuilder()
                    .setTitle(mProduct.getProductName())
                    .setTabs(getView().mViewPager)
                    .build();
        }

        public ProductRealm getProduct() {
            return mRootModel.getProductById(mProductId);
        }

        public RootPresenter getRootPresenter() {
            return mRootPresenter;
        }
    }
}
