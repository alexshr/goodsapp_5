package com.skb.goodsapp.data.storage.network;

public class UserLoginReq extends UserReq {

    private String mPassword;

    public UserLoginReq(String email, String password) {
        super(email);
        mPassword = password;
    }

    public String getPassword() {
        return mPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserLoginReq)) return false;

        UserLoginReq that = (UserLoginReq) o;

        return getPassword().equals(that.getPassword()) && getEmail().equals(that.getEmail());

    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserLoginReq{");
        sb.append("mEmail='").append(getEmail()).append('\'');
        sb.append("mPassword='").append(mPassword).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
