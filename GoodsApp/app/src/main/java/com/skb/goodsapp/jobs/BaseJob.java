package com.skb.goodsapp.jobs;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.skb.goodsapp.AppConfig;
import com.skb.goodsapp.data.managers.DataManager;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.di.AppComponent;

import javax.inject.Inject;

import static com.skb.goodsapp.ConstantManager.LOG_TAG;

abstract public class BaseJob extends Job {

    @Inject
    transient DataManager mDataManager;

    public MessageDto mSuccessMes;
    public MessageDto mCancelMes;

    protected BaseJob(Params params) {
        super(params);
    }


    @Override
    public void onAdded() {
        Log.d(LOG_TAG, getClass().getSimpleName() + " onAdded started");
    }

    @Override
    public void onRun() throws Throwable {
        Log.d(LOG_TAG, getClass().getSimpleName() + " onRun started");
    }

    @Override
    protected void onCancel(int i, @Nullable Throwable throwable) {
        //задача завершена
        Log.e(LOG_TAG, getClass().getSimpleName() + " onCancel i=" + i + " error=" + throwable);
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        //ошибка при выполнении задачи, политика повторений разрешается здесь
        Log.e(LOG_TAG, getClass().getSimpleName() + " Send Message shouldReRunOnThrowable: " + runCount + " " + maxRunCount + " s ");
        return RetryConstraint.createExponentialBackoff(runCount, AppConfig.RETRY_INITIAL_DELAY);
    }


    //место для переопределения maх кол-во повторений
    @Override
    protected int getRetryLimit() {
        return super.getRetryLimit();
    }

    public void inject(AppComponent component) {
        component.inject(this);
    }

    //для выдачи результата через callback
    public MessageDto getCancelMes() {
        return mCancelMes;
    }

    public MessageDto getSuccessMes() {
        return mSuccessMes;
    }

    public void setCancelMes(MessageDto cancelMes) {
        mCancelMes = cancelMes;
    }

    public void setSuccessMes(MessageDto successMes) {
        mSuccessMes = successMes;
    }
}
