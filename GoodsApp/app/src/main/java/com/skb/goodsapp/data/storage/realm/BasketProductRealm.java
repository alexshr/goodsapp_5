package com.skb.goodsapp.data.storage.realm;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

import static com.skb.goodsapp.ConstantManager.STATUS_EXIST_LOCAL;

public class BasketProductRealm extends RealmObject {

    @PrimaryKey
    private String productId;
    private ProductRealm product;
    private int count;
    private int status = STATUS_EXIST_LOCAL;


    public static BasketProductRealm getInstance(ProductRealm product, int count, int status) {
        BasketProductRealm basketProduct = new BasketProductRealm();
        basketProduct.setProductId(product.getId());
        basketProduct.setProduct(product);
        basketProduct.setCount(count);
        basketProduct.setStatus(status);
        return basketProduct;
    }

    public static BasketProductRealm getLocalInstance(ProductRealm product, int count) {
        BasketProductRealm basketProduct = new BasketProductRealm();
        basketProduct.setProductId(product.getId());
        basketProduct.setProduct(product);
        basketProduct.setCount(count);
        return basketProduct;
    }

    public static BasketProductRealm getByPrimaryKey(Realm realm, String productId) {
        return realm.where(BasketProductRealm.class).equalTo("productId", productId).findFirst();
    }


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setProduct(ProductRealm product) {
        this.product = product;
    }

    public ProductRealm getProduct() {
        return product;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void incrementCount() {
        count++;
    }

    public void decrementCount() {
        if (count > 1) {
            count--;
        }
    }
}
