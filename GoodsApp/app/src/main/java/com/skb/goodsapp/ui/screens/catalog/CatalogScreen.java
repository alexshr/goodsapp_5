package com.skb.goodsapp.ui.screens.catalog;

import android.os.Bundle;
import android.util.Log;

import com.skb.goodsapp.ConstantManager;
import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.realm.BasketProductRealm;
import com.skb.goodsapp.data.storage.realm.ProductRealm;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.AbstractScreen;
import com.skb.goodsapp.flow.Screen;
import com.skb.goodsapp.mvp.models.CatalogModel;
import com.skb.goodsapp.mvp.presenters.RootPresenter;
import com.skb.goodsapp.ui.activities.RootActivity;
import com.skb.goodsapp.ui.screens.login.LoginScreen;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;
import javax.inject.Scope;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import mortar.ViewPresenter;

@Screen(R.layout.screen_catalog)
public class CatalogScreen extends AbstractScreen {
    @Override
    public Object[] getModules() {
        return new Object[]{new Module()};
    }

    @Override
    public Class getComponentClass() {
        return Component.class;
    }

    //region ============================= DI =============================
    @dagger.Module
    public class Module {
        @Provides
        @CatalogScope
        CatalogModel provideCatalogModel() {
            return new CatalogModel();
        }

        @Provides
        @CatalogScope
        CatalogPresenter provideCatalogPresenter() {
            return new CatalogPresenter();
        }

        /*
        @Provides
        @CatalogScope
        CatalogMenu provideCatalogMenu(CatalogPresenter catalogPresenter) {
            return new CatalogMenu(catalogPresenter.mCatalogModel.getBasketProducts());
        }
        */
    }

    @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
    @CatalogScope
    public interface Component {
        void inject(CatalogPresenter catalogPresenter);

        void inject(CatalogRealmAdapter adapter);

        void inject(CatalogView view);

        CatalogPresenter getCatalogPresenter();

        CatalogModel getCatalogModel();
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface CatalogScope {
    }
    //endregion

    public static final String LOG_TAG = ConstantManager.LOG_TAG + CatalogScreen.class.getName();

    //region ============================= Presenter =============================
    public class CatalogPresenter extends ViewPresenter<CatalogView> {

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        CatalogModel mCatalogModel;

        private CatalogRealmAdapter mCatalogAdapter;

        //отложенное добавление в корзину (после авторизации)
        private BasketProductRealm mPendingBasketProduct;

        private int mViewPagerPosition;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            setupNavigation();

            mCatalogAdapter = new CatalogRealmAdapter(getView().getContext());
            getView().showCatalog(mCatalogAdapter, mViewPagerPosition);

            mCatalogModel.forceCatalogSynchronization();

            //до того пытались добавить без авторизации, а теперь вернулись из авторизации - добавляем
            if (mPendingBasketProduct != null) {
                addProductToBasket(mPendingBasketProduct);
            }
        }

        @Override
        public void dropView(CatalogView view) {
            super.dropView(view);
        }


        public ProductRealm getCurrentProduct() {
            return mCatalogAdapter.getProductByPosition(getView().getCurrentPosition());
        }

        public void addProductToBasket(int count) {
            addProductToBasket(BasketProductRealm.getLocalInstance(getCurrentProduct(), count));
        }

        public void addProductToBasket(BasketProductRealm basketProduct) {
            Log.d(LOG_TAG, "addProductToBasket " + basketProduct);
            if (isUserAuth()) {
                mCatalogModel.updateBasketFromUser(basketProduct);
                mRootPresenter.showMessage(new MessageDto(R.string.product_added, basketProduct.getProduct().getProductName(), basketProduct.getCount()));
                mPendingBasketProduct = null;
            } else {
                if (mPendingBasketProduct == null) {
                    mPendingBasketProduct = basketProduct;

                    mRootPresenter.showMessage(new MessageDto(R.string.auth_for_buying));
                    Log.d(LOG_TAG, "addProductToBasket before Flow.get(getView()).set(new LoginScreen())");
                    Flow.get(getView()).set(new LoginScreen());
                } else {
                    mPendingBasketProduct = null;
                }
            }
        }

        public boolean isUserAuth() {
            return mCatalogModel.isUserAuth();
        }

        public RootPresenter getRootPresenter() {
            return mRootPresenter;
        }

        public void setViewPagerPosition(int viewPagerPosition) {
            mViewPagerPosition = viewPagerPosition;
        }

        private void setupNavigation() {
            mRootPresenter.createNavigationBuilder()
                    .setTitle(R.string.title_catalog)
                    .build();
        }
    }
}
