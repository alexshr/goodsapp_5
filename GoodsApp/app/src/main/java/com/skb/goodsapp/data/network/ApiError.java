package com.skb.goodsapp.data.network;

public class ApiError extends Exception {
    private int statusCode;
    private String message;

    public ApiError() {
    }

    /*
        public ApiError(int statusCode, String message) {
            this.statusCode=statusCode;
            this.message=message;
        }
    */
    @Override
    public String getMessage() {
        return getUserMessage();
    }

    private String getUserMessage() {
        if (statusCode == 403) {
            return "Illegal login or password " + statusCode;
        }
        if (statusCode >= 400 && statusCode < 500) {
            return "Client error " + statusCode;
        } else if (statusCode >= 500 && statusCode < 600) {
            return "Server error " + statusCode;
        } else {
            return "Unknown network error, try later";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApiError apiError = (ApiError) o;

        if (statusCode != apiError.statusCode) return false;
        return message != null ? message.equals(apiError.message) : apiError.message == null;
    }
}
