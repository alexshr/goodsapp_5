package com.skb.goodsapp.data.storage.dto;

public class UserInfoDto {
    private String name;
    private String phone;



    public UserInfoDto(String name, String phone) {
        this.name = name;
        this.phone = phone;

    }

    public static UserInfoDto getEmptyInstance() {
        return new UserInfoDto("", "");
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public boolean isEmpty(){
        return name.trim().isEmpty()&&phone.trim().isEmpty();
    }

    public void setName(String name) {
        name = name;
    }


}
