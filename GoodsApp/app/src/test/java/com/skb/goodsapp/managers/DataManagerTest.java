package com.skb.goodsapp.managers;

import android.content.Context;

import com.birbit.android.jobqueue.JobManager;
import com.skb.goodsapp.MockDispatcher;
import com.skb.goodsapp.MyApplication;
import com.skb.goodsapp.R;
import com.skb.goodsapp.TestSchedulerRule;
import com.skb.goodsapp.data.managers.DataManager;
import com.skb.goodsapp.data.managers.PreferencesManager;
import com.skb.goodsapp.data.managers.RealmManager;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.network.ProductCommentReq;
import com.skb.goodsapp.data.storage.network.ProductCommentRes;
import com.skb.goodsapp.data.storage.network.ProductRes;
import com.skb.goodsapp.data.storage.network.UserAddressRes;
import com.skb.goodsapp.data.storage.network.UserLoginReq;
import com.skb.goodsapp.data.storage.network.UserReq;
import com.skb.goodsapp.data.storage.network.UserRes;
import com.skb.goodsapp.data.storage.network.UserSocReq;
import com.skb.goodsapp.di.AppComponent;
import com.skb.goodsapp.di.AppModule;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.utils.ConnectionChecker;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.TestScheduler;
import okhttp3.mockwebserver.MockWebServer;

import static com.skb.goodsapp.AppConfig.DEFAULT_LAST_MODIFIED_DATE;
import static com.skb.goodsapp.AppConfig.NO_NAME;
import static com.skb.goodsapp.AppConfig.UPDATE_DATA_INTERVAL;
import static com.skb.goodsapp.MockDispatcher.RequestPath.avatar;
import static com.skb.goodsapp.MockDispatcher.RequestPath.comments;
import static com.skb.goodsapp.MockDispatcher.RequestPath.login;
import static com.skb.goodsapp.MockDispatcher.RequestPath.products;
import static com.skb.goodsapp.MockDispatcher.RequestPath.socialLogin;
import static com.skb.goodsapp.MockDispatcher.SERVER_LAST_MODIFIED_DATE;
import static com.skb.goodsapp.PredicateUtils.check;
import static junit.framework.Assert.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.verifyPrivate;

@PowerMockIgnore("javax.net.ssl.*")//http://stackoverflow.com/a/36167978/2886841

@RunWith(PowerMockRunner.class)
@PrepareForTest({MyApplication.class, DataManager.class, ProductCommentReq.class})
//Prepare classes for static mocking
public class DataManagerTest {

    @Rule
    public TestSchedulerRule testSchedulerRule = new TestSchedulerRule();

    @Mock
    Context mockContext;
    @Mock
    PreferencesManager mockPrefManager;
    //@Mock
    //RestService mockRestService;
    @Mock
    RealmManager mockRealmManager;
    @Mock
    ConnectionChecker mockConnectionChecker;
    @Mock
    JobManager mockJobManager;

    private MockWebServer mMockWebServer;
    private MockDispatcher mMockDispatcher;
    //private RestService mRestService;
    private DataManager mDataManager;
    //private Retrofit mRetrofit;

    //private RestService mRestService;

    private TestScheduler mTestScheduler;

    //region ================= before and after =================
    @Before
    public void setUp() throws Exception {
        prepareStubs();
        mDataManager = new DataManager();
    }


    private void prepareStubs() {
        mMockWebServer = new MockWebServer();
        mMockDispatcher = new MockDispatcher();
        mMockWebServer.setDispatcher(mMockDispatcher);

        PowerMockito.mockStatic(MyApplication.class);
        mTestScheduler = testSchedulerRule.getTestScheduler();

        given(MyApplication.getDaggerComponent()).willReturn(DaggerService.buildComponent(AppComponent.class, new AppModule(mockContext) {
            @Override
            protected String provideBaseUrl() {
                return mMockWebServer.url("").toString();
            }

            /*
                        @Override
                        protected RestService provideRestService(Retrofit retrofit) {
                            mRestService = super.provideRestService(retrofit);
                            return mRestService;
                        }
            */
            @Override
            protected PreferencesManager providePreferencesManager() {
                return mockPrefManager;
            }

            @Override
            protected ConnectionChecker provideConnectionChecker() {
                return mockConnectionChecker;
            }

            @Override
            protected RealmManager provideRealmManager() {
                return mockRealmManager;
            }

            @Override
            protected JobManager provideJobManager() {
                return mock(JobManager.class);
            }
        }));
    }


    @After
    public void tearDown() throws Exception {
        mMockWebServer.shutdown();

    }

    //endregion


    //region ================= tests =================
    //region ================= tests getNetworkProductsObservable =================
    @Test
    public void getNetworkProductsObservable_retry_success() throws Exception {
        //given

        given(mockPrefManager.getProductLastModified()).willReturn(DEFAULT_LAST_MODIFIED_DATE);
        mMockDispatcher.setRequestPlan(products, 400, 400, 400, 200);

        //MockDispatcher dispatcher = new MockDispatcher(REQUEST_GET_PRODUCTS, 400, 400, 400, 200);
        //mMockWebServer.setDispatcher(dispatcher);

        DataManager dataManager = spy(mDataManager);

        //when
        //call private method
        TestObserver<List<ProductRes>> testObserver = Whitebox.<Observable<List<ProductRes>>>invokeMethod(dataManager, "getNetworkProductsObservable").test();

        mTestScheduler.advanceTimeBy(10, TimeUnit.SECONDS);
        mTestScheduler.triggerActions();
        //then
        testObserver
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(check(
                        l -> assertThat(l).hasSize(9)
                ))
                .assertComplete();

        mMockDispatcher.assertRequestsHistory();

        verifyPrivate(dataManager, times(0)).invoke("sendMessage", new MessageDto(R.string.update_error));
        verifyPrivate(dataManager, times(1)).invoke("setCatalogLoading", false);
        then(mockPrefManager).should(times(1)).getProductLastModified();
        then(mockPrefManager).should(times(1)).saveProductLastModified(SERVER_LAST_MODIFIED_DATE);
    }

    @Test
    public void getNetworkProductsObservable_retry_error() throws Exception {
        given(mockPrefManager.getProductLastModified()).willReturn(DEFAULT_LAST_MODIFIED_DATE);

        mMockDispatcher.setRequestPlan(products, 400, 400, 400, 400);

        DataManager dataManager = spy(mDataManager);

        //call private method
        TestObserver<List<ProductRes>> testObserver = Whitebox.<Observable<List<ProductRes>>>invokeMethod(dataManager, "getNetworkProductsObservable").test();


        mTestScheduler.advanceTimeBy(10, TimeUnit.SECONDS);
        mTestScheduler.triggerActions();
        testObserver
                .assertNoErrors()
                .assertNoValues()
                .assertComplete();

        mMockDispatcher.assertRequestsHistory();

        verifyPrivate(dataManager, times(1)).invoke("sendMessage", new MessageDto(R.string.update_error));
        verifyPrivate(dataManager, times(1)).invoke("setCatalogLoading", false);
        then(mockPrefManager).should(times(1)).getProductLastModified();
        then(mockPrefManager).should(times(0)).saveProductLastModified(SERVER_LAST_MODIFIED_DATE);
    }


    @Test
    public void getNetworkProductsObservable_empty() throws Exception {
        //given
        given(mockPrefManager.getProductLastModified()).willReturn(DEFAULT_LAST_MODIFIED_DATE);

        mMockDispatcher.setRequestPlan(products, 304);

        DataManager dataManager = spy(mDataManager);

        //when
        //call private method
        TestObserver<List<ProductRes>> testObserver = Whitebox.<Observable<List<ProductRes>>>invokeMethod(dataManager, "getNetworkProductsObservable").test();


        mTestScheduler.advanceTimeBy(10, TimeUnit.SECONDS);
        mTestScheduler.triggerActions();
        //then
        testObserver
                .assertNoErrors()
                .assertValueCount(0)
                .assertComplete();

        mMockDispatcher.assertRequestsHistory();

        verifyPrivate(dataManager, times(0)).invoke("sendMessage", new MessageDto(R.string.update_error));
        verifyPrivate(dataManager, times(0)).invoke("setCatalogLoading", true);
        then(mockPrefManager).should(times(1)).getProductLastModified();
        then(mockPrefManager).should(times(0)).saveProductLastModified(SERVER_LAST_MODIFIED_DATE);
    }

    @Test
    public void loadProductsFromServer_success() throws Exception {
        //given

        given(mockPrefManager.getProductLastModified()).willReturn(DEFAULT_LAST_MODIFIED_DATE);

        mMockDispatcher.setRequestPlan(products, 400, 400, 200);

        DataManager dataManager = spy(mDataManager);

        //when
        //call private method
        Whitebox.<Observable<List<ProductRes>>>invokeMethod(dataManager, "loadProductsFromServer");


        mTestScheduler.advanceTimeBy(10, TimeUnit.SECONDS);
        mTestScheduler.triggerActions();
        //then

        mMockDispatcher.assertRequestsHistory();

        verifyPrivate(dataManager, times(0)).invoke("sendMessage", new MessageDto(R.string.update_error));
        verifyPrivate(dataManager, times(1)).invoke("setCatalogLoading", false);
        then(mockPrefManager).should(times(1)).getProductLastModified();
        then(mockPrefManager).should(times(1)).saveProductLastModified(SERVER_LAST_MODIFIED_DATE);

        verifyPrivate(dataManager, times(1)).invoke("setCatalogLoading", false);
        then(mockRealmManager).should(times(1)).synchronizeWithServerData(argThat(productList -> productList.size() == 9), any());
    }

    @Test
    public void loadProductsFromServer_error() throws Exception {
        //given

        given(mockPrefManager.getProductLastModified()).willReturn(DEFAULT_LAST_MODIFIED_DATE);

        mMockDispatcher.setRequestPlan(products, 400, 400, 400, 400);

        DataManager dataManager = spy(mDataManager);

        //when
        //call private method
        Whitebox.<Observable<List<ProductRes>>>invokeMethod(dataManager, "loadProductsFromServer");

        mTestScheduler.advanceTimeBy(10, TimeUnit.SECONDS);
        mTestScheduler.triggerActions();
        //then

        mMockDispatcher.assertRequestsHistory();


        verifyPrivate(dataManager, times(1)).invoke("sendMessage", new MessageDto(R.string.update_error));
        verifyPrivate(dataManager, times(1)).invoke("setCatalogLoading", false);
        then(mockPrefManager).should(times(1)).getProductLastModified();
        then(mockPrefManager).should(times(0)).saveProductLastModified(SERVER_LAST_MODIFIED_DATE);

        then(mockRealmManager).should(times(0)).synchronizeWithServerData(any(), any());
    }

    @Test
    public void loadProductsFromServer_empty() throws Exception {
        //given

        given(mockPrefManager.getProductLastModified()).willReturn(DEFAULT_LAST_MODIFIED_DATE);

        mMockDispatcher.setRequestPlan(products, 304);

        DataManager dataManager = spy(mDataManager);

        //when
        //call private method
        Whitebox.<Observable<List<ProductRes>>>invokeMethod(dataManager, "loadProductsFromServer");


        mTestScheduler.advanceTimeBy(10, TimeUnit.SECONDS);
        mTestScheduler.triggerActions();
        //then

        mMockDispatcher.assertRequestsHistory();

        verifyPrivate(dataManager, times(0)).invoke("sendMessage", new MessageDto(R.string.update_error));
        then(mockPrefManager).should(times(1)).getProductLastModified();
        then(mockPrefManager).should(times(0)).saveProductLastModified(SERVER_LAST_MODIFIED_DATE);

        verifyPrivate(dataManager, times(1)).invoke("setCatalogLoading", false);
        then(mockRealmManager).should(times(0)).synchronizeWithServerData(any(), any());
        then(mockPrefManager).should(times(1)).getProductLastModified();
        then(mockPrefManager).should(times(0)).saveProductLastModified(SERVER_LAST_MODIFIED_DATE);
        verifyPrivate(dataManager, times(0)).invoke("sendMessage", new MessageDto(R.string.update_error));
    }

    @Test
    public void enableCatalogSynchronization() throws Exception {

        DataManager dataManager = spy(mDataManager);
        doReturn(false).when(dataManager, "isSynchronizationStarted");


        //region ================= step 1 =================
        //в начале нет сети
        given(mockConnectionChecker.isConnected()).willReturn(false);
        //поэтому запросов в сеть не должно быть

        //запускаем синхронизацию
        dataManager.enableCatalogSynchronization();
        //перемещаемся во времени
        mTestScheduler.advanceTimeBy(10, TimeUnit.SECONDS);
        //проверяем, что запросов не было
        mMockDispatcher.assertRequestsHistory();

        verifyPrivate(dataManager, times(0)).invoke("sendMessage", any());
        verifyPrivate(dataManager, times(0)).invoke("sendError", new MessageDto("R.string.no_internet"));
        verifyPrivate(dataManager, times(0)).invoke("setCatalogLoading", anyBoolean());

        then(mockPrefManager).should(times(0)).saveProductLastModified(any());
        then(mockRealmManager).should(times(0)).synchronizeWithServerData(any(), any());
        //endregion


        //region ================= step 2 =================
        //включаем сеть
        given(mockConnectionChecker.isConnected()).willReturn(true);
        //сервер выдает ошибки
        mMockDispatcher.setRequestPlan(products, 400, 400, 400, 4000);

        //перемещаемся во времени
        mTestScheduler.advanceTimeBy(UPDATE_DATA_INTERVAL, TimeUnit.SECONDS);

        //проверяем на соответствие ожиданиям
        mMockDispatcher.assertRequestsHistory();

        verifyPrivate(dataManager, times(1)).invoke("sendMessage", new MessageDto(R.string.update_error));
        verifyPrivate(dataManager, times(1)).invoke("setCatalogLoading", true);
        verifyPrivate(dataManager, times(1)).invoke("setCatalogLoading", false);

        then(mockPrefManager).should(times(0)).saveProductLastModified(any());
        then(mockRealmManager).should(times(0)).synchronizeWithServerData(any(), any());
        //endregion

        //region ================= step 3 =================
        //планируем ответы
        mMockDispatcher.setRequestPlan(products, 400, 400, 400, 200);

        //перемещаемся во времени
        mTestScheduler.advanceTimeBy(UPDATE_DATA_INTERVAL, TimeUnit.SECONDS);

        //проверяем на соответствие ожиданиям
        mMockDispatcher.assertRequestsHistory();

        verifyPrivate(dataManager, times(1 + 0)).invoke("sendMessage", new MessageDto(R.string.update_error));
        verifyPrivate(dataManager, times(1 + 1)).invoke("setCatalogLoading", true);
        verifyPrivate(dataManager, times(1 + 1)).invoke("setCatalogLoading", false);

        then(mockPrefManager).should(times(0 + 1)).saveProductLastModified(SERVER_LAST_MODIFIED_DATE);
        then(mockRealmManager).should(times(0 + 1)).synchronizeWithServerData(argThat(productList -> productList.size() == 9), any());
        //endregion

        //region ================= step 4 =================
        //планируем ответы
        mMockDispatcher.setRequestPlan(products, 200);

        //перемещаемся во времени
        mTestScheduler.advanceTimeBy(UPDATE_DATA_INTERVAL, TimeUnit.SECONDS);

        //проверяем запросы на соответствие ожиданиям
        mMockDispatcher.assertRequestsHistory();

        verifyPrivate(dataManager, times(1 + 0 + 0)).invoke("sendMessage", new MessageDto(R.string.update_error));
        verifyPrivate(dataManager, times(1 + 1 + 1)).invoke("setCatalogLoading", true);
        verifyPrivate(dataManager, times(1 + 1 + 1)).invoke("setCatalogLoading", false);

        then(mockPrefManager).should(times(0 + 1 + 1)).saveProductLastModified(SERVER_LAST_MODIFIED_DATE);
        then(mockRealmManager).should(times(0 + 1 + 1)).synchronizeWithServerData(argThat(productList -> productList.size() == 9), any());

        verifyPrivate(dataManager, times(0)).invoke("sendError", new MessageDto(R.string.sync_stopped_error));
        //endregion
    }

    @Test
    public void forceCatalogSynchronization_force() throws Exception {
//given
        DataManager dataManager = spy(mDataManager);
        doReturn(false).when(dataManager, "isSynchronizationStarted");

        given(mockConnectionChecker.isConnected()).willReturn(true);

        mMockDispatcher.setRequestPlan(products, 200, 200);

//when
        dataManager.enableCatalogSynchronization();
        mTestScheduler.advanceTimeBy(1, TimeUnit.SECONDS);
        dataManager.forceCatalogSynchronization();

        mTestScheduler.advanceTimeBy(10, TimeUnit.SECONDS);
//then
        then(mockRealmManager).should(times(2)).synchronizeWithServerData(any(), any());
        mMockDispatcher.assertRequestsHistory();
    }

    @Test
    public void forceCatalogSynchronization_no_force() throws Exception {
//given
        DataManager dataManager = spy(mDataManager);
        doReturn(false).when(dataManager, "isSynchronizationStarted");

        given(mockConnectionChecker.isConnected()).willReturn(true);

        mMockDispatcher.setRequestPlan(products, 200);

//when
        dataManager.enableCatalogSynchronization();
        mTestScheduler.advanceTimeBy(1, TimeUnit.SECONDS);
//given
        Whitebox.setInternalState(dataManager, "mIsCatalogLoading", true);
//when
        dataManager.forceCatalogSynchronization();
        mTestScheduler.advanceTimeBy(10, TimeUnit.SECONDS);
//then
        then(mockRealmManager).should(times(1)).synchronizeWithServerData(any(), any());
        mMockDispatcher.assertRequestsHistory();
    }

    @Test
    public void postComment_201_auth() throws Exception {
        postComment(201, true);
    }

    @Test
    public void postComment_201_no_auth() throws Exception {
        postComment(201, false);
    }

    @Test
    public void postComment_error_auth() throws Exception {
        postComment(400, true);
    }

    @Test
    public void uploadFile_200() throws Exception {
        uploadFile(200);
    }

    @Test
    public void uploadFile_error() throws Exception {
        uploadFile(400);
    }

    @Test
    public void loginToApi_connected_200() throws Exception {
        loginToApi(200, false);
    }

    @Test
    public void loginToApi_connected_social_200() throws Exception {
        loginToApi(200, true);
    }

    @Test
    public void loginToApi_connected_400() throws Exception {
        loginToApi(400, false);
    }

    @Test
    public void loginToApi_connected_403() throws Exception {
        loginToApi(403, false);
    }

    @Test
    public void loginToApi_not_connected() throws Exception {
        loginToApi(null, false);
    }


    private void uploadFile(int respCode) throws Exception {
//given
        File file = new File("test");
        file.createNewFile();

        mMockDispatcher.setRequestPlan(avatar, respCode);

        DataManager dataManager = spy(mDataManager);
//when
        String fileUrl = null;
        Exception exception = null;
        try {
            fileUrl = Whitebox.<String>invokeMethod(dataManager, "uploadFile", file);
        } catch (Exception e) {
            exception = e;
        } finally {
            file.delete();
        }

//then
        mMockDispatcher.assertRequestsHistory();
        if (respCode == 200) {
            assertEquals("http://skba1.mgbeta.ru/assets/avatars/1491972833904.png", fileUrl);
        } else {
            assertThat(exception).isInstanceOf(Exception.class)
                    .hasMessageContaining("Client error " + respCode);
        }
    }


    private void postComment(int responseCode, boolean isUserAuth) throws Exception {
//given
        String id = "testId";
        String productId = "testProductId";
        String text = "test comment";
        float rating = 2;
        String userName = isUserAuth ? NO_NAME : "any name";
        String phone = "911";
        String avatar = "http://skill-branch.ru/img/app/avatar-1.png";
        Integer remoteId = 911;
        String date = "2017-02-28T08:17:30.277Z";
        boolean isActive = true;


        ProductCommentReq commentExpectedReq = new ProductCommentReq(rating, text, NO_NAME, avatar);
        ProductCommentRes commentExpectedRes = new ProductCommentRes(id, remoteId, null, rating, text, NO_NAME, isActive, date, productId);

        DataManager dataManager = spy(mDataManager);

        mMockDispatcher.setRequestPlan(comments, responseCode);

        given(dataManager.isUserAuth()).willReturn(isUserAuth);
        given(mockPrefManager.getUserName()).willReturn(userName);
        given(mockPrefManager.getPhone()).willReturn(phone);
        given(mockPrefManager.getAvatarUrl()).willReturn(avatar);


//when
        ProductCommentRes commentRes = null;
        Exception exception = null;
        try {
            commentRes = dataManager.postComment(productId, rating, text);
        } catch (Exception e) {
            exception = e;
        }

//then
        mMockDispatcher.assertRequestsHistory();

        if (responseCode == 201) {
            verifyPrivate(dataManager, times(1)).invoke("postComment", productId, commentExpectedReq);
            assertEquals(commentExpectedRes, commentRes);
        } else {
            assertThat(exception).isInstanceOf(Exception.class)
                    .hasMessageContaining("Client error " + responseCode);
        }
    }


    private void loginToApi(Integer respCode, boolean isSocRequest) throws Exception {
//given
        boolean isConnected = respCode != null;

        UserReq userReq = isSocRequest ? new UserSocReq("test@test.ru", "firstName", "lastName", "http://skba1.mgbeta.ru/assets/avatars/1491972833904.png", "999999999") : new UserLoginReq("test@test.ru", "testPsw");

        /*
        String email = "test@test.ru";
        String password = "test";
        UserLoginReq loginExpectedReq = new UserLoginReq(email, password);
*/
        List<UserAddressRes> addressList = new ArrayList<>();
        addressList.add(new UserAddressRes("58711631a242690011b1b26d", "Работа", "Автостроителей", "24", "1", 1, "comment1"));
        addressList.add(new UserAddressRes("58711631a242690011b1b26d", "Дом", "Автостроителей", "25", "2", 2, "comment2"));
        UserRes expectedRes = new UserRes("58711631a242690011b1b26d", "Вася", "http://asac.ru/sajdwsj.jpg", "wegfvw;edcnw'lkedm93847983yuhefoij32lkml'kjvj30fewoidvn", "89179711111", addressList);


        DataManager dataManager = spy(mDataManager);

        if (isConnected)
            mMockDispatcher.setRequestPlan(isSocRequest ? socialLogin : login, respCode);

        given(mockConnectionChecker.isConnected()).willReturn(isConnected);

        //when
        dataManager.loginToApi(userReq);

        //mTestScheduler.advanceTimeBy(1, TimeUnit.SECONDS);
        mTestScheduler.triggerActions();

        //then
        mMockDispatcher.assertRequestsHistory();

        int count = isConnected ? 1 : 0;
        verifyPrivate(dataManager, times(count)).invoke("loginToApiObservable", userReq);

        //сохранение результата
        count = isConnected && respCode == 200 ? 1 : 0;
        then(mockPrefManager).should(times(count)).saveProfileInfo(expectedRes);
        verifyPrivate(dataManager, times(count)).invoke("refreshAuthState", true);

        //отправление ошибок
        count = isConnected ? 0 : 1;
        verifyPrivate(dataManager, times(count)).invoke("sendError", new MessageDto(R.string.no_internet));

        count = isConnected && respCode == 403 ? 1 : 0;
        verifyPrivate(dataManager, times(count)).invoke("sendError", new MessageDto(R.string.login_error));

        count = isConnected && respCode == 400 ? 1 : 0;
        verifyPrivate(dataManager, times(count)).invoke("sendError", new MessageDto("Client error " + respCode));

    }
}