package com.skb.goodsapp.mortar;

import android.content.Context;
import android.util.Log;

import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.AbstractScreen;
import com.skb.goodsapp.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mortar.MortarScope;

import static com.skb.goodsapp.ConstantManager.LOG_TAG;
import static mortar.MortarScope.getScope;


public class ScreenScoper {
    public static final String SCOPE_ROOT_NAME = "Root";

    public static List<String> getParentScopePath(AbstractScreen screen) {

        List<String> pathList = new ArrayList<>();
        while (screen.getParentScopeScreen() != null) {
            screen = screen.getParentScopeScreen();
            pathList.add(screen.getScopeName());
        }
        Collections.reverse(pathList);
        Log.d(LOG_TAG, "ScreenScoper getParentScopePath pathList=" + Utils.listToString(pathList));
        return pathList;
    }

    /**
     * обеспечиваем (находим или создаем новый) mortar scope для экрана, не зная при этом parent context
     *
     * @param screen
     * @param baseContext
     * @return
     */
    public static MortarScope provideScopeFromBaseContext(AbstractScreen screen, Context baseContext) {
        MortarScope baseScope = getScope(baseContext);

        List<String> parentScopePath = getParentScopePath(screen);
        MortarScope parentScope = searchParentScope(parentScopePath, baseScope);

        return provideScopeFromParent(screen, parentScope);
    }

    private static MortarScope searchParentScope(List<String> parentScopePath, MortarScope baseScope) {
        MortarScope scope = baseScope;
        for (String scopeName : parentScopePath) {
            scope = scope.findChild(scopeName);
            if (scope == null) {
                Log.e(LOG_TAG, "ERROR searchParentScope can't find scope " + scopeName);
            } else {
                Log.d(LOG_TAG, "searchParentScope scope finded: " + scopeName);
            }
        }
        return scope;
    }

    private static MortarScope provideScopeFromParent(AbstractScreen screen, MortarScope parentScope) {
        Log.d(LOG_TAG, "ScreenScoper provideScopeFromParent screen: " + screen.getScopeName() + " parentScope: " + parentScope.getName());
        MortarScope childScope = parentScope.findChild(screen.getScopeName());
        if (childScope == null) {
            Object parentComponent = parentScope.getService(DaggerService.SERVICE_NAME);
            Object childComponent = DaggerService.buildComponent(screen.getComponentClass(), parentComponent, screen.getModules());
            childScope = parentScope.buildChild()
                    .withService(DaggerService.SERVICE_NAME, childComponent)
                    .build(screen.getScopeName());
            Log.d(LOG_TAG, "ScreenScoper provideScopeFromParent scope was created: " + childScope.getPath());
        } else {
            Log.d(LOG_TAG, "ScreenScoper provideScopeFromParent scope already exists: " + childScope.getPath());
        }

        return childScope;
    }


    /**
     * обеспечиваем (находим или создаем новый) mortar scope для экрана, зная при этом parent context
     * берем parent component from parent scope
     * @param screen
     * @param context
     * @return
     */
    public static MortarScope provideScopeFromParentContext(AbstractScreen screen, Context context) {
        MortarScope parentScope = MortarScope.getScope(context);
        return provideScopeFromParent(screen, parentScope);
    }

    /**
     * пересоздаем mortar scope для экрана, зная при этом parent context
     *
     * @param screen
     * @param context
     * @return
     */
    public static MortarScope recreateScopeFromParentContext(AbstractScreen screen, Context context) {
        MortarScope parentScope = getScope(context);
        MortarScope childScope = parentScope.findChild(screen.getScopeName());
        if (childScope != null) {
            childScope.destroy();
        }
        return provideScopeFromParent(screen, parentScope);
    }

}
