package com.skb.goodsapp.data.network;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.skb.goodsapp.AppConfig.BASE_LOGIN_URL;

/**
 * url replacement
 */
public final class HostSelectionInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        if (!isTestMode()) {
            HttpUrl url = request.url();

            String path = url.pathSegments().get(2);
            if (path.equals("login") || path.equals("socialLogin")) {

                HttpUrl baseUrl = HttpUrl.parse(BASE_LOGIN_URL);

                HttpUrl newUrl = new HttpUrl.Builder()
                        .scheme(baseUrl.scheme())
                        .host(baseUrl.host())
                        .addPathSegment(path)
                        .build();
                request = request.newBuilder()
                        .url(newUrl)
                        .build();
            }
        }
        return chain.proceed(request);
    }

    private static boolean isTestMode() {
        boolean result;
        try {
            Class.forName("com.skb.goodsapp.TestIndicator");

            result = true;
        } catch (final Exception e) {
            result = false;
        }
        return result;
    }
/*
    @Override public okhttp3.Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        String host = this.host;
        if (host != null) {
            HttpUrl newUrl = request.url().newBuilder()
                    .host(host)
                    .build();
            request = request.newBuilder()
                    .url(newUrl)
                    .build();
        }
        return chain.proceed(request);
    }

    public static void main(String[] args) throws Exception {
        HostSelectionInterceptor interceptor = new HostSelectionInterceptor();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        Request request = new Request.Builder()
                .url("http://www.coca-cola.com/robots.txt")
                .build();

        okhttp3.Call call1 = okHttpClient.newCall(request);
        okhttp3.Response response1 = call1.execute();
        System.out.println("RESPONSE FROM: " + response1.request().url());
        System.out.println(response1.body().string());

        interceptor.setHost("www.pepsi.com");

        okhttp3.Call call2 = okHttpClient.newCall(request);
        okhttp3.Response response2 = call2.execute();
        System.out.println("RESPONSE FROM: " + response2.request().url());
        System.out.println(response2.body().string());
    }
    */
}
