package com.skb.goodsapp.mvp.views;

public interface IBackPressedView {

    /*
    return true - мы провели обработку и дальше блокируем
    false - мы не до конца обработали и разрешаем продолжать
    * */
    boolean onViewBackPressed();

}
