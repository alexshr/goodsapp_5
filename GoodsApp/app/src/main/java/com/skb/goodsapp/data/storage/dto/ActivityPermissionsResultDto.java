package com.skb.goodsapp.data.storage.dto;

import android.content.pm.PackageManager;

public class ActivityPermissionsResultDto {
    private int requestCode;
    private String[] permissions;
    private int[] grantResult;

    public ActivityPermissionsResultDto(int requestCode, String[] permissions, int[] grantResult) {
        this.requestCode = requestCode;
        this.permissions = permissions;
        this.grantResult = grantResult;
    }

    public boolean isAllGranted() {
        boolean res = true;
        for (int grant : grantResult) {
            res = res && (grant == PackageManager.PERMISSION_GRANTED);
            if (res == false) {
                break;
            }
        }
        return res;
    }

    public int getRequestCode() {
        return requestCode;
    }

}
