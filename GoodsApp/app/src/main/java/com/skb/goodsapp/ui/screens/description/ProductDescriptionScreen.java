package com.skb.goodsapp.ui.screens.description;

import android.os.Bundle;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.realm.BasketProductRealm;
import com.skb.goodsapp.data.storage.realm.FavoriteProductRealm;
import com.skb.goodsapp.data.storage.realm.ProductRealm;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.AbstractScreen;
import com.skb.goodsapp.flow.Screen;
import com.skb.goodsapp.mvp.models.ProductDescriptionModel;
import com.skb.goodsapp.ui.screens.product_detail.ProductDetailScreen;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;
import javax.inject.Scope;

import dagger.Provides;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import mortar.MortarScope;
import mortar.ViewPresenter;

@Screen(R.layout.screen_product_description)
public class ProductDescriptionScreen extends AbstractScreen {

    private String mProductId;

    public ProductDescriptionScreen(String productId) {
        mProductId = productId;
        setParentScopeScreen(new ProductDetailScreen());
    }

    @Override
    public Object[] getModules() {
        return new Object[]{new Module()};
    }

    @Override
    public Class getComponentClass() {
        return Component.class;
    }

    //region ============================== DI ==============================

    @dagger.Component(dependencies = ProductDetailScreen.Component.class, modules = Module.class)
    @ProductDescriptionScope
    public interface Component {
        void inject(ProductDescriptionPresenter presenter);

        void inject(ProductDescriptionView view);
    }

    @dagger.Module
    public class Module {

        @Provides
        @ProductDescriptionScope
        ProductDescriptionModel provideDescriptionModel() {
            return new ProductDescriptionModel();
        }

        @Provides
        @ProductDescriptionScope
        ProductDescriptionPresenter provideDescriptionPresenter() {
            return new ProductDescriptionPresenter();
        }
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface ProductDescriptionScope {
    }
    //endregion

    //region ============================== Presenter ==============================

    public class ProductDescriptionPresenter extends ViewPresenter<ProductDescriptionView> {

        @Inject
        ProductDescriptionModel mProductDescriptionModel;
        @Inject
        ProductDetailScreen.ProductDetailPresenter mProductDetailPresenter;

        ProductRealm mProduct;
        RealmResults<BasketProductRealm> mBasket;
        RealmResults<FavoriteProductRealm> mFavorites;

        private RealmChangeListener<ProductRealm> mProductRealmListener = product -> showProductArea();

        private RealmChangeListener<RealmResults<BasketProductRealm>> mBasketRealmListener = basket -> showBasketArea();

        private RealmChangeListener<RealmResults<FavoriteProductRealm>> mFavoritesRealmListener = basket -> showFavoriteArea();

        @Override
        protected void onEnterScope(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            mProduct = mProductDescriptionModel.getProductById(mProductId);
            mBasket = mProductDescriptionModel.getBasket();
            mFavorites = mProductDescriptionModel.getFavorites();

            showProductArea();
            showBasketArea();
            showFavoriteArea();

            mProduct.addChangeListener(mProductRealmListener);
            mBasket.addChangeListener(mBasketRealmListener);
            mFavorites.addChangeListener(mFavoritesRealmListener);

            //show(mProduct);//перввй показ - нужен при синхронных запросах
        }

        @Override
        public void dropView(ProductDescriptionView view) {
            super.dropView(view);
            mProduct.removeAllChangeListeners();
            mBasket.removeAllChangeListeners();
            mFavorites.removeAllChangeListeners();
        }


        private void showProductArea() {
            if (getView() != null) getView().showProductArea(mProduct);
        }

        private void showFavoriteArea() {
            boolean isFavorite = mProductDescriptionModel.isFavorite(mProductId);
            if (getView() != null) getView().showFavoriteArea(isFavorite);
        }

        private void showBasketArea() {
            int count = !mProductDescriptionModel.isInBasket(mProductId) ? 1 : mProductDescriptionModel.getBasketProductById(mProductId).getCount();
            if (getView() != null) getView().showBasketArea(count);
        }

        public void incrementProductCount() {
            int count = getView().getProductCount();
            getView().mProductCountView.setText(++count + "");
        }


        public void decrementProductCount() {
            int count = getView().getProductCount();
            count = count > 1 ? --count : count;
            getView().mProductCountView.setText(count + "");
        }

        public void onFavoriteChange(boolean isChecked) {
            if (mProductDescriptionModel.isUserAuth()) {
                mProductDescriptionModel.setFavorite(mProductId, isChecked);
            } else {
                showFavoriteArea();
                mProductDetailPresenter.getRootPresenter().showError(new MessageDto(R.string.not_authorised));

            }

        }

        public void addOrUpdateBasketProduct() {
            if (mProductDescriptionModel.isUserAuth()) {
                mProductDescriptionModel.updateBasketFromUser(mProductId, getView().getProductCount());
                mProductDetailPresenter.getRootPresenter().showMessage(new MessageDto(R.string.product_added_success));
            } else {
                mProductDetailPresenter.getRootPresenter().showError(new MessageDto(R.string.not_authorised));
            }
        }
    }
    //endregion
}
