package com.skb.goodsapp.ui.screens.login;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.skb.goodsapp.R;
import com.skb.goodsapp.data.rx.SimpleDisposableObserver;
import com.skb.goodsapp.data.storage.dto.ActivityResultDto;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.network.UserLoginReq;
import com.skb.goodsapp.data.storage.network.UserSocReq;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.AbstractScreen;
import com.skb.goodsapp.flow.Screen;
import com.skb.goodsapp.mvp.models.LoginModel;
import com.skb.goodsapp.mvp.presenters.RootPresenter;
import com.skb.goodsapp.ui.activities.RootActivity;
import com.skb.goodsapp.ui.screens.catalog.CatalogScreen;
import com.skb.goodsapp.utils.Utils;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.AccountService;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKRequest.VKRequestListener;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;
import javax.inject.Scope;

import dagger.Provides;
import flow.Direction;
import flow.Flow;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import mortar.MortarScope;
import mortar.ViewPresenter;

import static com.skb.goodsapp.AppConfig.EMAIL_PATTERN;
import static com.skb.goodsapp.AppConfig.PASSWORD_PATTERN;
import static com.skb.goodsapp.ConstantManager.LOG_TAG;

//import org.greenrobot.eventbus.EventBus;

@Screen(R.layout.screen_login)
public class LoginScreen extends AbstractScreen {
    @Override
    public Object[] getModules() {
        return new Object[]{new Module()};
    }

    @Override
    public Class getComponentClass() {
        return Component.class;
    }

    //region ============================= DI =============================
    @dagger.Module
    public static class Module {
        @Provides
        @LoginScope
        protected LoginModel provideLoginModel() {
            return new LoginModel();
        }

        @Provides
        @LoginScope
        protected LoginPresenter provideLoginPresenter() {
            return new LoginPresenter();
        }

        @Provides
        @LoginScope
        protected LoginManager provideFbLoginManager() {
            return LoginManager.getInstance();
        }

        @Provides
        @LoginScope
        protected CallbackManager provideFbCallbackManager() {
            return CallbackManager.Factory.create();
        }


        @Provides
        @LoginScope
        protected TwitterAuthClient provideTwitterAuthClient() {
            return new TwitterAuthClient();
        }


    }

    @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
    @LoginScope
    public interface Component {
        void inject(LoginPresenter catalogPresenter);

        void inject(LoginView view);
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface LoginScope {
    }
    //endregion

    //region ============================= Presenter =============================
    public static class LoginPresenter extends ViewPresenter<LoginView> {

        private static String LOG = LoginPresenter.class.getSimpleName();

        //ждем ответа сервера
        //private boolean mIsLoading;
        private Boolean mIsLoginPanelOpened;

        private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

        private boolean mIsActivityResultWaiting;

        //for VK
        private String mVkEmail;

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        LoginModel mLoginModel;

        @Inject
        LoginManager mFbLoginManager;
        @Inject
        CallbackManager mFbCallbackManager;

        @Inject
        TwitterAuthClient mTwitterAuthClient;

        //UserSocReq mUserFbSocReq = new UserSocReq();
        UserSocReq mUserSocReq;


        //region ================= fb callbacks =================
        GraphRequest.GraphJSONObjectCallback fbRequestEmailCallback = (object, response) -> {
            mUserSocReq.setEmail(object.optString("email"));
            mLoginModel.login(mUserSocReq);
        };

        FacebookCallback<LoginResult> fbLoginCallback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Profile profile = Profile.getCurrentProfile();
                mUserSocReq = new UserSocReq();
                mUserSocReq.setFirstName(profile.getFirstName());
                mUserSocReq.setLastName(profile.getLastName());
                mUserSocReq.setAvatarUrl(profile.getProfilePictureUri(200, 200).toString());

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        fbRequestEmailCallback);

                Bundle parameters = new Bundle();
                parameters.putString("fields", "email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                mRootPresenter.showMessage(new MessageDto(R.string.login_cancelled));
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(LOG_TAG, " ", error);
                mRootPresenter.showError(new MessageDto(error.getLocalizedMessage()));
            }
        };

        //endregion

        //region ================= vk callbacks =================
        private VKCallback<VKAccessToken> vkLoginCallback = new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken token) {
                mUserSocReq = new UserSocReq();
                mUserSocReq.setEmail(token.email);
                VKRequest request = VKApi.users().get();
                request.attempts = 3;
                request.executeWithListener(vkUserListener);
            }

            @Override
            public void onError(VKError error) {
                Log.e(LOG_TAG, "vkLoginCallback onError " + error.errorMessage + " " + error.errorReason, error.httpError);
                MessageDto mes = error.errorMessage != null ? new MessageDto(error.errorMessage) : new MessageDto(R.string.login_cancelled);
                mRootPresenter.showError(mes);
            }
        };

        private VKRequestListener vkUserListener = new VKRequestListener() {

            @Override
            public void onComplete(VKResponse response) {
                VKList<VKApiUserFull> list = (VKList<VKApiUserFull>) response.parsedModel;
                VKApiUserFull user = list.get(0);
                mUserSocReq.setFirstName(user.first_name);
                mUserSocReq.setLastName(user.last_name);
                mUserSocReq.setAvatarUrl(user.photo_200);
                mUserSocReq.setPhone(user.mobile_phone);

                mLoginModel.login(mUserSocReq);
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                Log.d(LOG, "vkUserListener attemptFailed " + attemptNumber + " from " + totalAttempts);
            }

            @Override
            public void onError(VKError error) {
                mRootPresenter.showError(new MessageDto(error.errorMessage));
                Log.e(LOG_TAG, "vkUserListener onError " + error.errorMessage + " " + error.errorReason, error.httpError);
            }

        };
        //endregion


        //region ================= twitter callbacks =================
        private Callback<User> mTwUserCallback = new Callback<User>() {
            @Override
            public void success(Result<User> result) {
                User user = result.data;
                mUserSocReq = new UserSocReq();
                mUserSocReq.setFirstName(user.name);
                mUserSocReq.setAvatarUrl(user.profileImageUrl);
                mUserSocReq = new UserSocReq(user.email, user.name, "", user.profileImageUrl, "");
                TwitterAuthClient authClient = new TwitterAuthClient();
                authClient.requestEmail(TwitterCore.getInstance().getSessionManager().getActiveSession(), mTwEmailCallback);
            }

            @Override
            public void failure(TwitterException e) {
                Log.e(LOG_TAG, "", e);
                mRootPresenter.showError(new MessageDto(e.getMessage()));
            }
        };

        private Callback<String> mTwEmailCallback = new Callback<String>() {
            @Override
            public void success(Result<String> result) {
                mUserSocReq.setEmail(result.data);
                mLoginModel.login(mUserSocReq);
            }

            @Override
            public void failure(TwitterException e) {
                Log.e(LOG_TAG, "", e);
                mRootPresenter.showError(new MessageDto(e.getLocalizedMessage()));
            }
        };

        private Callback<TwitterSession> mTwLoginCallback = new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                AccountService ac = TwitterCore.getInstance().getApiClient(result.data).getAccountService();
                ac.verifyCredentials(true, true, true).enqueue(mTwUserCallback);
            }

            @Override
            public void failure(TwitterException e) {
                mRootPresenter.showError(new MessageDto(e.getLocalizedMessage()));
            }
        };
        //endregion


        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);

            mFbLoginManager.registerCallback(mFbCallbackManager, fbLoginCallback);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            setupNavigation();
            if (getView() != null) {
                if (mIsLoginPanelOpened == null) {
                    closeLoginPanel();//иначе анимация открытия не работает
                }
                new Handler().postDelayed(() -> openLoginPanelWithAnimation(), 100);
            }

            //подписка авторизацию
            Log.d(LOG_TAG, "LoginPresenter onLoad before subscribe mLoginModel.getAuthStateSubject()= " + mLoginModel.getAuthStateSubject() + " " + this + " getView()=" + getView());

            mCompositeDisposable.add(mLoginModel.getAuthStateSubject()
                    .doOnNext(res -> Log.d(LOG_TAG, "doOnNext res=" + res))
                    .filter(isAuth -> isAuth)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new SimpleDisposableObserver<Boolean>() {
                        @Override
                        public void onNext(Boolean res) {
                            onAuthSuccessWithAnim();
                        }
                    }));

            //.subscribe(isSuccess -> onAuthSuccessWithAnim()));

            //подписка на activity result
            if (mIsActivityResultWaiting) {
                mCompositeDisposable.add(getVkActivityResultObservable()
                        .subscribeWith(new SimpleDisposableObserver<>()));

                mCompositeDisposable.add(getFbActivityResultObservable()
                        .subscribeWith(new SimpleDisposableObserver<>()));

                mCompositeDisposable.add(getTwActivityResultObservable()
                        .subscribeWith(new SimpleDisposableObserver<>()));
            }

            //Log.d(LOG_TAG, "LoginPresenter onLoad finished " + this + " getView()=" + getView());
        }


        @Override
        public void dropView(LoginView view) {
            //mRootPresenter.setupProgress(false);
            //EventBus.getDefault().unregister(this);
            super.dropView(view);
            if (getView() == null) {
                mCompositeDisposable.clear();
                Log.d(LOG_TAG, "LoginPresenter dropView finished " + this + " view to drop: " + view + " current view=" + getView());
            }

        }


        @Override
        protected void onSave(Bundle outState) {
            super.onSave(outState);
            Log.d(LOG_TAG, "LoginPresenter onSave  " + this);
        }

        public void clickOnLogin() {
            if (getView() != null) {
                boolean isValid = validateEmail() && validatePassword();
                getView().mLoginBtn.setEnabled(isValid);
                if (isValid) {
                    String email = getView().getUserEmail();
                    String password = getView().getUserPassword();
                    mLoginModel.login(new UserLoginReq(email, password));
                    //mIsLoading = true;
                    //mRootPresenter.setupProgress(mIsLoading);
                }
            }
        }

        public Observable<ActivityResultDto> getVkActivityResultObservable() {
            return mRootPresenter.getActivityResultSubject()
                    .filter(res -> VKSdk.onActivityResult(res.getRequestCode(), res.getResultCode(), res.getIntent(), vkLoginCallback))
                    .doOnNext(res -> mIsActivityResultWaiting = false)
                    .doOnError(res -> mIsActivityResultWaiting = false);//real canceled case
        }

        public Observable<ActivityResultDto> getFbActivityResultObservable() {
            return mRootPresenter.getActivityResultSubject()
                    .filter(res -> mFbCallbackManager.onActivityResult(res.getRequestCode(), res.getResultCode(), res.getIntent()))
                    .doOnNext(res -> mIsActivityResultWaiting = false)
                    .doOnError(res -> mIsActivityResultWaiting = false);//as a precaution
        }

        public Observable<ActivityResultDto> getTwActivityResultObservable() {
            return mRootPresenter.getActivityResultSubject()
                    .filter(res -> res.getRequestCode() == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE)
                    .doOnNext(res -> mIsActivityResultWaiting = false)
                    .doOnNext(res -> mTwitterAuthClient.onActivityResult(res.getRequestCode(), res.getResultCode(), res.getIntent()));
            //.filter(res -> res.getResultCode() == Activity.RESULT_CANCELED)
            //.doOnNext(res -> mRootPresenter.showMessage(new MessageDto(R.string.login_cancelled)));
        }


        public void clickOnFb() {
            getView().startFbAnimation();
            mRootPresenter.fbLogin(mFbLoginManager);//т.к. метод getView() задуман во flow как protected
            mIsActivityResultWaiting = true;
        }


        public void clickOnVk() {
            getView().startVkAnimation();
            mRootPresenter.vkSdkLogin();
            mIsActivityResultWaiting = true;
        }


        public void clickOnTwitter() {
            getView().startTwAnimation();
            mRootPresenter.twLogin(mTwitterAuthClient, mTwLoginCallback);
            mIsActivityResultWaiting = true;
        }


        public boolean validateEmail() {
            boolean isValid = false;
            if (getView() != null) {
                String email = getView().getUserEmail();

                if (!Utils.validate(email, EMAIL_PATTERN)) {
                    getView().setEmailError(R.string.err_msg_email);

                } else {
                    getView().setEmailError(null);
                    isValid = true;
                }
            }
            return isValid;
        }


        public boolean validatePassword() {
            boolean isValid = false;
            if (getView() != null) {
                String password = getView().getUserPassword();

                if (!Utils.validate(password, PASSWORD_PATTERN)) {
                    getView().setPasswordError(R.string.err_msg_password);
                } else {
                    getView().setPasswordError(null);
                    isValid = true;
                }
            }
            return isValid;
        }

        public RootPresenter getRootPresenter() {
            return mRootPresenter;
        }

        private void setupNavigation() {
            RootPresenter.NavigationBuilder nb = mRootPresenter.createNavigationBuilder();
            nb.setTitle(R.string.title_enter);
            nb.build();
        }

        private void openLoginPanelWithAnimation() {
            TransitionManager.getDefaultTransition().addListener(new Transition.TransitionListenerAdapter() {
                @Override
                public void onTransitionStart(Transition transition) {
                    getView().showMockLoginData();//иначе исчезает
                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    transition.removeListener(this);
                }
            });
            TransitionManager.beginDelayedTransition(getView());

            openLoginPanel();
        }

        private void openLoginPanel() {
            mIsLoginPanelOpened = true;
            getView().onLoginPanelStateChanged(mIsLoginPanelOpened);
        }

        private void onAuthSuccessWithAnim() {
            Log.d(LOG_TAG, "onAuthSuccessWithAnim()");
            TransitionManager.getDefaultTransition().addListener(new Transition.TransitionListenerAdapter() {
                @Override
                public void onTransitionEnd(Transition transition) {
                    transition.removeListener(this);
                    onAuthSuccess();
                }
            });
            TransitionManager.beginDelayedTransition(getView());
            closeLoginPanel();
        }

        private void closeLoginPanel() {
            mIsLoginPanelOpened = false;
            getView().onLoginPanelStateChanged(false);
        }


        private void onAuthSuccess() {

            Log.d(LOG_TAG, "onAuthSuccess() started");
            if (!Flow.get(getView()).goBack()) {
                Log.d(LOG_TAG, "onAuthSuccess() before replaceHistory");
                Flow.get(getView()).replaceHistory(new CatalogScreen(), Direction.REPLACE);
                Log.d(LOG_TAG, "onAuthSuccess() after replaceHistory");
            }
        }

    }
    //endregion
}
