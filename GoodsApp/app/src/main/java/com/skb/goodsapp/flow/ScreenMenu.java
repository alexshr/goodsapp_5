package com.skb.goodsapp.flow;

import android.content.Context;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.skb.goodsapp.MyApplication;
import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.BasketProductRealm;
import com.skb.goodsapp.mvp.models.RootModel;
import com.skb.goodsapp.ui.screens.basket.BasketFooter;
import com.skb.goodsapp.ui.screens.basket.BasketScreen;

import javax.inject.Inject;

import flow.Flow;
import io.realm.RealmResults;

public class ScreenMenu {

    @Inject
    protected RootModel mRootModel;

    private Context mContext;


    private boolean mIsToHideBasket;//возможность спрятать корзину для авторизованного пользователя

    RealmResults<BasketProductRealm> mBasketProducts;
    TextView mCountView;
    MenuItem mBasketItem;


    public ScreenMenu(Context context) {
        MyApplication.getDaggerComponent().inject(this);
        mContext = context;
    }

    public void onCreateMenu(Menu menu, MenuInflater inflater) {
        if (!mIsToHideBasket) {
            inflater.inflate(R.menu.screen_catalog, menu);
            mBasketItem = menu.findItem(R.id.basket);

            MenuItemCompat.setActionView(mBasketItem, R.layout.icon_counting_bucket);
            mBasketItem.getActionView().setOnClickListener(v -> Flow.get(mContext).set(new BasketScreen()));

            View basketView = menu.findItem(R.id.basket).getActionView();
            mCountView = (TextView) basketView.findViewById(R.id.basket_count);

            mBasketProducts = mRootModel.getBasket();
            mBasketProducts.addChangeListener(prod -> {
                mCountView.setText(new BasketFooter(mBasketProducts).getProductCount() + "");
            });

            mBasketItem.setVisible(mRootModel.isUserAuth());

            //видимость корзины в зависимости от состояния
            mRootModel.getAuthStateSubject().subscribe(mBasketItem::setVisible);
        }
    }

    public void onOptionsItemSelected(MenuItem item) {

    }

    public void setToHideBasket(boolean toHideBasket) {
        mIsToHideBasket = toHideBasket;
    }
}
