package com.skb.goodsapp;


public interface ConstantManager {

    String LOG_TAG = "DEV " + BuildConfig.FLAVOR;
    String LOG_UPLOAD = LOG_TAG + " UPLOAD";

    String TEST_TOKEN = "TEST_TOKEN";

    String MES_AUTH_FINISHED = "MES_AUTH_FINISHED";


    //String TAG_LOG_UTILS = "Goods-App-Dream-Team";

    //запас готовых страниц во view pager
    int OFFSCREEN_PAGES = 1;


    int REQUEST_CAMERA_CODE = 1;
    int REQUEST_GALLERY_CODE = 2;

    int REQUEST_PERMISSION_FOR_GALLERY_CODE = 101;
    int REQUEST_PERMISSION_FOR_CAMERA_CODE = 102;


    String KEY_USER_PHONE ="KEY_USER_PHONE" ;
    String KEY_USER_NAME ="KEY_USER_NAME" ;
    int RETRY_COUNT = 3;

    String JOB_GROUP_COMMENTS = "COMMENTS";
    String JOB_GROUP_UPLOADS = "UPLOADS";

    //realm object states
    public static final int STATUS_CONFIRMED = 0;//подтверждено сервером
    public static final int STATUS_EXIST_LOCAL = 1;//меняем статус на STATUS_CONFIRMED при подтверждении сервера, удаляем если job в итоге не сработал
    public static final int STATUS_DELETE_LOCAL = 2;//в корзине - не показываем, удаляем при любом исходе job
}

