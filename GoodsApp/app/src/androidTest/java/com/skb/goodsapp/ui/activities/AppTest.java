package com.skb.goodsapp.ui.activities;

import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.jakewharton.espresso.OkHttp3IdlingResource;
import com.skb.goodsapp.EspressoExecutor;
import com.skb.goodsapp.MockDispatcher;
import com.skb.goodsapp.R;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.mockwebserver.MockWebServer;

import static com.schibsted.spain.barista.BaristaAssertions.assertDisplayed;
import static com.skb.goodsapp.EspressoUtils.assertCatalogProductCounter;
import static com.skb.goodsapp.EspressoUtils.assertIllegalEmailHintDisplayed;
import static com.skb.goodsapp.EspressoUtils.assertIllegalEmailHintNotExists;
import static com.skb.goodsapp.EspressoUtils.assertIllegalPasswordHintDisplayed;
import static com.skb.goodsapp.EspressoUtils.assertIllegalPasswordHintNotExists;
import static com.skb.goodsapp.EspressoUtils.assertLoginBtnDisabled;
import static com.skb.goodsapp.EspressoUtils.assertNavigationMenuItemsDisplayed;
import static com.skb.goodsapp.EspressoUtils.assertNavigationMenuItemsNotExists;
import static com.skb.goodsapp.EspressoUtils.clickCatalogAddToBasketBtn;
import static com.skb.goodsapp.EspressoUtils.clickCatalogFavoriteCheckbox;
import static com.skb.goodsapp.EspressoUtils.clickCatalogProductPlusBtn;
import static com.skb.goodsapp.EspressoUtils.clickLoginBtn;
import static com.skb.goodsapp.EspressoUtils.clickOnNavigationMenuItem;
import static com.skb.goodsapp.EspressoUtils.closeDrawer;
import static com.skb.goodsapp.EspressoUtils.deleteAllFromRealm;
import static com.skb.goodsapp.EspressoUtils.getToolbarBasketCount;
import static com.skb.goodsapp.EspressoUtils.matchActionMenuItemDisplayed;
import static com.skb.goodsapp.EspressoUtils.matchActionMenuItemNotDisplayedOrNotExists;
import static com.skb.goodsapp.EspressoUtils.matchToolbarTitle;
import static com.skb.goodsapp.EspressoUtils.openDrawer;
import static com.skb.goodsapp.EspressoUtils.resetPreferences;
import static com.skb.goodsapp.EspressoUtils.writeToLoginText;
import static com.skb.goodsapp.EspressoUtils.writeToPasswordText;
import static com.skb.goodsapp.MockDispatcher.RequestPath.login;
import static com.skb.goodsapp.MockDispatcher.RequestPath.products;
import static com.skb.goodsapp.MockMyApplication.setupBaseUrl;


/**
 * Using my EspressoScheduler:
 * Idling resources and animations turn off is not required!
 */
@RunWith(AndroidJUnit4.class)
public class AppTest {
    public static String LOG = AppTest.class.getSimpleName();

    EspressoExecutor mScheduler = new EspressoExecutor<ViewInteraction>(10000, 200);


    String mValidEmail = "test@test.ru";
    String mValidPassword = "test111111111";

    String mIllegalEmail = "test";
    String mIllegalPassword = "test";

    private MockWebServer mMockWebServer = new MockWebServer();
    private MockDispatcher mMockDispatcher;

    @Rule
    public ActivityTestRule<SplashActivity> mActivityTestRule = new ActivityTestRule<>(SplashActivity.class, false, false);
    private OkHttp3IdlingResource mOkHttp3IdlingResource;


    @Before
    public void setUp() throws Exception {
        mMockDispatcher = new MockDispatcher();

        mMockWebServer.setDispatcher(mMockDispatcher);
        mMockWebServer.start();

        setupBaseUrl(mMockWebServer.url("") + "");

        mMockDispatcher.setRequestPlan(products, 200, 200);
        mMockDispatcher.setRequestPlan(login, 200, 200);

        deleteAllFromRealm();

        resetPreferences();

        mActivityTestRule.launchActivity(null);
    }


    /**
     * check login, logout, navigator,
     * catalog(with and without auth),
     * adding to basket (through login)
     */
    @Test
    public void start_test() {

/*   it's sample of  OkHttp3IdlingResource use:
    1)long delays
    2)my approach with EspressoScheduler is mutch better!

        OkHttpClient okHttpClient = mActivityTestRule.getActivity().getDataManager().getOkHttpClient();
        mOkHttp3IdlingResource = OkHttp3IdlingResource.create(
                "okHttp", okHttpClient);
        Espresso.registerIdlingResources(mOkHttp3IdlingResource);
*/
        checkUnauthorizedCatalog();
        checkUnauthorizedNavigator();

        goToLoginFormByNavigation();
        checkLoginForm();
        submitLoginForm();

        checkAuthorizedCatalog();
        checkAuthorizedNavigator();

        Assert.assertEquals(0, getToolbarBasketCount());

        exitByNavigator();

        checkUnauthorizedCatalog();
        checkUnauthorizedNavigator();

        int beforeCount = 1;
        assertCatalogProductCounter(beforeCount + "");

        int addCount = 3;
        clickCatalogProductPlusBtn(addCount);

        clickCatalogAddToBasketBtn();

        submitLoginForm();

        mScheduler.call(() -> Assert.assertEquals(beforeCount + addCount, getToolbarBasketCount()));
    }

    private void checkUnauthorizedCatalog() {
        //check toolbar title
        mScheduler.call(() -> matchToolbarTitle(R.string.title_catalog));

        //check basket menu item
        matchActionMenuItemNotDisplayedOrNotExists(R.id.basket);

        //click favorite btn on visible product
        mScheduler.call(() -> clickCatalogFavoriteCheckbox());

        //check "not authorised" snack bar message
        mScheduler.call(() -> assertDisplayed(R.string.not_authorised));
    }

    private void checkAuthorizedCatalog() {
        //check toolbar title
        mScheduler.call(() -> matchToolbarTitle(R.string.title_catalog));

        //check basket menu item
        matchActionMenuItemDisplayed(R.id.basket);
    }

    private void checkUnauthorizedNavigator() {
        //open navigation drawer
        openDrawer();

        //check drawer menu items
        assertNavigationMenuItemsDisplayed(R.string.title_enter, R.string.title_catalog);
        assertNavigationMenuItemsNotExists(R.string.title_exit, R.string.title_favorites, R.string.title_basket);

        closeDrawer();
    }

    private void checkAuthorizedNavigator() {
        //open navigation drawer
        openDrawer();

        //check drawer menu items
        assertNavigationMenuItemsNotExists(R.string.title_enter);
        assertNavigationMenuItemsDisplayed(R.string.title_exit, R.string.title_catalog, R.string.title_favorites, R.string.title_basket);

        closeDrawer();
    }

    private void submitLoginForm() {
        //fill valid email and password
        writeToLoginText(mValidEmail);
        writeToPasswordText(mValidPassword);

        //click login btn
        clickLoginBtn();

        //check toolbar title
        mScheduler.call(() -> matchToolbarTitle(R.string.title_catalog));
    }

    private void goToLoginFormByNavigation() {
        //open navigation drawer
        openDrawer();

        //click "enterAccount" drawer menu item
        clickOnNavigationMenuItem(R.string.title_enter);

        //check toolbar title
        matchToolbarTitle(R.string.title_enter);
    }

    private void checkLoginForm() {

        //fill illegal email and password
        writeToLoginText(mIllegalEmail);
        writeToPasswordText(mIllegalPassword);

        //check error hints are displayed
        assertIllegalEmailHintDisplayed();
        assertIllegalPasswordHintDisplayed();

        assertLoginBtnDisabled();

        //fill valid email and password
        writeToLoginText(mValidEmail);
        writeToPasswordText(mValidPassword);

        //check error hints are not displayed
        assertIllegalEmailHintNotExists();
        assertIllegalPasswordHintNotExists();
    }

    private void exitByNavigator() {
        openDrawer();

        //click "enterAccount" drawer menu item
        clickOnNavigationMenuItem(R.string.title_exit);
    }

}
