package com.skb.goodsapp.mvp.models;

import com.birbit.android.jobqueue.JobManager;
import com.skb.goodsapp.MyApplication;
import com.skb.goodsapp.data.managers.DataManager;
import com.skb.goodsapp.data.managers.RealmManager;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.dto.UserInfoDto;
import com.skb.goodsapp.data.storage.network.UserReq;
import com.skb.goodsapp.data.storage.realm.BasketProductRealm;
import com.skb.goodsapp.data.storage.realm.FavoriteProductRealm;
import com.skb.goodsapp.data.storage.realm.ProductRealm;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.realm.RealmResults;


public class RootModel {

    @Inject
    DataManager mDataManager;
    @Inject
    JobManager mJobManager;

    RealmManager mRealmManager;

    public RootModel() {
        MyApplication.getDaggerComponent().inject(this);
        mRealmManager = mDataManager.getRealmManager();
    }

    public DataManager getDataManager() {
        return mDataManager;
    }

    public RealmManager getRealmManager() {
        return mRealmManager;
    }


    public JobManager getJobManager() {
        return mJobManager;
    }

    public RealmResults<BasketProductRealm> getBasket() {
        return mRealmManager.getBasket();
    }

    public boolean isUserAuth() {
        return mDataManager.isUserAuth();
    }

    public ProductRealm getProductById(String id) {
        return mRealmManager.getProductById(id);
    }

    public RealmResults<ProductRealm> getProductList() {
        return mRealmManager.getProductList();
    }

    public void setFavorite(String productId, boolean isFavorite) {
        if (isFavorite) {
            addFavoriteFromUser(productId);
        } else {
            deleteFavorite(productId);
        }
    }

    public void addFavoriteFromUser(String productId) {
        mRealmManager.addFavoriteFromUser(productId);
    }

    public void deleteFavorite(String projectId) {
        mRealmManager.deleteFavorite(projectId);
    }

    public void updateBasketFromUser(String productId, int count) {
        mRealmManager.updateBasketFromUser(productId, count);
    }

    public void updateBasketFromUser(BasketProductRealm basketProduct) {
        mRealmManager.updateBasketFromUser(basketProduct.getProductId(), basketProduct.getCount());
    }

    public int getBasketCount(String productId) {
        return mRealmManager.getBasketCount(productId);
    }

    public BasketProductRealm getBasketProductById(String productId) {
        return mRealmManager.getBasketProductById(productId);
    }

    public RealmResults<FavoriteProductRealm> getFavorites() {
        return mRealmManager.getFavorites();
    }


    public boolean isFavorite(String productId) {
        return mRealmManager.isFavorite(productId);
    }

    public boolean isInBasket(String productId) {
        return mRealmManager.isInBasket(productId);
    }


    public void logout() {
        mDataManager.logout();
    }


    public void login(UserReq userReq) {
        mDataManager.loginToApi(userReq);
    }

    public PublishSubject<Boolean> getAuthStateSubject() {
        return mDataManager.getAuthStateSubject();
    }

    public void saveProfileInfo(UserInfoDto userInfo) {
        mDataManager.saveUserInfo(userInfo);
    }

    public PublishSubject<UserInfoDto> getUserInfoSubject() {
        return mDataManager.getUserInfoSubject();
    }

    //аналог get метода, но через rx подписку
    public void requestUserInfo() {
        mDataManager.refreshUserInfo();
    }

    public String getAvatarUrl() {
        return mDataManager.getAvatarUrl();
    }

    public PublishSubject<String> getAvatarSubject() {
        return mDataManager.getAvatarSubject();
    }

    public void requestAvatar() {
        mDataManager.refreshAvatar();
    }

    public BehaviorSubject<Boolean> getProgressSubject() {
        return mDataManager.getProgressSubject();
    }

    public void sendError(MessageDto mes) {
        mDataManager.sendError(mes);
    }

    public Observable<MessageDto> getMessageSubject() {
        return mDataManager.getMessageSubject();
    }

    public Observable<MessageDto> getErrorSubject() {
        return mDataManager.getErrorSubject();
    }
}
