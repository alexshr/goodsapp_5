package com.skb.goodsapp.data.storage.realm;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

import static com.skb.goodsapp.ConstantManager.STATUS_EXIST_LOCAL;

public class FavoriteProductRealm extends RealmObject {

    @PrimaryKey
    private String productId;

    private ProductRealm product;
    private int status = STATUS_EXIST_LOCAL;

    public static FavoriteProductRealm getInstance(ProductRealm product, int status) {
        FavoriteProductRealm favoriteProduct = new FavoriteProductRealm();
        favoriteProduct.setProductId(product.getId());
        favoriteProduct.setProduct(product);
        favoriteProduct.setStatus(status);
        return favoriteProduct;
    }

    public static FavoriteProductRealm getLocalInstance(ProductRealm product) {
        FavoriteProductRealm favoriteProduct = new FavoriteProductRealm();
        favoriteProduct.setProductId(product.getId());
        favoriteProduct.setProduct(product);
        return favoriteProduct;
    }

    public static FavoriteProductRealm getByPrimaryKey(Realm realm, String productId) {
        return realm.where(FavoriteProductRealm.class).equalTo("productId", productId).findFirst();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setProduct(ProductRealm product) {
        this.product = product;
    }

    public ProductRealm getProduct() {
        return product;
    }
}
