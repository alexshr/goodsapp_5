package com.skb.goodsapp.data.storage.network;

import android.support.annotation.Nullable;

import com.squareup.moshi.Json;

import java.util.List;

public class UserRes {
    @Json(name = "_id")
    private String id;
    private String fullName;
    private String avatarUrl;
    private String token;
    @Nullable
    private String phone;
    @Nullable
    private List<UserAddressRes> addresses;

    public String getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getToken() {
        return token;
    }

    @Nullable
    public String getPhone() {
        return phone;
    }

    public List<UserAddressRes> getAddresses() {
        return addresses;
    }

    public UserRes(String id, String fullName, String avatarUrl, String token, String phone, List<UserAddressRes> addresses) {
        this.id = id;
        this.fullName = fullName;
        this.avatarUrl = avatarUrl;
        this.token = token;
        this.phone = phone;
        this.addresses = addresses;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRes userRes = (UserRes) o;

        if (id != null ? !id.equals(userRes.id) : userRes.id != null) return false;
        if (fullName != null ? !fullName.equals(userRes.fullName) : userRes.fullName != null)
            return false;
        if (avatarUrl != null ? !avatarUrl.equals(userRes.avatarUrl) : userRes.avatarUrl != null)
            return false;
        if (token != null ? !token.equals(userRes.token) : userRes.token != null) return false;
        if (phone != null ? !phone.equals(userRes.phone) : userRes.phone != null) return false;
        return addresses != null ? addresses.equals(userRes.addresses) : userRes.addresses == null;

    }

}
