package com.skb.goodsapp.flow;

import flow.ClassKey;

public abstract class AbstractScreen extends ClassKey  {

    //можно отдельно указать нужно ли сохранять scope пред. экрана при переходе сюда
    //по умолчанию сохраняем, если старый экран предок нового
    //можно установить отдельно, например для возврата из логирования

    private AbstractScreen mParentScopeScreen;

    public String getScopeName() {
        return getClass().getName();
    }

    public String getScopeName(String id) {
        return String.format("%s_%s", getClass().getName(), id);
    }

    public abstract Object[] getModules();

    public abstract Class getComponentClass();


    public int getLayoutId(){
        return getClass().getAnnotation(Screen.class).value();
    }

    //для получения scope tree; не используется для навигации
    public AbstractScreen getParentScopeScreen() {
        return mParentScopeScreen;
    }

    public void setParentScopeScreen(AbstractScreen parentScopeScreen) {
        mParentScopeScreen = parentScopeScreen;
    }
}
