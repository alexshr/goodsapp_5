package com.skb.goodsapp.ui.screens.account;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;

import com.skb.goodsapp.ConstantManager;
import com.skb.goodsapp.R;
import com.skb.goodsapp.data.rx.SimpleDisposableObserver;
import com.skb.goodsapp.data.storage.dto.ActivityPermissionsResultDto;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.dto.UserInfoDto;
import com.skb.goodsapp.data.storage.dto.UserSettingsDto;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.AbstractScreen;
import com.skb.goodsapp.flow.Screen;
import com.skb.goodsapp.mvp.models.AccountModel;
import com.skb.goodsapp.mvp.presenters.RootPresenter;
import com.skb.goodsapp.ui.activities.RootActivity;
import com.skb.goodsapp.ui.screens.address.AddressScreen;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;
import javax.inject.Scope;

import dagger.Provides;
import flow.Flow;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import mortar.MortarScope;
import mortar.ViewPresenter;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.skb.goodsapp.ConstantManager.KEY_USER_NAME;
import static com.skb.goodsapp.ConstantManager.KEY_USER_PHONE;
import static com.skb.goodsapp.ConstantManager.LOG_UPLOAD;
import static com.skb.goodsapp.ConstantManager.REQUEST_CAMERA_CODE;
import static com.skb.goodsapp.ConstantManager.REQUEST_GALLERY_CODE;
import static com.skb.goodsapp.ConstantManager.REQUEST_PERMISSION_FOR_CAMERA_CODE;
import static com.skb.goodsapp.ConstantManager.REQUEST_PERMISSION_FOR_GALLERY_CODE;


@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen {


    @Override
    public Object[] getModules() {
        return new Object[]{new Module()};
    }

    @Override
    public Class getComponentClass() {
        return Component.class;
    }


    //region ============================== DI ==============================

    @dagger.Module
    public class Module {

        @AccountScope
        @Provides
        AccountModel provideAccountModel() {
            return new AccountModel();
        }

        @AccountScope
        @Provides
        AccountPresenter provideAccountPresenter() {
            return new AccountPresenter();
        }

    }

    @AccountScope
    @dagger.Component(dependencies = RootActivity.Component.class, modules = Module.class)
    public interface Component {
        void inject(AccountPresenter presenter);

        void inject(AccountView view);

        RootPresenter getRootPresenter();

        AccountModel getAccountModel();
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface AccountScope {
    }

    //endregion

    //region ============================== Presenter ==============================

    public class AccountPresenter extends ViewPresenter<AccountView> {

        private boolean mIsEditMode;

        @Inject
        RootPresenter mRootPresenter;

        @Override
        protected void onSave(Bundle outState) {
            super.onSave(outState);
            if(isEditMode()) {
                outState.putString(ConstantManager.KEY_USER_PHONE, getView().mUserPhoneValidatedTextLayout.getText());
                outState.putString(KEY_USER_NAME, getView().mUserNameValidatedTextLayout.getText());
            }
        }



        @Inject
        AccountModel mAccountModel;

        private boolean mIsActivityResultWaiting;
        private boolean mIsActivityPermissionsResultWaiting;

        private CompositeDisposable mCompositeDisposable;


        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);

            setupNavigation();

            if(isEditMode()) {
                getView().mUserPhoneValidatedTextLayout.setText(savedInstanceState.getString(KEY_USER_PHONE));
                getView().mUserNameValidatedTextLayout.setText(savedInstanceState.getString(KEY_USER_NAME));
            }
            getView().initView(mAccountModel.findUserAddressListAsync());

            mCompositeDisposable = new CompositeDisposable();
            mCompositeDisposable.add(mAccountModel.getUserInfoSubject()
                    .observeOn(AndroidSchedulers.mainThread())
                    //.filter(userInfoDto -> isEditMode() || getView().getUserInfo().isEmpty())
                    .subscribeWith(new SimpleDisposableObserver<UserInfoDto>() {
                        @Override
                        public void onNext(UserInfoDto userInfoDto) {
                            getView().showUserInfo(userInfoDto);
                        }
                    }));

            mCompositeDisposable.add(mAccountModel.getAvatarSubject()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new SimpleDisposableObserver<String>() {
                        @Override
                        public void onNext(String avatarUrl) {
                            getView().showAccountAvatar(avatarUrl);
                        }
                    }));


            if (mIsActivityResultWaiting) {
                mRootPresenter.getActivityResultAvatarUrlSubject()
                        .subscribeWith(new SimpleDisposableObserver<String>() {
                            @Override
                            public void onNext(String uri) {
                                Log.d(LOG_UPLOAD, "AccountPresenter onNext avatarLocalUrl=" + uri);
                                mIsActivityResultWaiting = false;
                                mAccountModel.saveAvatarUrl(uri);//для локального вывода
                                mAccountModel.uploadUserAvatar(uri);
                                dispose();
                            }
                        });
                Log.d(LOG_UPLOAD, "AccountPresenter subscribed to mActivityResultPublishSubject");
            }

            if (mIsActivityPermissionsResultWaiting) {
                getPermissionsResultPublishSubject()
                        .subscribeWith(new SimpleDisposableObserver<ActivityPermissionsResultDto>() {
                            @Override
                            public void onNext(ActivityPermissionsResultDto result) {
                                dispose();
                                mIsActivityPermissionsResultWaiting = false;
                                if (!result.isAllGranted()) {
                                    mRootPresenter.showMessage(new MessageDto(R.string.permission_warning));
                                } else {
                                    switch (result.getRequestCode()) {
                                        case REQUEST_PERMISSION_FOR_CAMERA_CODE:
                                            goToCamera();
                                            break;
                                        case REQUEST_PERMISSION_FOR_GALLERY_CODE:
                                            goToGallery();
                                    }
                                }
                            }
                        });
            }

            mAccountModel.getUserSettingsObservable().subscribe(getView()::setUserSettings);

            mAccountModel.requestAvatar();
            mAccountModel.requestUserInfo();
        }


        private Observable<ActivityPermissionsResultDto> getPermissionsResultPublishSubject() {
            return mRootPresenter.getActivityPermissionsResultPublishSubject()
                    .filter(res ->
                            res.getRequestCode() == REQUEST_PERMISSION_FOR_CAMERA_CODE
                                    || res.getRequestCode() == REQUEST_PERMISSION_FOR_GALLERY_CODE);
        }

        @Override
        public void dropView(AccountView view) {
            super.dropView(view);
            mCompositeDisposable.dispose();
        }

        public boolean isEditMode() {
            return mIsEditMode;
        }


        public void goToGallery() {
            Intent takeGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            takeGalleryIntent.setType("image/*");
            mRootPresenter.startActivityForResult(takeGalleryIntent, REQUEST_GALLERY_CODE);
            mIsActivityResultWaiting = true;
        }

        public void chooseGallery() {
            String[] permissions = new String[]{READ_EXTERNAL_STORAGE};
            if (mRootPresenter.checkPermissions(permissions)) {
                goToGallery();
            } else {
                if (mRootPresenter.requestPermissions(permissions, REQUEST_PERMISSION_FOR_GALLERY_CODE)) {
                    mIsActivityPermissionsResultWaiting = true;
                } else {
                    mRootPresenter.showMessage(new MessageDto(R.string.permission_warning));
                }
            }
        }

        public void chooseCamera() {
            String[] permissions = new String[]{CAMERA, WRITE_EXTERNAL_STORAGE};
            if (mRootPresenter.checkPermissions(permissions)) {
                goToCamera();
            } else {
                if (mRootPresenter.requestPermissions(permissions, REQUEST_PERMISSION_FOR_CAMERA_CODE)) {
                    mIsActivityPermissionsResultWaiting = true;
                } else {
                    mRootPresenter.showMessage(new MessageDto(R.string.permission_warning));
                }
            }
        }

        private void goToCamera() {
            Uri photoFileUri = mRootPresenter.createFileForPhoto();
            if (photoFileUri != null) {
                Intent takeCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takeCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoFileUri);
                mRootPresenter.startActivityForResult(takeCaptureIntent, REQUEST_CAMERA_CODE);
                mIsActivityResultWaiting = true;
            }
        }

        public boolean onToolbarClicked() {
            if (mIsEditMode) {
                if (!getView().validatePhoneAndName()) {
                    return false;
                }
                mAccountModel.saveProfileInfo(getView().getUserInfo());
            }
            mIsEditMode = !mIsEditMode;
            getView().refreshToCurrentMode();
            return true;
        }

        public void onAddAddressClicked() {
            Flow.get(getView()).set(new AddressScreen());
        }


        public void onAvatarClicked() {
            getView().showPhotoSourceDialog();
        }


        public void onUserSettingsChanged(UserSettingsDto userSettings) {
            mAccountModel.saveUserSettings(userSettings);
        }

        public void onDeleteAddressClicked(long id) {
            mAccountModel.deleteAddress(id);
        }

        public void onEditAddressClicked(long id) {
            Flow.get(getView()).set(new AddressScreen(id));
        }

        private void setupNavigation(){
            mRootPresenter.createNavigationBuilder()
                    .setTitle(R.string.title_account)
                    .build();
        }
    }
}
