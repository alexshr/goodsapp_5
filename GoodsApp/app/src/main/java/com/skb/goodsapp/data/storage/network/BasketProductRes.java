package com.skb.goodsapp.data.storage.network;

import com.squareup.moshi.Json;

public class BasketProductRes {
    @Json(name = "_id")
    public String id;
    public int count;
}
