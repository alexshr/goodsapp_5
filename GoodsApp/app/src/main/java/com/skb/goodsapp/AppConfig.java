package com.skb.goodsapp;


import java.util.regex.Pattern;

public interface AppConfig {

    Pattern EMAIL_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\._%\\-]{3,256}" +
                    "@[a-zA-Z0-9]{2,64}" +
                    "\\.[a-zA-Z0-9]{2,25}"
    );

    Pattern PASSWORD_PATTERN = Pattern.compile(
            "^[a-zA-Z0-9@#$%!]{8,}$"
    );

    String BASE_URL = "https://skba1.mgbeta.ru/api/v1/";

    //mock address
    String BASE_LOGIN_URL = "https://private-8967d4-middleappskillbranch.apiary-mock.com";


    int MAX_CONNECTION_TIMEOUT = 5000;
    int MAX_READ_TIMEOUT = 5000;
    int MAX_WRITE_TIMEOUT = 5000;

    String FILE_PROVIDER_AUTHORITY = "com.skb.goodsapp.fileprovider";
    String LAST_MODIFIED_HEADER = "Last-Modified";
    String IF_MODIFIED_SINCE_HEADER = "If-Modified-Since";
    String DEFAULT_LAST_MODIFIED_DATE = "Thu, 01 Jan 1970 00:00:00 GMT";
    String COMMENT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    String UTC_TIME_ZONE = "UTC";
    //int UPDATE_DATA_INTERVAL = 3000;
    int UPDATE_DATA_INTERVAL = 30;

    //job manager
    int MIN_CONSUMER_COUNT = 1;
    int MAX_CONSUMER_COUNT = 3;
    int LOAD_FACTOR = 3;
    int KEEP_ALIVE = 120;

    int RETRY_INITIAL_DELAY = 100;

    //basket
    int MIN_COST_TO_DISCOUNT = 3000;
    int DISCOUNT_PRC = 15; //скидка в %
    String NO_NAME = "user";
}
