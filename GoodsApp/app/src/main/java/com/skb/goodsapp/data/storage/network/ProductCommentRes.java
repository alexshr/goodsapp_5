package com.skb.goodsapp.data.storage.network;

import android.support.annotation.VisibleForTesting;

import com.squareup.moshi.Json;

import static android.support.annotation.VisibleForTesting.NONE;

public class ProductCommentRes {

    @Json(name="_id")
    public String id;
    public Integer remoteId;
    public String avatar;
    @Json(name="raiting")
    public Float rating;
    public String comment;
    public String userName;
    @Json(name="active")
    public Boolean isActive;
    public String commentDate;

    //приходит в ответе на post
    public String productId;

    @VisibleForTesting(otherwise = NONE)
    public ProductCommentRes(String id, Integer remoteId, String avatar, Float rating, String comment, String userName, Boolean isActive, String commentDate, String productId) {
        this.id = id;
        this.remoteId = remoteId;
        this.avatar = avatar;
        this.rating = rating;
        this.comment = comment;
        this.userName = userName;
        this.isActive = isActive;
        this.commentDate = commentDate;
        this.productId = productId;
    }

    @VisibleForTesting(otherwise = NONE)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductCommentRes that = (ProductCommentRes) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (remoteId != null ? !remoteId.equals(that.remoteId) : that.remoteId != null)
            return false;
        if (avatar != null ? !avatar.equals(that.avatar) : that.avatar != null) return false;
        if (rating != null ? !rating.equals(that.rating) : that.rating != null) return false;
        if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null)
            return false;
        if (isActive != null ? !isActive.equals(that.isActive) : that.isActive != null)
            return false;
        if (commentDate != null ? !commentDate.equals(that.commentDate) : that.commentDate != null)
            return false;
        return productId != null ? productId.equals(that.productId) : that.productId == null;

    }


}