package com.skb.goodsapp;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.skb.goodsapp.data.managers.RealmManager;
import com.skb.goodsapp.di.AppComponent;
import com.skb.goodsapp.di.AppModule;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.mortar.ScreenScoper;
import com.skb.goodsapp.utils.ImageUtils;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;
import com.vk.sdk.VKSdk;

import mortar.MortarScope;

//import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

public class MyApplication extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "2AXTcJwR20p6j5GirHI4NA19p";
    private static final String TWITTER_SECRET = "vDK5iKZo3X63406Yo8mJi36hrVOUcsTgS7NVKCZDqikWnE59px";


    private MortarScope mRootScope;
    private static MyApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        //Fabric.with(this, new Twitter(authConfig));
        TwitterConfig.Builder builder=new TwitterConfig.Builder(this);
        builder.twitterAuthConfig(authConfig);
        Twitter.initialize(builder.build());


        sInstance = this;
        RealmManager.initRealm(this);
        ImageUtils.initPicasso(this);
        VKSdk.initialize(this);

        if (BuildConfig.DEBUG) {
            Stetho.initialize(Stetho.newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                    .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                    .build());
        }
    }

    @Override
    public Object getSystemService(String name) {
        if (mRootScope == null) mRootScope = MortarScope.buildRootScope()
                .withService(DaggerService.SERVICE_NAME, createComponent())
                .build(ScreenScoper.SCOPE_ROOT_NAME);

        return mRootScope.hasService(name) ? mRootScope.getService(name) : super.getSystemService(name);
    }

    public static MyApplication getInstance() {
        return sInstance;
    }

    public static AppComponent getDaggerComponent() {
        return DaggerService.getDaggerComponent(MyApplication.getInstance());
    }

    public Object createComponent() {
        return DaggerService.buildComponent(AppComponent.class, new AppModule(this));
    }
//endregion
}
