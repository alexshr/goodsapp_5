package com.skb.goodsapp.data.network;

import android.util.Log;

import com.skb.goodsapp.MyApplication;

import java.io.IOException;

import retrofit2.Response;
import retrofit2.Retrofit;

import static com.skb.goodsapp.ConstantManager.LOG_TAG;

public class ErrorUtils {

    public static ApiError parseError(Response<?> response) {
        try {
            Retrofit retrofit= MyApplication.getDaggerComponent().getRetrofit();

            return (ApiError)retrofit
                    .responseBodyConverter(ApiError.class, ApiError.class.getAnnotations())
                    .convert(response.errorBody());
        } catch (IOException e) {
            Log.e(LOG_TAG, "", e);
            return new ApiError();
        }
    }
}
