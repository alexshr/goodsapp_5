package com.skb.goodsapp.ui.screens.comments;


import android.content.Context;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.ProductCommentRealm;
import com.skb.goodsapp.utils.BorderedCircleTransform;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

import static com.skb.goodsapp.AppConfig.COMMENT_DATE_FORMAT;
import static com.skb.goodsapp.AppConfig.UTC_TIME_ZONE;
import static com.skb.goodsapp.ConstantManager.LOG_TAG;
import static com.skb.goodsapp.R.id.date;

public class ProductCommentsRealmAdapter extends RealmRecyclerViewAdapter<ProductCommentRealm, ProductCommentsRealmAdapter.ViewHolder> {

    private Context mContext;
    private static SimpleDateFormat sDateFormat;

    static {
        sDateFormat = new SimpleDateFormat(
                COMMENT_DATE_FORMAT, Locale.US);
        sDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TIME_ZONE));
    }

    public ProductCommentsRealmAdapter(Context context, OrderedRealmCollection<ProductCommentRealm> data) {
        super(data, true);
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductCommentsRealmAdapter.ViewHolder holder, int position) {

        ProductCommentRealm comment = getData().get(position);

        String avatarUrl = comment.getAvatar();
        Picasso.with(mContext)
                .load(avatarUrl != null && !avatarUrl.isEmpty() ? avatarUrl : "dummy")
                .placeholder(R.mipmap.ic_launcher)
                .fit()
                .transform(new BorderedCircleTransform(0))
                .into(holder.mAvatar);

        holder.mText.setText(comment.getComment());
        holder.mRatingBar.setRating(comment.getRating());
        holder.mUserName.setText(comment.getUserName());

        try {
            if (comment.getCommentDate() != null) {
                Date date = sDateFormat.parse(comment.getCommentDate());
                holder.mDate.setText(DateUtils.getRelativeTimeSpanString(date.getTime()));
            } else {//эта запись не с сервера
                holder.mDate.setText(mContext.getText(R.string.in_process));
            }
        } catch (ParseException e) {
            Log.e(LOG_TAG, "error date to convert: " + comment.getCommentDate(), e);
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        //region ================= butterknife =================
        @BindView(R.id.avatar)
        ImageView mAvatar;

        @BindView(R.id.user_name)
        TextView mUserName;

        @BindView(R.id.product_rating)
        AppCompatRatingBar mRatingBar;

        @BindView(R.id.text)
        TextView mText;

        @BindView(date)
        TextView mDate;

        //endregion

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
