package com.skb.goodsapp.jobs;


import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Params;
import com.skb.goodsapp.ConstantManager;
import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.network.ProductCommentRes;

import java.util.Date;

public class PostCommentJob extends BaseJob {

    public final static int SUCCESS_MES = R.string.comment_added;
    public final static int CANCEL_MES = R.string.comment_cancelled;

    private String mProductId;
    private String mText;
    private float mRating;
    private long mLocalId;

    public PostCommentJob(float rating, String text, String productId) {
        super(new Params(JobPriority.MID)
                .requireNetwork()
                .persist()
                .groupBy(ConstantManager.JOB_GROUP_COMMENTS));  //группа задач (выполняются поочередно)
        mRating = rating;
        mProductId = productId;
        mText = text;
        mLocalId = new Date().getTime();
        setSuccessMes(new MessageDto(SUCCESS_MES));
        setCancelMes(new MessageDto(CANCEL_MES));
    }

    @Override
    public void onAdded() {
        super.onAdded();
        mDataManager.getRealmManager().saveComment(mRating, mText, mLocalId, mProductId);
    }

    @Override
    public void onRun() throws Throwable {
        ProductCommentRes commentRes = mDataManager.postComment(mProductId, mRating, mText);
        mDataManager.getRealmManager().replaceCommentFromServer(commentRes, mLocalId);
        super.onRun();
    }

    @Override
    protected void onCancel(int i, @Nullable Throwable throwable) {
        mDataManager.getRealmManager().deleteLocalComment(mLocalId);
        super.onCancel(i, throwable);
    }
}
