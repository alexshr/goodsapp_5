package com.skb.goodsapp.ui.screens.address;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.dto.MessageDto;
import com.skb.goodsapp.data.storage.realm.UserAddressRealm;
import com.skb.goodsapp.di.DaggerService;
import com.skb.goodsapp.flow.AbstractScreen;
import com.skb.goodsapp.flow.Screen;
import com.skb.goodsapp.mvp.models.AccountModel;
import com.skb.goodsapp.mvp.presenters.RootPresenter;
import com.skb.goodsapp.ui.screens.account.AccountScreen;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Inject;
import javax.inject.Scope;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import mortar.ViewPresenter;


@Screen(R.layout.screen_address)
public class AddressScreen extends AbstractScreen {

    @Nullable
    private Long mUserAddressId;

    public AddressScreen(long userAddressId) {
        this();
        mUserAddressId = userAddressId;
    }

    public AddressScreen() {
        setParentScopeScreen(new AccountScreen());
    }

    @Override
    public Object[] getModules() {
        return new Object[]{new Module()};
    }

    @Override
    public Class getComponentClass() {
        return Component.class;
    }

    //region ============================== DI ==============================

    @dagger.Module
    public class Module {
        @Provides
        @AddressScope
        AddressPresenter provideAddressPresenter() {
            return new AddressPresenter();
        }
    }

    @dagger.Component(dependencies = AccountScreen.Component.class, modules = Module.class)
    @AddressScope
    public interface Component {
        void inject(AddressPresenter presenter);

        void inject(AddressView view);
    }

    @Scope
    @Retention(RetentionPolicy.RUNTIME)
    public @interface AddressScope {
    }
    //endregion

    //region ============================== Presenter ==============================

    public class AddressPresenter extends ViewPresenter<AddressView> {
        @Inject
        AccountModel mAccountModel;
        @Inject
        RootPresenter mRootPresenter;

        UserAddressRealm mUserAddress;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (mUserAddressId != null && mUserAddress == null) {//edit (before screen rotation)
                mUserAddress = mAccountModel.getUserAddressById(mUserAddressId);
                if (mUserAddress != null) {
                    getView().initView(mUserAddress);
                }
            }
        }


        public void onSaveAddressClicked() {
            if (getView() != null) {
                UserAddressRealm userAddress = new UserAddressRealm();
                getView().fillAddress(userAddress);
                if (validateAddress(userAddress)) {
                    if (mUserAddress != null) {
                        userAddress.setId(mUserAddressId);
                    }
                    mAccountModel.addOrUpdateUserAddress(userAddress);
                    //noinspection CheckResult - чтобы lint не ругался на след. строку
                    Flow.get(getView()).goBack();
                } else {
                    mRootPresenter.showError(new MessageDto(R.string.address_add_err));
                }
            }
        }

        public boolean validateAddress(UserAddressRealm address) {
            return address.validate();
        }
    }

    //endregion
}
