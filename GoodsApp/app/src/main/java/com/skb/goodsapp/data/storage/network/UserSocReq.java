package com.skb.goodsapp.data.storage.network;

public class UserSocReq extends UserReq {
    private String mFirstName;
    private String mLastName;
    private String mAvatarUrl;
    private String mPhone;

    public UserSocReq(String email, String firstName, String lastName, String avatarUrl, String phone) {
        super(email);
        mFirstName = firstName;
        mLastName = lastName;
        mAvatarUrl = avatarUrl;
        mPhone = phone;
    }

    public UserSocReq() {
        super();
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public void setAvatarUrl(String avatarUrl) {
        mAvatarUrl = avatarUrl;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserSocReq)) return false;

        UserSocReq that = (UserSocReq) o;

        if (getEmail() != null ? !getEmail().equals(that.getEmail()) : that.getEmail() != null)
            return false;
        if (getFirstName() != null ? !getFirstName().equals(that.getFirstName()) : that.getFirstName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(that.getLastName()) : that.getLastName() != null)
            return false;
        if (getAvatarUrl() != null ? !getAvatarUrl().equals(that.getAvatarUrl()) : that.getAvatarUrl() != null)
            return false;
        return getPhone() != null ? getPhone().equals(that.getPhone()) : that.getPhone() == null;

    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserSocReq{");
        sb.append("mFirstName='").append(mFirstName).append('\'');
        sb.append(", mLastName='").append(mLastName).append('\'');
        sb.append(", mAvatarUrl='").append(mAvatarUrl).append('\'');
        sb.append(", mPhone='").append(mPhone).append('\'');
        sb.append(", mEmail='").append(getEmail()).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
