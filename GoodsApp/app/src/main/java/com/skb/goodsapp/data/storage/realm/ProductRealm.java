package com.skb.goodsapp.data.storage.realm;

import com.skb.goodsapp.data.storage.network.ProductRes;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ProductRealm extends RealmObject {

    @PrimaryKey
    private String id;
    private Integer remoteId;
    private String productName;
    private String imageUrl;
    private String description;
    private Integer price = 0;
    private Float rating;


    public static String formatCost(int cost) {
        return String.valueOf(cost + ".-");
    }


    public static ProductRealm getInstanceFromNetworkRes(ProductRes productRes) {
        ProductRealm product = new ProductRealm();
        product.id = productRes.id;
        product.productName = productRes.productName;
        product.imageUrl = productRes.imageUrl;
        product.description = productRes.description;
        product.price = productRes.price;
        product.rating = productRes.rating;
        return product;
    }

    public void updateFromNetworkRes(ProductRes productRes) {

        productName = productRes.productName;
        imageUrl = productRes.imageUrl;
        description = productRes.description;
        price = productRes.price;
        rating = productRes.rating;
    }

    public static ProductRealm getByPrimaryKey(Realm realm, String id) {
        return realm.where(ProductRealm.class).equalTo("id", id).findFirst();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }



    @Override
    public String toString() {
        StringBuilder res = new StringBuilder("ProductRealm{" +
                "id='" + id + '\'' +
                ", remoteId=" + remoteId +
                ", productName='" + productName + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", rating=" + rating + "\n");

        return res.toString();
    }


}
