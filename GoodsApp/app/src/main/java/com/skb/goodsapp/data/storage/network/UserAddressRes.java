package com.skb.goodsapp.data.storage.network;

import com.squareup.moshi.Json;

public class UserAddressRes {
    @Json(name = "_id")
    private String id;
    private String name;
    private String street;
    private String house;
    private String apartment;
    private int floor;
    private String comment;

    public UserAddressRes(String id, String name, String street, String house, String apartment, int floor, String comment) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.house = house;
        this.apartment = apartment;
        this.floor = floor;
        this.comment = comment;
    }

    public UserAddressRes() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAddressRes that = (UserAddressRes) o;

        if (floor != that.floor) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (street != null ? !street.equals(that.street) : that.street != null) return false;
        if (house != null ? !house.equals(that.house) : that.house != null) return false;
        if (apartment != null ? !apartment.equals(that.apartment) : that.apartment != null)
            return false;
        return comment != null ? comment.equals(that.comment) : that.comment == null;

    }

}
