package com.skb.goodsapp.data.storage.realm;

import com.skb.goodsapp.data.storage.network.ProductCommentRes;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class ProductCommentRealm extends RealmObject {

    @PrimaryKey
    private String id;

    /*для неотправленных на сервер записей
     чтобы не удалить при синхронизации*/
    private Long localId;
    private String avatar;
    private Float rating;
    private String comment;
    private String userName;
    private String commentDate;

    private String productId;

    public ProductCommentRealm() {
    }

    public static ProductCommentRealm getLocalInstance(String productId, float rating, String comment, Long localId) {
        ProductCommentRealm productComment = new ProductCommentRealm();
        productComment.rating = rating;
        productComment.comment = comment;
        productComment.localId = localId;
        productComment.productId = productId;
        return productComment;
    }

    //from products get resp
    public static ProductCommentRealm getInstanceFromNetworkRes(ProductCommentRes productCommentRes, String productId) {
        ProductCommentRealm comment = new ProductCommentRealm();
        comment.id = productCommentRes.id;
        comment.avatar = productCommentRes.avatar;
        comment.rating = productCommentRes.rating;
        comment.comment = productCommentRes.comment;
        comment.userName = productCommentRes.userName;
        comment.commentDate = productCommentRes.commentDate;
        comment.productId = productId;
        return comment;
    }

    //from comment post resp
    public static ProductCommentRealm getInstanceFromNetworkRes(ProductCommentRes productCommentRes) {
        ProductCommentRealm comment = new ProductCommentRealm();
        comment.id = productCommentRes.id;
        comment.avatar = productCommentRes.avatar;
        comment.rating = productCommentRes.rating;
        comment.comment = productCommentRes.comment;
        comment.userName = productCommentRes.userName;
        comment.commentDate = productCommentRes.commentDate;
        comment.productId = productCommentRes.productId;
        return comment;
    }

    public void updateFromNetworkRes(ProductCommentRes productCommentRes, String productId) {
        avatar = productCommentRes.avatar;
        rating = productCommentRes.rating;
        comment = productCommentRes.comment;
        userName = productCommentRes.userName;
        commentDate = productCommentRes.commentDate;
        this.productId = productId;
    }

    public static ProductCommentRealm getByPrimaryKey(Realm realm, String id) {
        return realm.where(ProductCommentRealm.class).equalTo("id", id).findFirst();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public Long getLocalId() {
        return localId;
    }

    public void setLocalId(Long localId) {
        this.localId = localId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductCommentRealm{");
        sb.append("id='").append(id).append('\'');
        sb.append(", productId=").append(productId).append('\'');
        sb.append(", localId=").append(localId);
        sb.append(", avatar='").append(avatar).append('\'');
        sb.append(", rating=").append(rating);
        sb.append(", comment='").append(comment).append('\'');
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", commentDate='").append(commentDate);

        sb.append('}');
        return sb.toString();
    }


    public static RealmResults<ProductCommentRealm> getByProductId(Realm realm, String productId) {
        return realm.where(ProductCommentRealm.class).equalTo("productId", productId).findAll();
    }
}