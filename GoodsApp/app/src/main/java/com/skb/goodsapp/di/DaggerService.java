package com.skb.goodsapp.di;

import android.content.Context;
import android.util.Log;

import java.lang.reflect.Method;

import static com.skb.goodsapp.ConstantManager.LOG_TAG;


public class DaggerService {

    public static final String SERVICE_NAME = DaggerService.class.getName();

    /**
     * Caller is required to know the type of the component for this context.
     */
    @SuppressWarnings("unchecked") //
    public static <T> T getDaggerComponent(Context context) {
        //noinspection ResourceType
        return (T) context.getSystemService(SERVICE_NAME);
    }

    /**
     * Magic method that creates a component with its dependencies set, by reflection. Relies on
     * Dagger2 naming conventions.
     */
    public static <T> T buildComponent(Class<T> componentClass, Object... dependencies) {

        T component = null;
        String fqn = componentClass.getName();
        String packageName = componentClass.getPackage().getName();
        // Accounts for inner classes, ie MyApplication$Component
        String simpleName = fqn.substring(packageName.length() + 1);
        String generatedName = (packageName + ".Dagger" + simpleName).replace('$', '_');

        try {
            Class<?> generatedClass = Class.forName(generatedName);
            Object builder = generatedClass.getMethod("builder").invoke(null);

            for (Method method : builder.getClass().getDeclaredMethods()) {
                Class<?>[] params = method.getParameterTypes();
                if (params.length == 1) {
                    Class<?> dependencyClass = params[0];
                    for (Object dependency : dependencies) {
                        if (dependencyClass.isAssignableFrom(dependency.getClass())) {
                            method.invoke(builder, dependency);
                            break;
                        }
                    }
                }
            }
            //noinspection unchecked

            component = (T) builder.getClass().getMethod("build").invoke(builder);
            Log.d(LOG_TAG, "component builded: " + component);
        } catch (Exception e) {
            Log.e(LOG_TAG, "", e);
        }
        return component;
    }


    /**
     * Magic method that creates a component with its dependencies set, by reflection. Relies on
     * Dagger2 naming conventions. Specified for flow screen.
     */
    public static <T> T buildComponent(Class<T> componentClass, Object parentComponent, Object[] modules) {

        Object[] dependencies = new Object[modules.length + 1];
        int i = 0;
        dependencies[i++] = parentComponent;
        for (Object module : modules) {
            dependencies[i++] = module;
        }
        return buildComponent(componentClass, dependencies);
    }

}
