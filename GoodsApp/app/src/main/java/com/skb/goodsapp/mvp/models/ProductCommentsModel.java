package com.skb.goodsapp.mvp.models;

import com.skb.goodsapp.data.storage.realm.ProductCommentRealm;
import com.skb.goodsapp.jobs.PostCommentJob;

import io.realm.RealmResults;


public class ProductCommentsModel extends RootModel {

    public void postComment(float rating, String comment, String productId) {
        PostCommentJob job = new PostCommentJob(rating, comment, productId);
        getJobManager().addJobInBackground(job);
    }

    public RealmResults<ProductCommentRealm> getProductComments(String productId) {
        return mRealmManager.getProductComments(productId);
    }


}
