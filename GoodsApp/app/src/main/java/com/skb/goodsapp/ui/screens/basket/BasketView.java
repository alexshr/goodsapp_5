package com.skb.goodsapp.ui.screens.basket;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.skb.goodsapp.R;
import com.skb.goodsapp.data.storage.realm.BasketProductRealm;
import com.skb.goodsapp.di.DaggerService;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;

public class BasketView extends FrameLayout {

    @Inject
    BasketScreen.BasketPresenter mPresenter;

    //region ================= butterknife =================
    @BindView(R.id.basket_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.empty_view)
    TextView mEmptyView;
    @BindView(R.id.view_switcher)
    ViewSwitcher mViewSwitcher;

    @BindView(R.id.product_count)
    TextView mProductCountView;
    @BindView(R.id.discount)
    TextView mDiscountView;
    @BindView(R.id.total_cost)
    TextView mTotalCostView;
    @BindView(R.id.checkout_btn)
    TextView mCheckountBtn;
    //endregion

    private BasketRealmAdapter mBasketRealmAdapter;

    //для анимированного переключения между списком и empty screen
    private RecyclerView.AdapterDataObserver mAdapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            if ((mViewSwitcher.getDisplayedChild() == 0 && mBasketRealmAdapter.getItemCount() == 0)
                    || (mViewSwitcher.getDisplayedChild() == 1 && mBasketRealmAdapter.getItemCount() > 0)) {
                mViewSwitcher.showNext();
            }
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            onChanged();
        }
    };

    OrderedRealmCollectionChangeListener<RealmResults<BasketProductRealm>> mBasketRealmChangeListener = new OrderedRealmCollectionChangeListener<RealmResults<BasketProductRealm>>() {
        @Override
        public void onChange(RealmResults<BasketProductRealm> collection, OrderedCollectionChangeSet changeSet) {
            if (mBasketRealmAdapter.getItemCount() > 0) {
                BasketFooter basketFooter = new BasketFooter((RealmResults<BasketProductRealm>) mBasketRealmAdapter.getData());
                BasketView.this.showFooter(basketFooter);
            }
        }
    };


    private Unbinder mUnbinder;

    public BasketView(Context context, AttributeSet attrs) {
        super(context, attrs);
        DaggerService.<BasketScreen.Component>getDaggerComponent(context).inject(this);
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mUnbinder = ButterKnife.bind(this);

    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mBasketRealmAdapter.unregisterAdapterDataObserver(mAdapterDataObserver);
            ((RealmResults<BasketProductRealm>) mBasketRealmAdapter.getData()).removeChangeListener(mBasketRealmChangeListener);
            mUnbinder.unbind();
            mPresenter.dropView(this);
        }
    }

    public void showFooter(BasketFooter basketFooter) {
        mProductCountView.setText(getResources().getString(R.string.count_templ, basketFooter.getProductCount() + ""));
        mDiscountView.setText(getResources().getString(R.string.cost_templ, basketFooter.getDiscount() + ""));
        mTotalCostView.setText(getResources().getString(R.string.cost_templ, basketFooter.getCost() + ""));
    }

    public void initView(RealmResults<BasketProductRealm> basketProducts) {

        mViewSwitcher.setInAnimation(AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_in));
        mViewSwitcher.setOutAnimation(AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_out));

        mBasketRealmAdapter = new BasketRealmAdapter(getContext(), basketProducts, new BasketRealmAdapter.ActionItemListener() {
            @Override
            public void onDeleteClicked(String productId) {
                mPresenter.deleteFromBasket(productId);
            }

            @Override
            public void onImageClicked(String id) {
                mPresenter.goToProductDetail(id);
            }

            @Override
            public void onProductNumberChanged(String productId, int count) {
                mPresenter.updateBasketFromUser(productId, count);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mBasketRealmAdapter);
        mBasketRealmAdapter.registerAdapterDataObserver(mAdapterDataObserver);

        ((RealmResults<BasketProductRealm>) mBasketRealmAdapter.getData()).addChangeListener(mBasketRealmChangeListener);

        mAdapterDataObserver.onChanged();
    }


}
