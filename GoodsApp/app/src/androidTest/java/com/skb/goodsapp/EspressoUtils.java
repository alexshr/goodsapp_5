package com.skb.goodsapp;

import android.content.Context;
import android.support.design.internal.NavigationMenuItemView;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.v7.widget.ActionMenuView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.schibsted.spain.barista.BaristaEditTextActions;
import com.schibsted.spain.barista.BaristaNavigationDrawerActions;
import com.skb.goodsapp.data.managers.RealmManager;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

import java.io.File;

import io.realm.Realm;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isFocusable;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.schibsted.spain.barista.BaristaAssertions.assertDisplayed;
import static com.schibsted.spain.barista.BaristaAssertions.assertNotExist;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

public class EspressoUtils {

    private static String LOG = EspressoUtils.class.getSimpleName();

    private static RealmManager sRealmManager;


    public static ViewInteraction matchToolbarTitle(int titleRes) {
        return onView(isAssignableFrom(Toolbar.class))
                .check(matches(withToolbarTitle(is(getString(titleRes)))));
    }

    private static ViewInteraction matchActionMenuItemVisibility(final int menuItemId, boolean isVisible) {
        return onView(isAssignableFrom(ActionMenuView.class))
                .check(matches(withMenuItem(menuItemId, isVisible)));
    }

    public static ViewInteraction matchActionMenuItemDisplayed(final int menuItemId) {
        return matchActionMenuItemVisibility(menuItemId, true);
    }

    public static ViewInteraction matchActionMenuItemNotDisplayedOrNotExists(final int menuItemId) {
        return matchActionMenuItemVisibility(menuItemId, false);
    }

    public static ViewInteraction clickOnNavigationMenuItem(int titleRes) {
        return onView(allOf(withText(titleRes), withParent(isAssignableFrom(NavigationMenuItemView.class)))).perform(click());
    }


    private static void checkNavigationMenuItems(boolean isVisible, int... titles) {
        for (int title : titles) {
            onView(allOf(withText(title), withParent(isAssignableFrom(NavigationMenuItemView.class))))
                    .check(isVisible ? matches(isDisplayed()) : doesNotExist());
        }
    }

    public static void assertNavigationMenuItemsDisplayed(int... titles) {
        checkNavigationMenuItems(true, titles);
    }


    public static void assertNavigationMenuItemsNotExists(int... titles) {
        checkNavigationMenuItems(false, titles);
    }

    private static Matcher<Object> withToolbarTitle(final Matcher<CharSequence> textMatcher) {
        return new BoundedMatcher<Object, Toolbar>(Toolbar.class) {
            @Override
            public boolean matchesSafely(Toolbar toolbar) {
                return textMatcher.matches(toolbar.getTitle());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with toolbar title: ");
                textMatcher.describeTo(description);
            }
        };
    }

    private static Matcher<Object> withMenuItem(final int menuItemId, boolean isVisible) {
        return new BoundedMatcher<Object, ActionMenuView>(ActionMenuView.class) {
            @Override
            public boolean matchesSafely(ActionMenuView menuView) {
                View menuItem = menuView.findViewById(menuItemId);
                return isVisible ? menuItem.getVisibility() == View.VISIBLE : menuItem == null || menuItem.getVisibility() != View.VISIBLE;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with toolbar item id: " + menuItemId + " isVisible: " + isVisible);

            }
        };
    }


    public static String getString(int id) {
        Context targetContext = InstrumentationRegistry.getTargetContext();
        return targetContext.getResources().getString(id);
    }

    public static void openDrawer() {
        BaristaNavigationDrawerActions.openDrawer(R.id.drawer_layout);
    }

    public static void writeToLoginText(String email) {
        BaristaEditTextActions.writeToEditText(R.id.login_email_et, email);
    }

    public static void writeToPasswordText(String password) {
        BaristaEditTextActions.writeToEditText(R.id.login_password_et, password);
    }

    public static void clickLoginBtn() {
        onView(allOf(withId(R.id.login_btn), isClickable(), isFocusable())).perform(click());
    }

    public static void closeDrawer() {
        BaristaNavigationDrawerActions.closeDrawer(R.id.drawer_layout);
    }

    public static void assertLoginBtnDisabled() {
        assertDisplayed(R.id.login_btn);
    }

    public static void assertIllegalEmailHintDisplayed() {
        assertDisplayed(R.string.err_msg_email);
    }

    public static void assertIllegalPasswordHintDisplayed() {
        assertDisplayed(R.string.err_msg_password);
    }

    public static void assertIllegalEmailHintNotExists() {
        assertNotExist(R.string.err_msg_email);
    }

    public static void assertIllegalPasswordHintNotExists() {
        assertNotExist(R.string.err_msg_password);
    }

    public static void clickCatalogFavoriteCheckbox() {
        onView(allOf(withId(R.id.favorite_checkbox), isDisplayed())).perform(ViewActions.click());
    }

    public static void assertCatalogProductCounter(String s) {
        onView(allOf(withId(R.id.product_count_text_view), isDisplayed())).check(matches(withText(s)));
    }

    public static void clickCatalogProductPlusBtn(int pressCount) {
        ViewInteraction plusBtn = onView(allOf(withId(R.id.plus_button), isDisplayed()));
        for (int i = 0; i < pressCount; i++) {
            plusBtn.perform(click());
        }
    }


    public static void clickCatalogAddToBasketBtn() {
        onView(allOf(withId(R.id.add_to_card_button), isClickable(), isFocusable())).perform(swipeDown());
    }

    public static void clickToolbarBasketBtn() {
        matchToolbarBasketBtn().perform(click());
    }

    public static ViewInteraction matchToolbarBasketBtn() {
        return matchActionMenuItemDisplayed(R.id.basket_button);
    }

    public static int getToolbarBasketCount() {
        String text = getText(withId(R.id.basket_count));
        return Integer.parseInt(text);
    }

    private static String getText(final Matcher<View> matcher) {
        final String[] stringHolder = {null};
        onView(matcher).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "getting text from a TextView";
            }

            @Override
            public void perform(UiController uiController, View view) {
                TextView tv = (TextView) view; //Save, because of check in getConstraints()
                stringHolder[0] = tv.getText().toString();
            }
        });
        return stringHolder[0];
    }

    public static void deleteAllFromRealm() {
        RealmManager.configureRealm();
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(r -> r.deleteAll());
        realm.close();

    }

    public static void resetPreferences() {
        File root = InstrumentationRegistry.getTargetContext().getFilesDir().getParentFile();
        String[] sharedPreferencesFileNames = new File(root, "shared_prefs").list();
        if (sharedPreferencesFileNames != null) {
            for (String fileName : sharedPreferencesFileNames) {
                InstrumentationRegistry.getTargetContext().getSharedPreferences(fileName.replace(".xml", ""), Context.MODE_PRIVATE).edit().clear().commit();
            }
        }
    }

}
