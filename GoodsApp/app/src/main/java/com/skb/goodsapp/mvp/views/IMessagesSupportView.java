package com.skb.goodsapp.mvp.views;

import com.skb.goodsapp.data.storage.dto.MessageDto;

public interface IMessagesSupportView {


    void showError(MessageDto messageDto);

    void showMessage(MessageDto messageDto);

}
